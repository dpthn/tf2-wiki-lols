<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - La Mise à Jour Mann-Conomie</title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro_french">En règle générale, lors de la sortie de mises à jour de TF2, nous essayons d'être aussi vagues que possible, sachant que nos fans comprendront tout par eux-même une fois qu'ils auront pu jouer un peu. En revanche, avec une mise à jour aussi ambitieuse que la mise à jour Mann-Conomie, et car cela implique de l'argent bien réel, nous avons voulu mettre de côté ce petit jeu avec nos fans et expliquer précisément ce que nous sommes en train de faire ainsi que le pourquoi du comment de cette mise à jour.</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Back to Update Page" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Back to TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top">
			<div id="FAQ_top_content_french">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        		<h1>À PROPOS DU MAGASIN</h1>
           		<h2>Q : Le Magasin Mann Co., c'est quoi ?</h2>
                R : Accessible directement depuis le jeu TF2, le Magasin Mann Co. permet maintenant aux joueurs d'utiliser leur Porte-monnaie Steam pour acheter, échanger et pour personnaliser les objets ajoutés dans la plus grande mise à jour d'objets de l'histoire du jeu.
            	<br><br>
                <h2>Q : Le Porte-monnaie Steam, c'est quoi ?</h2>
                R : Steam a maintenant une nouvelle fonction appelée Porte-monnaie Steam. Vous pouvez ajouter de l'argent dans ce Porte-monnaie, pour ensuite le dépenser en achetant des objets dans le jeu (comme par exemple les objets disponibles sur le magasin TF2 Mann Co.) ou bien en achetant des jeux sur le magasin Steam.
            	<br><br>
           		<h2>Q : Les prix objets du magasin sont-ils exprimés en points, ou dans une autre monnaie bizarre ?</h2>
                R : Non, le Porte-monnaie Steam utilise les mêmes devises qui sont actuellement supportés sur le magasin Steam (Dollars US, Livre Anglaise, Euros).
            	<br><br>
                <h2>Q : Est-ce que je peux mettre le montant de mon choix sur mon Porte-monnaie ?</h2>
                R : Oui. Nous ne vous forcerons pas à acheter des "packs" de crédit. Vous pouvez placer le montant exact dont vous avez besoin pour acheter les objets que vous voulez en particulier. Cependant, chaque fois que vous mettez de l'argent sur votre Porte-monnaie, le montant minimal par transaction est de 5$ / 4£ / 5€, pour minimiser les frais de transaction.
            	<br><br>
                <h2>Q : Puis-je échanger ou crafter des objets que j'ai acheté ?</h2>
                R : Dans l'immédiat, non. Vous ne pouvez échanger ou crafter que les objets que vous avez gagné ou trouvé dans le jeu (voir la partie Échange &amp; Personnalisation, ci dessous). Nous avons choisi d'adopter cette politique pour l'instant, en attendant de voir l'effet qu'un système d'échange d'objets achetés pourrait avoir sur la Mann-Conomie.
            	<br><br>
                <h2>Q : Est-ce que mon argent "TF2" est utilisable ailleurs sur Steam ?</h2>
                R : Oui. L'argent de votre Porte-monnaie Steam peut être utilisé pour acheter ce que vous voulez sur Steam.
                <br><br>
                <h2>Q : Pensez-vous ne vendre que des objets Valve officiels ? Qu'en est-il des objets réalisés par la communauté ?</h2>
                R : Ces deux types d'objets seront disponibles dans le magasin. D'ailleurs, les contributeurs de la communauté recevront un pourcentage des ventes de leurs objets ! Nous communiquerons bientôt plus d'informations sur l'organisation de ce système.
            	<br><br>
                <h2>Q : Je suis un membre de la communauté et l'un de mes objets a été intégré au jeu. Pourquoi n'est-il pas sur le magasin Mann Co. ?</h2>
                R : Dans un premier temps, nous n'allons vendre que les objets de la communauté issus du Pack Polycount, afin de finaliser l'organisation avec un petit nombre de contributeurs. Une fois que nous aurons bien cerné la partie paiement des contributeurs, nous ouvrirons le magasin à toutes les contributions de la communauté. Nous espérons faire cela dès que possible.
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content_french">
            <h1>QU'EST-CE QUI VA CHANGER ?<i> (ET QU'EST-CE QUI NE CHANGERA PAS ?)</i></h1>
            <h2>Q : Cela signifie-t'il qu'il n'y aura plus de mises à jour dans le futur ? Allez-vous simplement arrêter de proposer des mises à jour gratuites du contenu du jeu ?</h2>
            R : Non. Nous prévoyons de continuer de mettre à jour TF2 tout comme nous l'avons fait jusqu'à présent, en ajoutant des cartes, des modes de jeu, des nouvelles fonctions et bien d'autres choses gratuites. Le Magasin Mann Co. est simplement une solution alternative pour obtenir les objets que les autres joueurs peuvent gagner en jouant.
            <h2>Q : Allez-vous commencer à faire payer pour d'autres types de contenu, comme les cartes ?</h2>
            R : Non, nous ne prévoyons pas de faire cela. Séparer les joueurs en groupes distincts qui ne peuvent pas jouer ensemble, suivant le contenu acheté par chacun, correspond exactement à l'inverse de ce que nous voulons faire.
            <h2>Q : Est-ce qu'il va falloir que je passe à la caisse pour rester compétitif ?</h2>
            R : Non. Tous les objets qui affectent le jeu, et même les objets purement cosmétiques, pourront toujours être obtenus en jouant simplement au jeu.
            <h2>Q : Avez-vous ajusté la fréquence d'obtention des objets existants ?</h2>
            R : Non. Nous n'avons pas fait en sorte qu'il soit plus difficile d'obtenir des objets sans passer par le magasin Mann Co.
            <h2>Q : Pourquoi mes objets sont tous devenus des objets "Rétros" ?</h2>
            R : Certains objets qui étaient rares deviendront plus communs maintenant qu'ils peuvent être achetés. Nous voulions permettre aux joueurs de les acheter, mais dans le même temps mettre en avant les joueurs qui avaient obtenus ces objets par la "méthode traditionnelle". C'est pourquoi nous avons décidé de convertir tous les anciens objets en versions "Rétro", qui ne pourront pas être obtenues à nouveau dans le futur. Cela permet à ces objets plus anciens de rester rares (d'ailleurs, ils sont encore plus rares qu'avant, maintenant qu'ils sont en éditions limitées).
            <h2>Q : Qu'en est-il des objets que je gagnerai dans le futur ? Les autres joueurs pourront-ils voir la différence avec les objets achetés ?</h2>
            R : Nous n'envisageons pas pour l'instant de rendre la différence visible par les autres joueurs. Cependant, les objets achetés ne peuvent pour l'instant pas être échangés ou craftés, donc il y a quand même un avantage à obtenir vos objets par "la méthode traditionnelle".
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
            <h1>ÉCHANGE &amp; PERSONNALISATION</h1>
            <h2>Q : Vais-je pouvoir échanger des objets ?</h2>
            R : Oui. Nous avons ajouté la très attendue (et très demandée) fonction d'Échange, pour vous permettre d'échanger des objets avec vos amis.
            <br><br>
            <h2>Q : Tous les objets peuvent-ils être échangés ?</h2>
            R : Non, tous les objets ne peuvent pas être échangés. Certains objets en disponibilité limité ne peuvent pas être échangés. De même, les objets achetés ne peuvent pas l'être non plus pour l'instant (voir la partie À propos du Magasin).
            <br><br>
            <h2>Q : Puis-je personnaliser mes objets ?</h2>
            R : Oui. En plus des options de personnalisation actuelles, nous avons créé de nouveaux types d'objets qui permettent une personnalisation plus poussée, comme par exemple la possibilité de donner un nom aux objets et même de changer leur couleur. Vous pouvez trouver la liste des objets personnalisables ainsi que des détails sur les possibilités de personnalisation dans le Catalogue Mann Co.. Nous avons par ailleurs doublé la capacité de votre Sac à dos, pour que vous ayez tout l'espace nécessaire pour ranger ces nouveaux objets.
            <br><br>
            <h2>Q : Puis-je offrir des objets à quelqu'un ? Doivent-ils forcément me donner quelque chose en retour ?</h2>
            R : Oui, vous pouvez "offrir" des objets aux autres joueurs sans qu'ils aient à vous donner un objet en retour.
            <br><br>
            <h2>Q : Puis-je crafter les armes Polycount ? Existe-t-il des plans de fabrication, comme pour les autres armes de la communauté ?</h2>
            R : Oui, elles peuvent être craftées et ont le même style de plan de fabrication (associer deux objets) que les autres armes de la communauté.
            <br><br>
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
