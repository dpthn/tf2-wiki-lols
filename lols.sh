wget \
     --recursive \
     --continue \
     --page-requisites \
     --convert-links \
     --domains teamfortress.com \
     --no-parent \
         https://www.teamfortress.com/