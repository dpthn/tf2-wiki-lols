<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:Bold" rel="stylesheet">
	<link rel="shortcut icon" href="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
    <link rel="shortcut icon" href="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/favicon.ico">
    <link href="war.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="magnific-popup.css">
	<title>TF2 - WAR!</title>
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/jquery.spritely.js"></script>
	<script src="https://steamcdn-a.akamaihd.net/apps/dota2/javascript/static/jquery.cycle.all.js?v=253" type="text/javascript"></script>	
	<script src="jquery.magnific-popup.js" type="text/javascript"></script>
	<meta property="og:title" content="Team Fortress 2 - WAR!" />
	<meta property="og:type" content="game" />
	<meta property="og:url" content="https://www.teamfortress.com/meetyourmatch/war.php" />
	<meta property="og:image" content="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images//meetyourmatch_facebook.jpg" />
	<meta property="og:site_name" content="teamfortress.com" />
	<meta property="fb:admins" content="726460592" />
<script>	

// video popups		
		$(document).ready(function() {
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
});

</script>
<script src="https://www.teamfortress.com/meetyourmatch/jquery.twentytwenty.js"></script>
	
</head>
<body>
<div id="fb-root"></div>
<div id="day2_bg_01">
    <div id="day2_container_01">
		<a href="index.html" id="dayoneLink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="113px" height="60" /></a>
		<a href="../index.html" id="logoLink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="255" height="67" /></a>
		<div class="day2_h1" id="day2_copy_01_01">
			MAKE HEAVY (OR PYRO) GREAT AGAIN
		</div>
    </div>
</div>
<div id="day2_bg_02">
    <div id="day2_container_02">
		<div id="day2_copy_02_01">
			One lucky class is going to get a full-fledged class pack in a future update! New weapons! New balancing! New achievements! It could be Heavy. Or it could be Pyro. But it's not gonna be both!

			<br /><br />Here's how it works. The next time you start the game, you'll be asked to pick a side. (Choose wisely, because you only get to pick once.) From then on, every point you score in a Competitive or Casual match will count towards either Team Heavy or Team Pyro. Which class deserves it more? Vote with your bullets, and we will listen to those bullet-votes with our computers.
		</div>
		<a href="https://www.teamfortress.com/theshowdown" id="comicLink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="378" height="568" /></a>
		<div class="day2_h1" id="day2_copy_02_02">
			NEW TAUNTS
		</div>
		<div id="day2_copy_02_03">
			Torch your troubles away with this spring-mounted fiberglass fun-frenzy.
			<br /><i>Created by: Texman, JPRAS, 'R' DoubleTap and <br />KFC's Spicey Chicken Winglets</i>
		</div>
		<div id="popup_balloonibouncer">
			<a class="popup-youtube" href="https://www.youtube.com/watch?v=R_plMGc5o8w"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="199" height="112" "="" data-pin-nopin="true"></a>
		</div>
	</div>
</div>
<div id="day2_bg_03">
    <div id="day2_container_03">
		<div id="day2_copy_03_01">
			What's new, pussycat? This super-smooth sex bomb you just dropped on the battlefield.
			<br /><i>Created by: Valve</i>
		</div>
		<div id="popup_carlton">
			<a class="popup-youtube" href="https://www.youtube.com/watch?v=VxzwyBoaxXw"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="199" height="112" "="" data-pin-nopin="true"></a>
		</div>
    </div>
</div>
<div id="day2_bg_04">
    <div id="day2_container_04">
		<div id="day2_copy_04_01">
			Bump and hustle over the buried gibs of your enemies.
			<br /><i>Created by: Nonamesleft~</i>
		</div>
		<div id="popup_discofever">
			<a class="popup-youtube" href="https://www.youtube.com/watch?v=_yF_IOHKbYw"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="199" height="112" "="" data-pin-nopin="true"></a>
    	</div>	
    </div>
</div>
<div id="day2_bg_05">
    <div id="day2_container_05">
		<div id="day2_copy_05_01">
			"Freedom isn't free, maggots! Guess the price! Wrong! That is too high! Freedom is NOT that expensive!"
			<br /><i>Created by: Codenator, FiveEyes</i>
		</div>
		<div id="popup_fubarfanfare">
			<a class="popup-youtube" href="https://www.youtube.com/watch?v=VElUGO7W0fc"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="199" height="112" "="" data-pin-nopin="true"></a>
    	</div>
    </div>
</div>
<div id="day2_bg_06">
    <div id="day2_container_06">
		<div class="day2_h1" id="day2_copy_06_01">
			What's in the box?
		</div>
		<div class="h2" id="day2_copy_06_02">
			STARTER CLASS PACKS
		</div>
		<div id="day2_copy_06_03">
			New player? We put every functionally unique weapon for each class into nine handy, low-priced packages. It's never been easier or cheaper to try out a new class. 
			<br /><br />Seasoned vet? If you've been avoiding playing as a new class because you didn't have time to trade for the load-out you wanted, we're happy to look the other way if you want to buy it too.
		</div>
		<div class="h2" id="day2_copy_06_04">
			KEYLESS CLASS CRATES
		</div>
		<div id="day2_copy_06_05">
			Feeling lucky? Buy a keyless class crate and get a random cosmetic item for that class.
		</div>
	</div>
</div>
<div id="day2_bg_07">
    <div id="day2_container_07">
		<div class="h2" id="day2_copy_07_01">
			THE PERFECT STRANGER CRATE
		</div>
		<div id="day2_copy_07_02">
			The TF2 community has been asking for strange versions of these weapons, and we're shipping them all in one convenient crate. We're also throwing in an assortment of strange parts.
		</div>
		<a href="../index.html" id="minmax"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="438" height="274" /></a>
		<div class="day2_h1" id="day2_copy_07_03">
			SMALL WONDER
		</div>
		<div class="h2" id="day2_copy_07_04">
			Clear Things Up With Viewmodel Minmode
		</div>
		<div id="day2_copy_07_05">
			Viewmodel Minmode reduces the overall size of viewmodel weapons on your screen for competitive play.
		</div>
		<div class="day2_h1" id="day2_copy_07_06">
			EYE BEFORE YOU BUY
		</div>
		<div class="h2" id="day2_copy_07_07">
			Mouseover Previews
		</div>
		<div id="day2_copy_07_08">
			Now when you click on a crate in the Mann Co. Store, mousing over the name of any item in the list lets you see exactly what you're buying.
		</div>
    </div>
</div>
<div id="day2_bg_08">
    <div id="day2_container_08">
		<div class="day2_h1" id="day2_copy_08_01">
			SCRATCH YOUR PRINNY ITCH
		</div>
		<div id="day2_copy_08_02">
			Those of you who bought <i>Disgaea PC</i> during the Steam Summer Sale will get three <i>Disgaea</i>-themed cosmetic items (Prinny Hat, Prinny Pouch and Prinny Knife).
		</div>
    </div>
</div>
<div id="day2_bg_09">
    <div id="day2_container_09">
		<div class="day2_h1" id="day2_copy_09_01">
			MEET YOUR MATCH UPDATE NOTES
		</div>
    </div>
</div>
<div id="day2_bg_10">
    <div id="day2_container_10">
		<div id="day2_copy_10_01">
	
	<u>General</u>
		<ul class="bullet">
			<li>Reworked the main menu
				<ul class="bullet"><li>All play-related buttons are now accessible by clicking the "Find a game" button</li></ul>
				<ul class="bullet"><li>Moved the Workshop and Replay buttons down the bottom group of mini-buttons</li></ul>
			</li>
			<li>Enemies killed by energy weapons now play a special sound as their body dissolves</li>
			<li>Added new sound vo files for Competitve Mode</li>
			<li>Added sounds to all attacks where the target player resisted a part of the damage</li>
			<li>Added Prinny promo items for purchasing Disgaea: Prinny Hat, Prinny Pouch, and Prinny Machete</li>
			<li>Added AsiaFortress Cup Mercenaries Cup Season 7 medals</li>
			<li>Added TF2Maps 72hr TF2Jam Summer Participant medal</li>
			<li>Added Random Acts of TF2 medals</li>
			<li>Added check to prevent players with P-REC loaded from participating in matchmaking</li>
				<ul class="bullet"><li>Prevents P-REC crashing which results in players receiving abandon penalties</li></ul>		
			<li>Updated the Demo Support feature
				<ul class="bullet"><li>Added an option to only record matches that are played using Tournament mode (mp_tournament)</li></ul>
				<ul class="bullet"><li>Added an option to auto-delete matches that don't have any events recorded</li></ul>
				<ul class="bullet"><li>Fixed a bug where tickcount values were being noted incorrectly</li></ul>
				<ul class="bullet"><li>Options can be set using the Adv. Options menu</li></ul>								
			</li>	
			<li>Updated model/materials for the Bonk Boy, Dr's Dapper Topper, The Sole Saviors, and The Dark Falkirk Helm</li>		
			<li>Updated the localization files</li>
			<li>PASS Time update</li>
					<ul class="bullet">
						<li>No longer in Beta!</li>
					</ul>	
					<ul class="bullet">
						<li>New Items Available
							<ul class="bullet"><li>An Early Participation Pin will be awarded to everyone who played PASS Time during beta</li></ul>
							<ul class="bullet"><li>Two new achievements with item rewards
								<ul class="bullet"><li>Tune Merasmus's Multi-Dimensional Television</li></ul>
								<ul class="bullet"><li>Jackpot!</li></ul>
						</li>	
						<li>Map Changes
							<ul class="bullet"><li>All maps are tweaked, polished, and out of beta</li></ul>
							<ul class="bullet"><li>Renamed pass_pinewood to pass_timberlodge</li></ul>
							<ul class="bullet"><li>Renamed pass_warehouse to pass_brickyard</li></ul>
							<ul class="bullet"><li>Added a new city-themed map, pass_district</li></ul>
							<ul class="bullet"><li>An updated pass_template for level designers will be released on <a href="http://www.escalation.com/news">escalation.com/news</a></li></ul>
						</li>
						<li>Game Mode Changes
							<ul class="bullet"><li>Changed the score limit from 3 to 5</li></ul>
							<ul class="bullet"><li>Tweaked various JACK throwing parameters</li></ul>
							<ul class="bullet">
								<li>Pack Running
										<ul class="bullet"><li>The JACK no longer heals the player carrying it</li></ul>
										<ul class="bullet"><li>A player carrying the JACK with no nearby teammates is marked for death</li></ul>
										<ul class="bullet"><li>Teammates near a player holding the JACK will be slowly healed, and can run as fast as the fastest nearby teammate</li></ul>
								</li>
							</ul>
							<ul class="bullet">
									<li>JACK Power
										<ul class="bullet"><li>Passing the JACK increases a power meter</li></ul>
										<ul class="bullet"><li>The power meter will decay over time</li></ul>
										<ul class="bullet"><li>Filling the power meter unlocks a special goal worth extra points</li></ul>
									</li>
							</ul>
						<li>Art Changes
							<ul class="bullet"><li>New view model animations</li></ul>
							<ul class="bullet"><li>The HUD provides more information about goal type and status</li></ul>
							<ul class="bullet"><li>Player pips for Spies will reflect cloak and disguise status</li></ul>				
						</li>
						<li>Misc Details
							<ul class="bullet"><li>Added cvar tf_passtime_scores_per_round</li></ul>
							<ul class="bullet"><li>Added tf_glow entity that can be used to enable the glow effect on any entity</li></ul>
							<ul class="bullet"><li>Updated trigger_passtime_ball FGD entry to hide unimplemented features, temporarily</li></ul>		
							<ul class="bullet"><li>Spawnflag added to func_passtime_goal to indicate to the HUD that a goal is unlocked by JACK power</li></ul>		
						</li>
					</ul>	
				</ul>					
		</ul>
		
			<u>Bug fixes</u>
			<ul class="bullet">
			<li>Fixed being able to repeatedly activate taunts before the current one had ended while underwater</li>
			<li>Fixed disguising with the Conniver's Kunai while spotted by a Sentry causing the Sentry to ignore the Spy</li>
			<li>Fixed class change notifications appearing to the enemy team for a period of time after teams swap sides in Competitive Mode</li>
			<li>Fixed a case where players would sometimes see other players carrying the wrong weapon(s)</li>
			<li>Fixed a bug where the Engineer couldn't remove the Sapper from a linked teleporter if was building (not active)</li>
			<li>Fixed a bug where the Engineer was able to heal a linked teleporter while it was building. This now matches the behavior for the other buildings.</li>
			<li>Updated several hats to fix the Sniper not removing them while taunting</li>
			<li>Fixed hiding the Pyro's head when using the second style of the Fear Monger</li>
			<li>Fixed using the wrong ozfortress medals for divisions other than Premier</li>
			<li>Fixed a few UGC tournament medals using the wrong names</li>
			<li>Fixed the Engineer's missing pelvis hitbox</li>
			<li>Thanks to Justin G., aka sigsegv, for these reports
				<ul class="bullet"><li>Fixed airblasts sometimes sending reflected projectiles toward friendly players, buildings, and projectile shields</li></ul>
				<ul class="bullet"><li>Fixed demo record/read stringtables not writing/reading past 512kb</li></ul>
				<ul class="bullet"><li>Fixed inconsistency between items that use mini-crit buff (TF_COND_ENERGY_BUFF)</li></ul>
				<ul class="bullet"><li>Fixed multiple Mann vs. Machine bugs caused by bot AI functions being executing after the bots have died</li></ul>
				<ul class="bullet"><li>Fixed a bug related to lunchbox items, where the ammo bar can be drained in cases when it shouldn't be</li></ul>
				<ul class="bullet"><li>Fixed Medics sometimes instantly reviving players in Mann vs. Machine</li></ul>
			</li>
		</ul>
		
		
		<u>Performance</u>
		<ul class="bullet">
			<li>Fixed several cases of loading and rendering hitches that would cause abrupt changes in FPS</li>
			<li>Improved Flamethrower flame performance in Mann vs. Machine on clients and servers by up to +500%</li>
			<li>VGUI .res files are now cached after loading, reducing framerate hitches. Custom HUD authors can disable this by setting vgui_cache_res_files to 0.</li>
			<li>Improved rendering performance of zombies</li>
			<li>Improved VGUI font performance</li>
		</ul>
		
		<u>Custom HUD Versioning</u>
		<ul class="bullet">
			<li>In order to reduce crashing and broken UI elements after updates, custom HUDs now must explicitly specify the version of the TF UI with which they are compatible.</li>
			<li>Custom HUD files must now be installed as add-ons under the tf/custom/ folder.  Loose .res files extracted to the game directory will not be loaded.</li>
			<li>Custom HUDs must now contain a file named "info.vdf" in their root folder, with the key "ui_version" set to the TF UI version with which they are compatible.</li>
			<li>The current TF UI version is 1.</li>
			<li>Non-HUD add-ons under tf/custom/ will continue to work without an "info.vdf" file, but may not load .res files.</li>
		</ul>
		
		<u>Mann Co. Store</u>
		<ul class="bullet">
			<li>Added the Competitive Matchmaking Pass</li>
			<li>Added new class starter packs</li>
			<li>Added new key-less cosmetic crates</li>
			<li>Revamped main store pages</li>
			<li>Added a spotlight item</li>
			<li>Added ability to mouse over and preview items in a bundle/crate/collection</li>
			<li>Adjusted some weapon prices for consistency</li>
		</ul>
		
		<u>Maps</u>
		<ul class="bullet">
			<li>cp_granary
				<ul class="bullet"><li>Added new ammo pack locations in final and yard areas</li></ul>
				<ul class="bullet"><li>Clips and blockbullets added to stairs</li></ul>
			</li>
			<li>cp_badlands
				<ul class="bullet"><li>Final cap point capture time increased to 2 seconds (from 1 second)</li></ul>
				<ul class="bullet"><li>Clips and blockbullets added to stairs</li></ul>
			</li>			
			<li>cp_foundry
				<ul class="bullet"><li>Reduced ammo kit in final cap entry room (attackers' side) to medium</li></ul>
				<ul class="bullet"><li>Slightly reduced attacking team's spawn time when working on final</li></ul>
			</li>
			<li>pl_upward
				<ul class="bullet"><li>Fixed an exploit where players could sometimes build sentries in the cliffside by capture point 3</li></ul>
			</li>			
			<li>pl_thundermountain
				<ul class="bullet"><li>Fixed the first and second cap points of stage 3 each awarding 2 points to the capturing team instead of 1</li></ul>
			</li>		
			<li>Mannpower
				<ul class="bullet"><li>All official maps now set Mannpower mode without requiring the use of the tf_powerup_mode convar</li></ul>
			</li>											
		</ul>
		
		<u>Game Balance</u>
		<ul class="bullet">
			<li>Scout
				<ul class="bullet"><li>Crit-A-Cola
					<ul class="bullet"><li>Added Marked-for-Death debuff for 2 seconds after the buff effect expires</li></ul>
					</li></ul>
				<ul class="bullet"><li>The Soda Popper
					<ul class="bullet"><li>Added "On hit: build Hype"</li></ul>
					<ul class="bullet"><li>Removed "build hype by running around"</li></ul>		
				</li></ul>								
				<ul class="bullet"><li>Shortstop
					<ul class="bullet"><li>Added an Alt-fire attack -- reach out and shove someone!</li></ul>
					<ul class="bullet"><li>Removed +healing bonus</li></ul>		
					<ul class="bullet"><li>Reduced pushback vuln to +20% (from +40%)</li></ul>	
				</li></ul>						
				<ul class="bullet"><li>Sun on a Stick
					<ul class="bullet"><li>Take 25% less damage from fire while deployed</li></ul>	
				</li></ul>
			</li>
			<br>
			<li>Soldier
				<ul class="bullet"><li>The Righteous Bison
					<ul class="bullet"><li>Fixed a bug causing players to be hit by the same projectile multiple times, causing the damage dealt to vary wildly</li></ul>
					<ul class="bullet"><li>Per-shot damage has been increased to compensate, resulting in slightly more damage on average
						<ul class="bullet"><li>Point-blank deals 54 damage (previously 20-80)</li></ul>
						<ul class="bullet"><li>Maximum range deals 24 damage (previously 14-56)</li></ul>
					</li></ul>
					<ul class="bullet"><li>Slowed projectile by 30%</li></ul>
					<ul class="bullet"><li>Projectile damage reduced by 25% for each enemy penetrated</li></ul>		
					<ul class="bullet"><li>Updated projectile impact sound</li></ul>				
				</li></ul>
				<ul class="bullet"><li>The Disciplinary Action
					<ul class="bullet"><li>Reduced duration of speed bonus on teammates to 2 seconds (from 3)</li></ul>
				</li></ul>
				<ul class="bullet"><li>The Rocket Jumper
					<ul class="bullet"><li>Updated model/materials and sound</li></ul>
				</li></ul>
			</li>
<br>
			<li>Sniper
				<ul class="bullet"><li>Cozy Camper
					<ul class="bullet"><li>Now requires a full charge to gain flinch resistance</li></ul>
				</li></ul>
				<ul class="bullet"><li>The Sydney Sleeper
					<ul class="bullet"><li>When fully charged, or when making a headshot, now applies Jarate in a radius</li></ul>
					<ul class="bullet"><li>Scoped shots now extinguish teammates</li></ul>
				</li></ul>
				<ul class="bullet"><li>The Cleaner's Carbine
					<ul class="bullet"><li>Removed hidden +10% damage taken multiplier while under the effects</li></ul>
				</li></ul>
			</li>
<br>			
			<li>Heavy
				<ul class="bullet"><li>Natascha and Brass Beast
					<ul class="bullet"><li>20% damage resistance now only applies when spun up and below 50% max health</li></ul>
				</li></ul>
				<ul class="bullet"><li>Huo-Long Heater
					<ul class="bullet"><li>Added -10% damage</li></ul>
					<ul class="bullet"><li>Added +25% increased damage vs. burning players</li></ul>
					<ul class="bullet"><li>Reduced ammo drain to -4/sec (from -6)</li></ul>
					<ul class="bullet"><li>Reduced pulse damage from Ring of Fire to 12 (from 15) due to increased damage vs. burning</li></ul>
				</li></ul>
				<ul class="bullet"><li>Buffalo Steak Sandvich
					<ul class="bullet"><li>Fixed "damage taken" multiplier accidentally being +10%, instead of the listed +25%</li></ul>
				</li></ul>
			</li>
<br>			
			<li>Spy
				<ul class="bullet"><li>Base
					<ul class="bullet"><li>Max speed increased to 320 (from 300)</li></ul>
				</li></ul>
				<ul class="bullet"><li>Enforcer
					<ul class="bullet"><li>Attacks pierce resist and absorb effects from all sources</li></ul>
				</li></ul>
			</li>
<br>
			<li>Pyro
				<ul class="bullet"><li>All Flamethrowers
					<ul class="bullet"><li>Added: Direct damage reduces Medi Gun healing and resist shield effects by 25%
						<ul class="bullet"><li>Medics hear a "healing interrupted" sound when this is happening to their heal target</li></ul>
					</li></ul>
				</li></ul>
				<ul class="bullet"><li>The Manmelter
					<ul class="bullet"><li>Removed (hidden) 20% fire rate penalty</li></ul>
				</li></ul>
			</li>
<br>
			<li>Medic
				<ul class="bullet"><li>Base
					<ul class="bullet"><li>All Mediguns allow the Medic to match the speed of their heal target
					<ul class="bullet"><li>Previously only available on The Quick Fix</li></ul>
					</li></ul>
				</li></ul>
				<ul class="bullet"><li>Quick Fix
					<ul class="bullet"><li>ÜberCharge rate reduced to +15% (from +25%)</li></ul>
				</li></ul>
				<ul class="bullet"><li>Overdose
					<ul class="bullet"><li>Increased movement speed bonus to +20% (from +10%)</li></ul>
					<ul class="bullet"><li>Increased damage penalty to -15% (from -10%)</li></ul>
				</li></ul>
				<ul class="bullet"><li> Dropped Mediguns
					<ul class="bullet"><li>Stored ÜberCharge begins to decay over time after coming to rest</li></ul>
				</li></ul>
			</li>
<br>
			<li>Demoman
				<ul class="bullet"><li>Base
					<ul class="bullet"><li>All boots now require a shield to activate any move speed bonus listed on the item</li></ul>
				</li></ul>
				<ul class="bullet"><li>The Iron Bomber
					<ul class="bullet"><li>Decreased the fuse time to 1.4 seconds (from 2.0)</li></ul>
				</li></ul>
				<ul class="bullet"><li>The Quickiebomb Launcher
					<ul class="bullet"><li>Increased charge time reduction to -70% (from -50%)</li></ul>
					<ul class="bullet"><li>Increased damage bonus for (max) charged shots to +35% (from +25%)</li></ul>
					<ul class="bullet"><li>Increased clip size penalty to -50% (from -25%)</li></ul>
					<ul class="bullet"><li>Removed "Stickybombs fizzle 4 seconds after landing"</li></ul>
				</li></ul>
				<ul class="bullet"><li>The Sticky Jumper
					<ul class="bullet"><li>Updated model/materials and sound</li></ul>
				</li></ul>
			</li>		
<br>				
			<li>Engineer
				<ul class="bullet"><li>Base
					<ul class="bullet"><li>Level 1 teleporters now cost 50 metal (previously 125)</li></ul>
				</li></ul>
				<ul class="bullet"><li>Widowmaker
					<ul class="bullet"><li>Damage increased +10% when attacking the same target as your sentry</li></ul>
				</li></ul>
				<ul class="bullet"><li>Eureka Effect
					<ul class="bullet"><li>Reduced "50% less metal from Dispensers and Pickups" to 20%</li></ul>
					<ul class="bullet"><li>Added "Teleporters cost 50% less metal"</li></ul>
				</li></ul>
				<ul class="bullet"><li>The Short Circuit
					<ul class="bullet"><li>Base projectile attack is -10 metal (hit or miss) and then add -5 metal for each projectile destroyed (sometimes a buff, sometimes a nerf)</li></ul>
				</li></ul>
				<ul class="bullet"><li>The Pomson
					<ul class="bullet"><li>Fixed an exploit with shooting through your own buildings</li></ul>
					<ul class="bullet"><li>Increased close-range damage to 72 (from 62)</li></ul>
					<ul class="bullet"><li>Reduced long-range damage to 32 (from 42)</li></ul>
					<ul class="bullet"><li>Updated projectile impact sound</li></ul>
				</li></ul>			
			</li>		
			
		</ul>

	
		</div>
    </div>
</div>
<div id="day2_bg_11">
    <div id="day2_container_11">
    </div>
</div>
		

	
</body>
</html>
