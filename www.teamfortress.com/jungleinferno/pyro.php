<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
	<link rel="shortcut icon" href="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
    <link href="pyro.css?v=2" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="magnific-popup.css">
	<title>TF2 - Jungle Inferno - Pyro</title>
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/jquery.spritely.js"></script>
	<script src="https://steamcdn-a.akamaihd.net/apps/dota2/javascript/static/jquery.cycle.all.js?v=253" type="text/javascript"></script>
	<script src="jquery.magnific-popup.js" type="text/javascript"></script>
	<meta property="og:title" content="Team Fortress 2 - Jungle Inferno - Pyro" />
	<meta property="og:type" content="game" />
	<meta property="og:url" content="https://www.teamfortress.com/jungleinferno/" />
	<meta property="og:image" content="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/jungleinferno_facebook.jpg">
	<meta property="og:site_name" content="teamfortress.com" />
	<meta property="fb:admins" content="726460592" />

<script>	
// video popups		
		$(document).ready(function() {
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});
});
</script>
</head>
<body>
<div id="fb-root"></div>
<div id="bg_01">
	
	<div id="container_01" class="container">
		<div class="content">
			
			<video muted autoplay="" loop="" preload="auto">
				<source type="video/webm" src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/tf_inferno_loop_04.webm">
			</video>
			
			<a href="../index.html" id="logoLink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="255" height="67" /></a>
			
			<a href="https://www.teamfortress.com/jungleinferno" id="d1offlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>

			<a href="campaign.php" id="d2offlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>

			<a href="pyro.php#" id="d3onlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>

			<a href="notes.php" id="d4offlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>
			
			<div id="plantstop">
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/plantstop_pyro.png">
			</div>			
		</div>
    </div>
	
</div>
<div id="bg_02">
    <div id="container_02">
	</div>
</div>

<div id="bg_03">
    <div id="container_03">
		
    </div>
</div>
<div id="bg_04">
    <div id="container_04">
        
    </div>
</div>
<div id="bg_05">
    <div id="container_05">
		
    </div>
</div>
<div id="bg_06">
    <div id="container_06">

    </div>
</div>
<div id="bg_07">
    <div id="container_07">
        <div id="popup_gasblast">
			<a class="popup-youtube" href="https://www.youtube.com/watch?v=MXGHUpt4oSI"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/trans.gif" width="240" height="135" /"></a>
		</div>
    </div>
</div>
<div id="bg_08">
    <div id="container_08">
		
    </div>
</div>

		

	
</body>
</html>