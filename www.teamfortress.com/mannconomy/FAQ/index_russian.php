<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - Обновление Манн-кономики</title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro">Обычно, в преддверии выхода крупного обновления для TF2, мы стараемся не рассказывать вам много информации, зная, что фанаты и сами смогут всё понять, зайдя в игру. С таким крупным обновлением, как «Обновление Манн-кономики» мы бы хотели расставить все точки над i и не давать расплывчатые описания новвоведениям, ведь теперь дело касается денег.</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="На страницу обновления" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="На страницу TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top">
        	<div id="FAQ_top_content">        	
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        		<h1>О МАГАЗИНЕ</h1>
           		<h2>В: Что же такое магазин «Maнн Ко»?</h2>
                О: Магазин «Maнн Ко» позволяет игрокам покупать, обмениваться и изменять вещи прямо в игре, используя свой Steam-кошелек. 
            	<br /><br />
                <h2>В: Что такое Steam-кошелек?</h2>
                О: Теперь в Steam появился кошелек. Вы можете положить деньги, которые могут быть потрачены либо на игровые предметы, вроде тех, что мы сейчас предлагаем в магазине «Maнн Ко», либо на полноценные игры из магазина Steam.
            	<br /><br />
           		<h2>В: Цена на предметы установлена в очках или какой-то другой непривычной мне валюте?</h2>
                О: Нет, Steam-кошелек использует реальные валюты (доллары/фунты стерлингов/евро).
            	<br /><br />
                <h2>В: Могу ли я положить в свой кошелек такую сумму, какую захочу?</h2>
                О: Да. Мы не будем заставлять вас класть какие-то суммы. Вы можете пополнить кошелек на то количество денежных знаков, которое сочтете нужным. Тем не менее, существует минимальный лимит на внесение денег в кошелек Steam, равный $5 / £4 / 5€. Эта мера позволила снизить нам и вам затраты на перевод в систему. 
            	<br /><br />
                <h2>В: Могу ли я обмениваться или расплавлять предметы, которые я приобрел?</h2>
                О: Нет. Вы можете обмениваться и использовать в создании вещей только те предметы, которые вы получили в игре (смотрите ниже, раздел «Обмен и изменение вещей»). Такие ограничения введены временно: пока мы не будем четко представлять, какое влияние на игру окажет обмен купленными предметами. 
            	<br /><br />
                <h2>В: Платежеспособны ли мои деньги из «TF2» где-нибудь ещё в Steam?</h2>
                О: Да. Деньги из вашего Steam-кошелька могут быть потрачены на любой товар из магазина Steam.
                <br /><br />
                <h2>В: Вы будете продавать только предметы, созданные Valve? А как же предметы, созданные сообществом?</h2>
                О: В магазине будут продаваться оба типа предметов. Кроме того, создатели этих предметов будут получать свой процент с продаж их творений! Чуть позже мы выложим более подробную информацию о том, как это работает. 
            	<br /><br />
                <h2>В: Я член сообщества, и мой предмет попал в игру. Почему его всё ещё нету в магазине «Maнн Ко»?</h2>
                О: Сначала мы будем продавать только вещи победителей конкурса «Polycount», чтобы точно понять, насколько успешно работает наша система. Как только мы наладим систему выплат членам сообщества, ваши предметы сразу появятся в магазине. Мы введем эту функцию тогда, когда она будет готова.
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content">
            <h1>ЧТО ИЗМЕНИТСЯ?<i> (А ЧТО ОСТАНЕТСЯ ТЕМ ЖЕ?)</i></h1>
            <h2>В: Значит ли это, что обновлений больше не будет? Теперь вы не будете выпускать бесплатный контент?</h2>
            О: Будем. В наших планах — продолжать работу над TF2 также, как мы это делали до сих пор, добавляя бесплатные карты, игровые режимы, новые возможности и многое другое. Магазин «Maнн Ко» — это просто иной способ получения предметов, которые любой игрок может заработать самостоятельно в игре. 
            <br /><br />
            <h2>В: Собираетесь ли вы вводить другой платный контент, например карты?</h2>
            О: Нет, мы не собираемся этого делать. Мы не хотим разделять игроков на группы, которые не смогут играть между собой.
            <br /><br />
            <h2>В: Придется ли мне тратить свои деньги на то, чтобы быть такими же сильными, как и мои противники?</h2>
            О: Нет. Любые предметы, влияющие на геймплей и декоративные вещи, можно будет получить просто играя, случайным образом. 
            <br /><br />
            <h2>В: Вы изменили частоту выпадения уже существующих предметов?</h2>
            О: Нет. Все осталось таким же.
            <br /><br />
            <h2>В: Почему все мои предметы получили статус «винтажных»?</h2>
            О: Мы хотели дать возможность купить редкие предметы, но также мы бы хотели наградить тех, кто собирал свои предметы «старыми способами». Именно поэтому мы решили сделать все старые предметы «винтажными». Теперь редкие в прошлом предметы станут ещё ценнее, ведь такой статус не получит больше ни один предмет.
            <br /><br />
            <h2>В: А что с предметами, которые я получу в будушем? Они как-то будут отличаться от купленных?</h2>
            О: Пока что мы не собираемся вводить какие-то визуальные различия между такими предметами. Тем не менее, купленные вещи нельзя переплавить или обменять, поэтому у предметов, получаемых обычным путем, все же больше плюсов.
            <br /><br />
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
            <h1>ОБМЕН И ИЗМЕНЕНИЕ ПРЕДМЕТОВ</h1>
            <h2>В: Могу ли я теперь меняться предметами с другими игроками?</h2>
            О: Можете. Мы добавили в игру долгожданную функцию обмена предметов.
            <br /><br />
            <h2>В: Я могу меняться любыми предметами?</h2>
            О: Нет, не любыми. Некоторые предметы из ограниченных серий нельзя обменять. Кроме того, купленные вещи тоже нельзя никому передать (подробнее в разделе «О магазине»)
            <br /><br />
            <h2>В: Могу ли я изменять свои предметы?</h2>
            О: Можете, можете. В дополнение к уже существующей системе предметов мы создали новые вещи, которые позволят вам изменять предметы еще сильнее. Например, вы запросто сможете поменять имя предмета или его цвет. В каталоге Mann Co вы сможете найти список доступных к изменению предметов и способы их изменения. Кроме того, мы удвоили объем ваших рюкзаков, чтобы каждый топор, шляпа или другая забавная вещица могла остаться при вас.
            <br /><br />
            <h2>В: Можно ли мне просто отдать кому-нибудь предметы? Или же им обязательно нужно что-то отдать мне взамен?</h2>
            О: Да, вы можете дарить предметы другим игрокам. Им не придется отдавать что-то взамен.
            <br /><br />
            <h2>В: Могу ли я создавать новые предметы, с конкурса «Polycount»? У них тоже есть свои рецепты?</h2>
            О: Да, их можно получить в кузнице. Шаблон рецепта у них такой же, как и у остальных предметов, созданных сообществом игроков (ковка двух орудий).
            <br /><br />
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
