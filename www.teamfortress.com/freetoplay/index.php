<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - Free-to-Play</title>
<link rel="stylesheet" type="text/css" href="../main_new.css" />
	<link rel = "stylesheet" type = "text/css" href = "f2p.css?v=2" />
<script language="JavaScript" type="text/JavaScript">

function addBookmark(title,url) {
if (window.sidebar) {
window.sidebar.addPanel(title, url,"https://www.teamfortress.com/");
} else if( document.all ) {
window.external.AddFavorite( url, title);
} else if( window.opera && window.print ) {
return true;
}
}

<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>

<div id = "pageContainer">
	<div id = "background_topper">


<div id = "top_menu">
	<a href = "../index.php" style = "display:block;width:391px;height:70px;position:absolute;z-index:1000;margin-left:260px;"><img src = "../images/trans.gif" width = "391px" height = "70px" /></a>
	<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/F2P/headline_f2p.png" >
	<a id = "getit1" class = "button" href = "https://store.steampowered.com/app/440/" target="_blank"><img src = "../images/trans.gif" height = "51px" width = "370"/></a>

</div>

<div id = main_content>
	<a id = button href = "index.php#"><img src = "../images/trans.gif" height = "87px" width = "198px"/></a>

		<div id = "topper">
			<div style = "position:absolute;margin-left:160px;top:266px;"><iframe width="640" height="390" src="https://www.youtube.com/embed/eQ8duKs2Plw?hd=1" frameborder="0" allowfullscreen></iframe></div>
			<div id = "caption">There's no catch! Play as much as you want, as long as you like!</div>
			<div id = "learnmore">
				<a href = "faq.php" class = "button" id = "faq"><img src = "../images/trans.gif" width = "198px" height = "87px"/></a>
				<a href = "../index.php" class = "button" id = "tf2com"><img src = "../images/trans.gif" width = "198px" height = "87px"/></a>
				<a href = "https://store.steampowered.com/app/440/" target="_blank" class = "button" id = "free"><img src = "../images/trans.gif" width = "198px" height = "87px"/></a>
				<a href = "https://www.metacritic.com/game/pc/team-fortress-2" target="_blank" class = "button" id = "metacritic"><img src = "../images/trans.gif" width = "186px" height = "187px"/></a>
			</div>
		</div>

	<div id = "body_image">
		<div id = "team">
			One of the most popular online action games of all time, Team Fortress 2 delivers constant updates&mdash;new game modes, maps, equipment and, most importantly, hats. Nine distinct classes provide a broad range of tactical abilities and personalities, and lend themselves to a variety of player skills.
			<br />
			<a href = "../movies.php">Meet the team &gt;</a>
		</div>

		<div id = "environment">
			No matter what your style and experience, we've got a character for you. Detailed training and offline practice modes will help you hone your skills before jumping into one of TF2's many game modes, including Capture the Flag, Control Point, Payload, Arena, King of the Hill and more.
		</div>

		<div id = "customize">
			There are hundreds of weapons, hats and more to collect, craft, buy and trade. Tweak your favorite class to suit your gameplay style and personal taste. You don't need to pay to win&mdash;virtually all of the items in the Mann Co. Store can also be found in-game.
			<br />
			<a href = "../history.php">Look back on all the fun &gt;</a>
		</div>
	</div>

	<div id = "bottom_piece">
		<a id = "getit2" class = "button" href = "https://store.steampowered.com/app/440/" target="_blank"><img src = "../images/trans.gif" height = "51px" width = "326px"/></a>
		<div id = "share_container">
			
			<a href = "javascript:var%20d=document,f='https://www.facebook.com/share',l=d.location,e=encodeURIComponent,p='.php?src=bm&v=4&i=1308789539&u='+e(l.href)+'&t='+e(d.title);1;try{if%20(!/^(.*\.)?facebook\.[^.]*$/.test(l.host))throw(0);share_internal_bookmarklet(p)}catch(z)%20{a=function()%20{if%20(!window.open(f+'r'+p,'sharer','toolbar=0,status=0,resizable=1,width=626,height=436'))l.href=f+p};if%20(/Firefox/.test(navigator.userAgent))setTimeout(a,0);else{a()}}void(0)" id = "facebook" class = "button"><img src = "../images/trans.gif" height = "60px" width = "60px"/></a>
			<a href = "https://twitter.com/intent/tweet?source=webclient&text=Look%2C+TF2+is+now+free%21+www.tf2.com" id = "twitter" class = "button"><img src = "../images/trans.gif" height = "60px" width = "60px"/></a>
			<a href = "https://www.stumbleupon.com/badge/?url=www.tf2.com" id = "stumble" class = "button"><img src = "../images/trans.gif" height = "60px" width = "60px"/></a>
			<a href = "https://reddit.com/submit?url=www.tf2.com" id = "reddit" class = "button"><img src = "../images/trans.gif" height = "60px" width = "60px"/></a>
		</div>
		<a href = "https://store.steampowered.com/app/440/" target="_blank" style = "display:block;width:177px;height:51px;position:absolute;z-index=1000;margin-left:297px;top:2280px"><img src = "../images/trans.gif" width = "177px" height = "51px" /></a>
	</div>



</div>


	<div id = "footer">
		<div id = "footer_logo"><a href = "https://www.valvesoftware.com/"><img src = "../images/footer_logo.png" /></a></div>
		<div id = "copyright">&#169 Valve Corporation, all rights reserved. Valve, the Valve logo, Steam, the Steam logo, Team Fortress, the Team Fortress logo are trademarks and/or registered
trademarks of Valve Corporation.</div>


	</div>

		</div>

</div>

<div id = "bg_bottom"></div>


</body>
