<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
	<link rel="shortcut icon" href="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
    <link href="notes.css?v=2" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="magnific-popup.css">
	<title>TF2 - Jungle Inferno - Notes</title>
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/jquery.spritely.js"></script>
	<script src="https://steamcdn-a.akamaihd.net/apps/dota2/javascript/static/jquery.cycle.all.js?v=253" type="text/javascript"></script>
	<script src="jquery.magnific-popup.js" type="text/javascript"></script>
	<meta property="og:title" content="Team Fortress 2 - Jungle Inferno - Notes" />
	<meta property="og:type" content="game" />
	<meta property="og:url" content="https://www.teamfortress.com/jungleinferno/" />
	<meta property="og:image" content="./image/jungleinferno_facebook.jpg" />
	<meta property="og:site_name" content="teamfortress.com" />
	<meta property="fb:admins" content="726460592" />


</head>
<body>
<div id="fb-root"></div>
<div id="bg_01">
	
	<div id="container_01" class="container">
		<div class="content">
			
			<video muted autoplay="" loop="" preload="auto">
				<source type="video/webm" src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/tf_inferno_loop_04.webm">
			</video>
			
			<a href="../index.html" id="logoLink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="255" height="67" /></a>
			
			<a href="https://www.teamfortress.com/jungleinferno" id="d1offlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>

			<a href="campaign.php" id="d2offlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>

			<a href="pyro.php" id="d3offlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>

			<a href="notes.php#" id="d4onlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>
			
			<div id="plantstop">
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/plantstop_notes.png?v=2" />
			</div>			
		</div>
    </div>
	
</div>
<div id="bg_02">
    <div id="container_02">
	</div>
</div>

<div id="bg_03">
    <div id="container_03">
		
    </div>
</div>
<div id="bg_04">
    <div id="container_04">
        
    </div>
</div>
<div id="bg_05">
    <div id="container_05">
		
    </div>
</div>
<div id="bg_06">
    <div id="container_06">

    </div>
</div>
<div id="bg_07">
    <div id="container_07">
        
    </div>
</div>
<div id="bg_08">
    <div id="container_08">
		
    </div>
</div>
<div id="bg_09">
    <div id="container_09">
		
    </div>
</div>
<div id="bg_10">
    <div id="container_10">
		
    </div>
</div>
<div id="bg_11">
    <div id="container_11">
		
    </div>
</div>
<div id="bg_12">
    <div id="container_12">
		
    </div>
</div>
<div id="bg_13">
    <div id="container_13">
		
    </div>
</div>

		

	
</body>
</html>