<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - Das Mann-Conomy Update </title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
<div id="FAQ_intro">Normalerweise veröffentlichen wir vor einem Update so wenig Informationen wie möglich, weil wir wissen, dass die Fans nach der Veröffentlichung alles selbst herausfinden. Bei so einem anspruchsvollen Projekt wie dem „Mann-Conomy-Update“, welches auch echtes Geld ins Spiel bringt, gehen wir lieber auf Nummer sicher und erklären alle wesentlichen Fakten in diesem FAQ.</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Zurück zur Update Seite" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Zurück zu TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top_german">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        		<h1>ÜBER DEN SHOP</h1>
<h2>Q: Was genau ist der Mann Co. Shop?</h2>
A: Der Shop ist aus dem Spiel selbst abrufbar und gibt Spielern die Möglichkeit mit ihrem Steam-Guthaben mit Gegenständen zu handeln, die zu kaufen oder sie anzupassen.
            	
                <h2>Q: Was ist ein Steam-Guthaben?</h2>
A: Steam verfügt seit kurzem über ein Guthabensystem. Man kann reales Geld einzahlen um dann damit Items in Spielen, wie in TF2s Mann-Co. Shop, oder komplette Spiele im Steam-Shop zu kaufen. 
            	
<h2>Q: Sind die Preise im Mann Co. Shop in Punkten oder in irgendeiner anderen virtuellen Währung angegeben?</h2>
                A: Nein, das Steam-Guthaben, und somit die Preise im Onlineshop, sind in denselben Währung angegeben, die Steam momentan unterstützt (US Dollar - $, Britisches Pfund - £, Euro - €).
            	
                <h2>Q: Kann ich selbst bestimmen, wie viel Geld genau auf mein Steam-Guthaben soll?</h2>
A: Ja. Wir zwingen niemanden dazu, bestimmte Festbeträge zu kaufen. Man kann jeden beliebigen Betrag einzahlen den man will, um genau den Betrag auf das Steam-Guthaben einzuzahlen, den man für den gewünschten Gegenstand zahlen will. Beachten Sie jedoch, dass bei einer jeden Einzahlung ein Mindestbetrag von 5$ / £4 / 5€ eingezahlt werden muss. Dies dienen dazu, die Kosten für die Transaktionen auf ein Minimum zu halten.
            	
<h2>Q: Kann ich meine gekauften Gegenstände mit anderen Spielern tauschen oder auch selbst zusammenbasteln?</h2>
A: Kurz gesagt: Nein. Sie können lediglich im Spiel gefundene Gegenstände zum Handel oder zum Herstellen anderer Gegenstände nutzen (weitere Informationen bei Handel & Anpassung). Wir haben diese Regeln fürs Erste so festgelegt, bis wir festgestellt haben, was ein Handel mit gekauften Gegenständen bewirken würde. 

            	
                <h2>Q: Ist mein TF2-Geld auch anderweitig nutzbar?</h2>
A: Ja. Das Steam-Guthaben kann auch dafür verwendet werden, um Produkte im Steam-Shop zu kaufen.
                
<h2>Q: Sind im Mann Co. Shop nur von Valve erstellte Gegenstände verfügbar? Was ist mit den von der Community erstellten Gegenständen?</h2>
A: Im Mann Co. Shop sind beide Arten von Gegenständen erhältlich. Darüber hinaus erhalten die Schöpfer der Community-Gegenstände einen Anteil vom Gewinn der Objekte, die sie kreiert haben. Genauere Information bezüglich des Gewinnanteils werden in Kürze folgen.
			
<h2>Q: Ich bin einer der Personen, die einen Community-Gegenstand kreiert und eingesendet hat. Waum ist mein Gegenstand noch nicht im Mann Co. Shop erhältlich?</h2>
A: Zunächst werden ausschließlich die Waffen aus dem Polycount-Pack der Community verkauft, da es bei der Problembehebung von Vorteil ist, nicht zu viele Angebote im Shop zu haben. Wenn das System zur Bezahlung der Ersteller dann komplett aufgebaut haben, werden wir den Shop mit mehr Gegenständen der Community erweitern. Dies soll so früh wie möglich geschehen. 
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content_german">
<h1>WAS ÄNDERT SICH? <i> (UND WAS BLEIBT GLEICH?)</i></h1>
<h2>Q: Bedeutet es, dass es in Zukunft keine Updates mehr für Team Fortress 2 gibt?</h2>
A: Nein. Wie planen auch Zukunft das Spiel mit Updates zu verbessern sowie neue Maps, Spielmodi und neue Features hinzuzufügen. Der Mann Co. Shop stellt lediglich eine Alternative dar, um Gegenstände außerhalb des Gameplays zu erhalten.
            
<h2>Q: Werden in Zukunft auch für andere Dinge, wie etwa Karten, zum Kauf angeboten?</h2>
A: Nein. Wie wollen nicht, dass sich Spieler in Gruppen aufteilen, nur weil jeder eine andere Karte oder Spielmodus gekauft hat. Um dies zu verhindern, wird es derartigen Inhalt nicht im Mann Co. Shop zu kaufen geben.
            
<h2>Q: Muss ich nun Geld ausgeben, um mithalten zu können?</h2>
A: Nein. Jeder Gegenstand, der das Gameplay beeinflusst, sowie die meisten kosmetischen Gegenstäde, wie etwa Hüte, werden nach wie vor durch Spielen erhältlich sein. 
            
            <h2>Q: Wurde durch das Update das Finden von Gegenständen erschwert?</h2>
A: Nein. Der Zeitintervall, in dem Spieler Gegenstände während des Spielens finden, wurde nicht verändert. Man kann weiterhin Gegenstände finden, wenn man regelmäßig das Spiel spielt.
            
<h2>Q: Warum sind alle meine Gegenstände plötzlich zu „klassischen“ Gegenständen geworden?</h2>
A: Durch die Option, Gegenstände im Mann Co. Shop zu kaufen, ist so manche Rarität nicht mehr so schwer zu ergattern. Manche Spieler haben jedoch eben diese Rarität vor dem Update auf altmodischem Wege, sei es durch Errungenschaften oder durchs Herstellen, erhalten. Um deren Bemühungen im Vergleich zu den erkauften Raritäten nicht umsonst gewesen zu lassen, wurde jeder Gegenstand beim Update in eine „klassische“ Version umgewandelt. Diese Gegenstände sind einzigartig und es wird in Zukunft keine weiteren „klassischen“ Versionen mehr geben. Auf diese Weise bleiben die ehemals raren Gegenstände weiterhin rar, weil sie nun zu einer limitierten Version gehören.
            
            <h2>Q: Was ist mit Gegenständen, die ich in Zukunft erhalten werde? Werden die anderen Spieler in der Lage sein, diese von den gekauften Gegenständen zu unterscheiden?</h2>
A: Momentan gibt es keine konkreten Pläne, diesen Unterschied optisch darzustellen. Gekaufte Gegenstände sind allerdings nicht selber herstellbar, noch lässt sich mit ihnen Handeln, sodass es durchaus noch einen Vorteil hat, Gegenstände auf dem „klassischen“ Weg zu erhalten. 
            
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
            <h1>TAUSCHSYSTEM & PERSÖNLICHE ANPASSUNGEN</h1>
            <h2>Q: Kann ich Gegenstände mit anderen Spielern tauschen?</h2>
            A: Ja. Auf Wunsch vieler Spieler gibt es nun die Möglichkeit, Waffen und Hüte mit seinen Freunden zu tauschen.
            <br /><br />
            <h2>Q: Sind alle Gegenstände tauschbar?</h2>
A: Nein. Gegenstände, die aus dem Mann Co. Shop gekauft wurden, sind nicht tauschbar. Auch sind Gegenstände, die nur für eine bestimmte Zeit erhältlich sind, sind nicht tauschbar (Siehe „ÜBER DEN SHOP“ für weitere Informationen).
            <br /><br />
            <h2>Q: Kann ich meine Gegenstände anpassen?</h2>
A: Ja. Neben den bereits existierenden Anpassungsmöglichkeiten gibt es die Möglichkeit, mithilfe von speziellen Gegenständen den Namen oder die Farbe zu verändern. Im Mann Co. Shop lässt sich einfach herausfinden, welche Gegenstände angepasst werden können und wie dies funktioniert. Ebenso haben wir den Platz in den Rucksäcken verdoppelt, damit nun mehr Raum für Ihre eigenen Gegenstände ist.
            <br /><br />
            <h2>Q: Muss ich etwas tauschen, oder kann ich auch einfach jemandem etwas schenken?</h2>
            A: Ja, man kann einen Gegenstand verschenken, ohne dafür etwas im Gegenzug zu erhalten.
            <br /><br />
<h2>Q: Kann ich die Waffen aus dem Polycount-Pack selber herstellen? Existieren Blaupausen für diese Waffen, ähnlich wie auch bei den anderen Community-Waffen?</h2>
A: Ja, die Gegenstände aus dem Polycount-Pack sind auch herstellbar. Sie verwenden die selben Blaupausen wie anderen Community-Waffe auch. (Beispiel: Kombinieren Sie zwei Gegenstände um eine Waffe zu erhalten).
            <br /><br />
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
