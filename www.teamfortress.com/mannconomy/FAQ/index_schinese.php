<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - The Mann-Conomy Update </title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro">通常的TF2更新，我们更希望（描述及给出的线索）尽量的模糊，因为粉丝们一旦进入游戏玩上几盘就能搞清楚一切了。但像 Mann-conomy 这样有野心的更新，因为涉及到现实的金钱，我们决定把玩弄你们的手段都先放一放，然后告诉大家 我们正在做什么 以及这个补丁到底为了什么。</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="返回更新页面" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="返回 TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        		<h1>关于商店</h1>
           		<h2>Q: Mann Co. 商店是什么？</h2>
                A: 作为可以从TF2游戏中进入的商店，Mann Co. 商店现在允许玩家用他们的 Steam 钱包购买、交易或自定义有史以来最大规模的物品外设。
            	<br /><br />
                <h2>Q: Steam钱包是什么？</h2>

                A: Steam现在增加了一个功能: Steam钱包。它允许你存钱进去——既可以用来买物品，例如TF2的 Mann Co. 商店里的东西，或者 Steam 商店里的所有游戏。
            	<br /><br />
           		<h2>Q: 商店里的东西是用点数来买的吗？还是别的什么游戏里用的货币？</h2>
                A: 不，Steam 钱包用的是现实世界里的真金白银。(USD/GBP/Euros).
            	<br /><br />
                <h2>Q: 我能放任意多的钱到我的 Steam 钱包里吗？</h2>
                A: 正确。我们不会强迫你放一大笔钱到钱包里。你可以往钱包里放刚好够你买的那样道具的具体数额的金钱。 但是，每次往钱包里放的金钱数额至少为 $5 / ￡4 / 5€ 以保证最低交易额度。
            	<br /><br />
                <h2>Q: 我能交易购买的物品或将其参与合成吗？</h2>

                A: 短期来说，不行。你只能交易或者制造那些你游戏过程中得到的物品（详见下面的“交易 & 自定义”部分）。我们会暂时采取这种办法，直到我们能看到交易购买的物品会有哪些影响（我们才会决定是否会这么做）。
            	<br /><br />
                <h2>Q: 我“TF2”的金钱也能用在购买其他 Steam 平台的东西吗？</h2>
                A: 没错。Steam 钱包里的钱可以用来购买 Steam 里的任何东西。
                <br /><br />
                <h2>Q: 你们只会销售 Valve 制造的物品吗？ 那些玩家自制物品呢？</h2>
                A: 商店两者都会销售。但是，社区贡献者会得到他们制造物品的销售额的提成！我们稍后会提供更多这个系统如何工作的信息。
            	<br /><br />
                <h2>Q: 我是一名社区成员，我设计的物品已经存在游戏之中了。为什么它还不在 Mann Co. 商店里？</h2>

                A: 在最开始,我们只会销售 Polycount Pack 社区设计的物品，从而发展出一套和小规模社区贡献者合作的模式。一旦我们掌握了这套支付流程，我们就会把所有社区贡献者设计的武器都囊括到商店中，我们会尽早实现这一点的。
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content">
            <h1>什么会改变？<i> （以及什么还跟以前一样？）</i></h1>
            <h2>Q: 这意味着更新已经结束了吗？你们会停止发放免费更新了吗？</h2>
            A: 不。我们的计划是继续更新TF2——就如同我们一直以来的那样，增加免费地图，游戏模式，新特性等等。Mann Co. 商店仅仅只是另一种获得其他玩家可以通过游戏过程掉落的物品的途径。
            <br /><br />

            <h2>Q: 你们是否打算改变其他东西，比如付费地图？</h2>
            A: 不，我们不打算这么做。我们一直避免这种情况：因为你买了什么东西（比如付费地图）而我没有，所以我们不能一起玩了。
            <br /><br />
            <h2>Q: 我需要买装备才能干得过金币玩家吗？</h2>
            A: 不。任何可以影响游戏的物品，甚至多数妆扮物品，仍然可以通过掉落系统获得。
            <br /><br />
            <h2>Q: 你们调整了现存物品的掉率了吗？</h2>
            A: 不，我们没有调低掉率。
            <br /><br />

            <h2>Q: 为什么我所有的东西都变成了“优良品质（Vintage）”的装备？</h2>
            A: 因为商店的出现，有些过去很稀有的物品变得越来越常见。我们想多给玩家一种获得物品的方式，但是仍然记得以前用“旧方法”得到物品的人，所以我们决定一次性为更新前获得的所有物品打上“Vintage”标记，也就是说，“Vintage”装备未来再也不会获得了。这样，这些旧的物品会依然保持它们的稀有程度（事实上，它们会变得更稀有，因为现在它们已经变成限量版了）。
            <br /><br />
            <h2>Q: 那么我将来得到的物品呢？别人怎么才能将其和购买的物品区分？</h2>
            A: 现在还未有计划让其他玩家能看出这些“区别”。 但是，购买的物品现在还不能交易或者参与合成，所以收藏通过“旧方法”获得的物品仍然有一些好处。
            <br /><br />
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
            <h1>交易 & 自定义</h1>
            <h2>Q: 我可以交易物品吗？</h2>
            A: 是的。我们已经加入了曾被长期要求添加的交易功能，现在你可以和朋友们交换物品了。
            <br /><br />
            <h2>Q: 是不是所有的物品都可以交易？</h2>
            A: 不，不是所有的物品都可以交易。一些有使用次数限制的物品不能交易，购买的物品当前也不能交易（参见上面的“关于商店”部分）。
            <br /><br />

            <h2>Q: 我可以自定义我的物品吗？</h2>
            A: 是的。除过我们现有的自定义过的物品，我们已经创建了更多新物品，这些物品能提供许多自定义属性，像物品名称的颜色、甚至物品配色方案等。Mann Co. 产品目录能够告诉你哪些物品能自定义、你该如何自定义它们等等。我们也把你的背包空间扩大了一倍，你可以有更多的空间才存放新的物品。
            <br /><br />
            <h2>Q: 我可以仅仅给对方送物品吗？还是对方必须得用一些物品来交换？</h2>
            A: 你可以给其他人送“礼物”。他们无须用其他物品来交换。
            <br /><br />
            <h2>Q: 我可以合成 Polycount 物品吗？它们有自己的合成蓝图吗（像其他玩家自制物品一样可以合成）？</h2>
            A: 完全可以的。这些物品都能够制造，它们也有类似其他自制物品一样的蓝图配方。
            <br /><br />
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
