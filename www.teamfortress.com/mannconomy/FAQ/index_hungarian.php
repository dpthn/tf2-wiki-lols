<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - A Mann-Conomy frissítés</title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro">A TF2 frissítésekkel kapcsolatban a megjelenésük előtt általában szeretünk annyira homályosan fogalmazni, amennyire lehet, tudva, hogy a rajongók mindenre rá fognak jönni maguktól, miután beléptek és játszani kezdtek. Ám egy olyan ambiciózus frissítés esetén, mint a Mann-conomy, és mert itt valódi pénzről is szó van, egy pillanatra félre akartuk tenni a kacsintgatást, oldalba bökdösést és poénkodást, és pontosan felvázolni, mit is csinálunk, és miről szól ez a frissítés.</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Vissza a frissítési oldalra" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Vissza a TF2.com oldalra" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        		<h1>AZ ÁRUHÁZRÓL</h1>
           		<h2>K: Mi az a Mann Co. Áruház?</h2>
                V: A TF2-n belül elérhető Mann Co. Áruházban a játékosok Steam Pénztárcájukat használva megvehetik, eladhatják és egyediesíthetik a játék fennállásának legnagyobb számú hozzáadott új tárgyát.
            	<br /><br />
                <h2>K: Mi az a Steam Pénztárca?</h2>
                V: A Steam mostantól rendelkezik egy Steam Pénztárca nevű funkcióval. Ebbe pénzt lehet betenni, amit aztán elkölthetsz játékon belüli tárgyakra, mint például a TF2 Mann Co. Áruházának kínálata, vagy teljes termékekre, például játékokra a Steam áruházban.
            	<br /><br />
           	<h2>K: Az árucikkek árai pontokban, vagy bármi egyéb fura pénznemen vannak megadva?</h2>
                V: Nem, a Steam Pénztárca ugyanazokat a valódi pénznemeket használja, melyeket a Steam jelenleg támogat (USD/GBP/Euró).
            	<br /><br />
                <h2>K: Akkora összeget tehetek a tárcámba, amekkorát akarok?</h2>
                V: Igen. Nem kényszerítünk egy bizonyos nagyságú „pénzköteg” megvételére. Betehetsz pontosan akkora összeget, amennyire a kiválasztott tárgyak megvételéhez szükséged van. Azonban mikor feltöltöd a pénztárcád, a minimálisan a Steam Pénztárcába tehető összeg  5 USD / 4 GBP / 5 EUR, hogy minimlizáljuk a tranzakciós és szolgáltatói díjakat.
            	<br /><br />
                <h2>K: Elcserélhetem a vásárolt tárgyakat, vagy használhatom őket tárgykészítésre?</h2>
                V: Egyelőre nem. Csak olyan tárgyat cserélhetsz el vagy használhatsz tárgykészítésre, amit a játékban szereztél (lásd a Csere és egyediesítés részt lejjebb). Ez a szabály átmenetileg van érvényben, míg nem látjuk, milyen hatása lehet a vásárolt tárgyak cseréjének a Mann-conomy működésére.
            	<br /><br />
                <h2>K: Használható a „TF2 pénzem” máshol a Steamen?</h2>
                V: Igen. A Steam Pénztárcádban levő pénzzel bármit megvásárolhatsz a Steamen.
                <br /><br />
                <h2>K: Csak a Valve által készített tárgyakat fogtok árulni? Mi van a közösség által készített tárgyakkal?</h2>
                V: Az Áruházban mindkét fajta kapható lesz. Ráadásul a közösségi hozzájárulók százalékot fognak kapni az általuk készített tárgyak eladásából! Hamarosan további információt fogunk közzétenni arról, ez pontosan hogyan is fog működni.
            	<br /><br />
                <h2>K: Az általam a közösség tagjaként készített tárgy már bekerült a játékba. A Mann Co. Áruházban miért nincs?</h2>
                V: Kezdetben csak a Polycount Pack közösségi tárgyait fogjuk árusítani, mivel így lehetőségünk lesz csak kis számú közreműködővel együttműködve orvosolni a problémákat. Miután a közreműködők felé történő kifizetés már rendben működik, elkezdjük kiterjeszteni az árukészletet az összes közösségi hozzájárulás körére. Ezt a lehető leggyorsabban tervezzük megtenni.
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content">
            <h1>MI VÁLTOZIK?<i> (ÉS MI MARAD UGYANOLYAN?)</i></h1>
            <h2>K: Ez azt jelenti, hogy a frissítéseknek vége? Mostantól nem adtok ki több ingyenes tartalmat?</h2>
            V: Nem. Úgy tervezzük, továbbra is frissíteni fogjuk a TF2-t, ahogy eddig, ingyenes pályákkal, játékmódokkal, új funkciókkal, és egyebekkel. A Mann Co. Áruház pusztán csak alternatív módja azon tárgyak beszerzésének, melyeket más játékosok játékon belül szereznek meg.
            <h2>K: Fogtok pénzt kérni másfajta tartalomért, mondjuk pályákért?</h2>
            V: Nem, ilyesmit nem tervezünk. Szeretnénk elkerülni, hogy egymással játszani nem tudó csoportokra különítsük el a játékosokat az alapján, hogy ki vett meg valamit, és ki nem.
            <h2>K: Pénzt kell majd költenem, ha versenyképes akarok maradni?</h2>
            V: Nem. Az összes tárgy, ami hatással van a játékmenetre, de még a tisztán csak látványelemként szolgálók is, továbbra is megszerezhetők lesznek egyszerűen csak a játékot játszva.
            <h2>K: Változtattatok a létező tárgyak előfordulási gyakoriságán?</h2>
            V: Nem. Nem tettük nehezebbé a tárgyak játékon belüli megszerezhetőségét.
            <h2>K: Miért lett minden tárgyamból „Klasszikus” (Vintage)?</h2>
            V: Egyes tárgyak, melyek korábban ritkák voltak, gyakoribbá fognak válni, mivel megvásárolhatók. Meg akartuk adni ezt a lehetőséget a játékosoknak, közben azonban továbbra is elismerve azok teljesítményét, akik korábban, a „régi módi szerint” szerezték meg ezeket a tárgyakat. Ezért úgy döntöttünk, elvégzünk egy egyszeri konverziót, mely során az összes régi tárgyat átalakítjuk „Klasszikus” (Vintage) verzióra, melyek a jövőben nem lesznek megszerezhetők. Így ezek a régebbi ritka tárgyak ritkák is maradnak (sőt, igazából még ritkábbak, korlátozott példányszámúak, lesznek).
            <h2>K: Mi van a tárgyakkal, melyeket később szerzek majd? Hogyan fogják azokat mások megkülönböztetni a vásárolt tárgyaktól?</h2>
            V: Jelenleg nem tervezzük, hogy ezt a különbséget mások számára láthatóvá tegyük. Azonban a vásárolt tárgyakat jelenleg nem lehet elcserélni, és nem használhatók tárgykészítésre, vagyis még mindig van előnye annak, ha tárgyaidat a „régi módi szerint” szerzed meg. 
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
            <h1>CSERE ÉS EGYEDIESÍTÉS</h1>
            <h2>K: Mostantól tudok tárgyakat cserélni?</h2>
            V: Igen. Megvalósítottuk a hosszú ideje kért tárgycsere funkciót, szóval mostantól cserélhetsz tárgyakat a barátaiddal.
            <br /><br />
            <h2>K: Minden tárgy cserélhető?</h2>
            V: Nem, nem minden tárgy cserélhető. Egyes korlátozott elérhetőségű tárgyak nem cserélhetők. A vásárolt tárgyak jelenleg szintén nem cserélhetők (lásd az Az áruházról részt).
            <br /><br />
            <h2>K: Egyediesíthetem a tárgyaimat?</h2>
            V: Igen. A jelenlegi egyediesítési lehetőségek mellett újfajta tárgyakat hoztunk létre, melyek nagyobb számú egyediesítést tesznek lehetővé, mint eddig; például el lehet nevezni a tárgyakat, és akár a színüket is meg lehet változtatni. A Mann Co. Katalógusból megtudhatod, mely tárgyakat lehet egyediesíteni, és hogyan lehet ezt megtenni. Ezen felül megdupláztunk a hátizsákban rendelkezésre álló hely nagyságát, hogy legyen hová tenni ezt a sok új holmit.
            <br /><br />
            <h2>K: Tudok egyszerűen csak odaadni valamit valakinek, vagy mindenképp kell adniuk valamit cserébe?</h2>
            V: Igen, tudsz tárgyakat „ajándékozni” másoknak. Nem kell adniuk semmit cserébe.
            <br /><br />
            <h2>K: Használhatom tárgykészítésre a Polycount fegyvereket? Azokhoz is tartoznak receptek, mint a többi közösségi tárgyhoz?</h2>
            V: Igen, használhatók tárgykészítésre. Ugyanolyan jellegű receptjeik vannak (két tárgy kombinálása), mint a többi közösség által készített tárgynak. 
            <br /><br />
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
