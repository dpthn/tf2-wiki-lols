<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - The Mann-conomie Update </title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro">Meestal als we TF2 updaten proberen we alles zo onduidelijk mogelijk te houden, wetende dat onze fans alles uit de kast halen om nou precies te kijken wat er in deze update zit. Met een update zo ambitieus als de Mann-conomie update en omdat het om echt geld gaat, leggen we het knipogen, knikken en lekker maken aan de kant, en leggen we precies uit wat we doen en wat deze update allemaal inhoudt.</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Terug naar Update Page" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Thuis Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Terug naar TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top_dutch">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        		<h1>OVER DE WINKEL</h1>
           		<h2>V: Wat is een Mann Co. Winkel?</h2>
                A: Toegankelijk via TF2, de Mann Co. Winkel laat spelers het kopen, ruilen, en aanpassen toe van de grootste toevoeging van voorwerpen in de geschiedenis van het spel, met het gebruik van hun Steam Portemonnee. 
            	<br /><br />
                <h2>V: Wat is een Steam Portemonnee?</h2>
                A: Steam heeft nu een optie die de Steam Portemonnee heet. Het stelt je in staat om er geld in te zetten, wat dan gebruikt kan worden om spullen te kopen, zoals die in de TF2 Mann Co. Winkel, of zelfs volledige producten in de Steam winkel. 
            	<br /><br />
           		<h2>V: Zijn winkel voorwerpen in punten, of een ander soort valuta?</h2>
                A: Nee, de Steam Portemonnee gebruikt dezelfde munteenheid die ook Steam gebruikt (Amerikaanse dollars/ponden/euro&#39;s).
            	<br /><br />
                <h2>V: Kan ik welk bedrag dan ook in mijn Steam Portemonnee zetten?</h2>
                A: Ja. We dwingen je niet om een vastgesteld bedrag te kopen. Je kan een specifiek bedrag kopen om de voorwerpen te kopen die jij graag wilt. Maar, iedere keer dat je je portemonnee wilt gebruiken, moet je het minimaal met $5 / £4 / €5 opwaarderen om transacties en betalingen om vergoedingen van de aanbieder zo laag mogelijk te houden.
            	<br /><br />
                <h2>V: Kan ik voorwerpen die ik gekocht heb ruilen of ontwerpen?</h2>
                A: Een kort antwoord: nee. Je kan alleen voorwerpen die je gevonden hebt tijdens het spelen, of die je verdiend hebt, ruilen en ontwerpen (zie Ruilen & Aanpassen hier beneden). We hebben dit beleid aangenomen tot we kunnen zien wa thet effect ruilen heeft op gekochte voorwerpen op de Mann-conomie. 
            	<br /><br />
                <h2>V: Is mijn "TF2" geld goed voor andere dingen?</h2>
                A: Ja. Het geld in je Steam Portemonnee kan gebruikt worden overal in de winkel van Steam.
                <br /><br />
                <h2>V: Verkopen jullie alleen Valve-ontworpen voorwerpen? Hoe zit het met community-bijdrages?</h2>
                A: De Winkel gaat beiden verkopen. Daarbij komt dat community bijdragers een percentage ontvangen van de verkopen van de voorwerpen die zij hebben gemaakt! We laten je binnenkort meer weten over hoe dit precies in zijn werk gaat. 
            	<br /><br />
                <h2>V: Ik ben een community lid die een voorwerp heeft bijgedragen en in het spel zit. Waarom zie ik het niet in de Mann Co. Winkel?</h2>
                A: In het begin verkopen we alleen de Polycount Pack community voorwerpen zodat we problemen kunnen uitwerken met een klein nummer van bijdragers. Als we er eenmaal volledig achter zijn hoe dit in zijn werk gaat, breiden we de winkel uit zodat alle community bijdrages er in komen. We proberen dit zo snel mogelijk te doen.
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content">
            <h1>WAT VERANDERT ER?<i> (EN WAT BLIJFT HETZELFDE?)</i></h1>
            <h2>V: Betekent dit dat het allemaal voorbij is? Stoppen jullie met het maken van gratis updates?</h2>
            A: Nee. Ons plan is om door te gaan met het updaten van TF2 zoals we altijd gedaan hebben, het toevoegen van gratis nieuwe mappen, spel modi, andere nieuwe dingen, en nog veel meer. De Mann Co. Winkel is gewoonweg een alternatieve manier om voorwerpen te krijgen die andere spelers verdienen tijdens het spelen.
            <br />
            <h2>V: Gaan jullie geld vragen voor andere dingen, zoals mappen?</h2>
            A: Nee, we hebben geen plannen om dit te doen. Het verspreiden van spelers in groepen die niet samen kunnen spelen, gebaseerd op wie kocht wat, is iets wat we liever ontlopen.
            <br />
            <h2>V: Moet ik geld gaan gebruiken om competitief te blijven spelen?</h2>
            A: Nee. Alle voorwerpen die het spel veranderen, en zelfs de meeste cosmetische voorwerpen, blijven gewoon verkrijgbaar door het spel te spelen. 
            <br />
            <h2>V: Hebben jullie het vinden van nieuwe voorwerpen veranderd?</h2>
            A: Nee. We hebben het niet moeilijker gemaakt om voorwerpen te vinden in het spel.
            <br />
            <h2>V: Waarom zijn al mijn voorwerpen nu ineens "Klassieke" voorwerpen?</h2>
            A: Sommige voorwerpen die zeldzaam waren worden minder zeldzaam als je ze kunt kopen. We willen spelers de optie geven om dit te doen, maar mensen die de voorwerpen op de oude manier hebben gekregen kun je nu herkennen. We hebben besloten dat we eenmalig een verandering doen van alle oude voorwerpen in "Klassieke" versies, die je vanaf nu nooit meer kunt vinden. Op deze manier blijven de oude zeldzame wapens zeldzaam (ze zijn zelfs zeldzamer, omdat je ze niet meer kunt krijgen nu). 
            Some items that used to be rare will become more common when they are available for purchase. We wanted to give players this option, but still recognize people who obtained those items “the old-fashioned way” in the past. So we decided to perform a one-time conversion of all the old items into "Vintage" versions, which will never be attainable in the future. This way, those older rare items remain rare (in fact, they're even rarer, because they're limited editions now).
            <br />
            <h2>V: Hoe zit het met voorwerpen die ik in de toekomst krijg? Hoe kunnen anderen ze onderscheiden van gekochte voorwerpen?</h2>
            A: Er zijn geen plannen om dit onderscheid te laten zien aan andere spelers. Maar, gekochte voorwerpen kan je momenteel niet ruilen of ontwerpen, dus er is nog steeds &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; een voordeel als je je voorwerpen op de "oude manier" krijgt.
            <br />
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
            <h1>RUILEN &amp; AANPASSEN</h1>
            <h2>V: Kan ik voorwerpen nu ruilen?</h2>
            A: Ja. We hebben eindelijk het lang-verwachte Ruilen toegevoegd, zodat je voorwerpen kunt uitwisselen met vrienden.
            <br /><br />
            <h2>V: Kan je alle voorwerpen ruilen?</h2>
            A: Nee, niet alle voorwerpen kun je ruilen. Sommige tijdelijk-beschikbare voorwerpen zijn niet ruilbaar. Gekochte voorwerpen zijn ook niet te ruilen (zie Over de Winkel).
            <br /><br />
            <h2>V: Kan ik mijn voorwerpen aanpassen?</h2>
            A: Ja. Samengaande met onze huidige voorwerp aanpassingen, hebben we nieuwe voorwerpen gemaakt die een groter aantal aanpassingen kunnen maken dan dat we dat in het verleden konden doen. Je kunt nu voorwerpen benoemen en zelfs hun kleurenschema veranderen. De Mann Co. Catalogus laat je zien welke voorwerpen je kunt aanpassen en op welke manier je dat kunt doen. We hebben ook de rugzak ruimte verdubbeld, zodat je genoeg ruimte hebt voor al deze nieuwe spullen.
            <br /><br />
            <h2>V: Kan ik gewoon voorwerpen weggeven aan iemand? Of moeten ze me iets teruggeven?</h2>
            A: Ja, je kan voorwerpen "weggeven" aan anderen. Ze hoeven je niks terug te geven.
            <br /><br />
            <h2>V: Kan ik de Polycount wapens ontwerpen? Hebben ze blauwdrukken net als de andere community voorwerpen?</h2>
            A: Ja, je kunt ze ontwerpen. Ze hebben dezelfde blauwdruk stijl (combineer twee wapens) als de andere community voorwerpen.
            <br /><br />
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
