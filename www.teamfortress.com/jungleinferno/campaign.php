<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
	<link rel="shortcut icon" href="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
    <link href="campaign.css?v=6" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="magnific-popup.css">
	<title>TF2 - Jungle Inferno - Campaign</title>
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/jquery.spritely.js"></script>
	<script src="https://steamcdn-a.akamaihd.net/apps/dota2/javascript/static/jquery.cycle.all.js?v=253" type="text/javascript"></script>
	<script src="jquery.magnific-popup.js" type="text/javascript"></script>
	<meta property="og:title" content="Team Fortress 2 - Jungle Inferno - Campaign" />
	<meta property="og:type" content="game" />
	<meta property="og:url" content="https://www.teamfortress.com/jungleinferno/" />
	<meta property="og:image" content="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/jungleinferno_facebook.jpg">
	<meta property="og:site_name" content="teamfortress.com" />
	<meta property="fb:admins" content="726460592" />
<script>	
// sliders
        $(document).ready(function() {
           $('#v_warpaint_slider').cycle({
                fx:     'scrollHorz', 
                speed:   200, 
                next:   '#v_warpaint_slider_next', 
                prev:   '#v_warpaint_slider_prev', 
                pause:   1,
                pager:   0,
                timeout: 6000
            });
        });

        $(document).ready(function() {
           $('#w_warpaint_slider').cycle({
                fx:     'scrollHorz', 
                speed:   300, 
                next:   '#w_warpaint_slider_next', 
                prev:   '#w_warpaint_slider_prev', 
                pause:   1,
                pager:   0,
                timeout: 6000
            });
        });

</script>

</head>
<body>
<div id="fb-root"></div>
<div id="bg_01">
	
	<div id="container_01" class="container">
		<div class="content">
			
			<video muted autoplay="" loop="" preload="auto">
				<source type="video/webm" src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/tf_inferno_loop_04.webm">
			</video>
			
			<a href="../index.html" id="logoLink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="255" height="67" /></a>
			
			<a href="https://www.teamfortress.com/jungleinferno" id="d1offlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>

			<a href="campaign.php" id="d2onlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>

			<a href="pyro.php" id="d3offlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>

			<a href="notes.php" id="d4offlink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="124px" height="106px" /></a>
			
			<div id="plantstop">
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/plantstop_campaign.png">
			</div>			
		</div>
    </div>
	
</div>
<div id="bg_02">
    <div id="container_02">
	</div>
</div>

<div id="bg_03">
    <div id="container_03">
		<div id="copy_03_01">
    		Buying a <b>Jungle Inferno Campaign Pass</b> gives you immediate access to 36 unique, skill-based contracts, letting you play at your own speed and challenge level. Unlike in previous campaigns, you won’t be waiting around for new contract deliveries — ALL contracts and their objectives are available from the start, and can be unlocked sequentially based on the path you choose and how brave you are. (If you’re not brave at all, the ConTracker also comes with an OFF button.) Buying a pass also changes your Casual Badge to a scary Yeti paw, so other players will know you’re up to serious business.
		</div>	
        <div id="copy_03_02">
            The <b>ConTracker</b> is a handy PDA that, for the first time, lets you customize your campaign! (It’s even equippable as a cosmetic item.) 
            <br /><br />Easily browse through every available contract and check out the rewards. Choose your own path based on the contracts you’re eager to complete. Match the contracts to your play-style with class, weapon and game mode-specific challenges. Tackle primary contract objectives and bonus objectives with varying challenge levels. 
        </div>
    </div>
</div>
<div id="bg_04">
    <div id="container_04">
        <div id="copy_04_01">
            For the first time, attack your contracts with co-op! Friends in your party (and on the same server) can now help you out with your contracts (while you give a helping hand with theirs). Any action they take that fulfills one of your contract goals will count toward the completion of that contract. And vice versa!
        </div>
        <div id="copy_04_02">
            Fulfilling contracts now earns either a campaign-specific item reward or <b>Blood Money</b> to spend on the items you want. Once you’ve purchased a Jungle Inferno Campaign Pass, you’ll start earning Blood Money for completing contracts. Then simply visit the <b>Mercenary Park Gift Shop</b> from your in-game ConTracker PDA and load up on War Paints and War Paint Cases.  (You’ll also get War Paint Case drops every week the Campaign is active.)
        </div>
    </div>
</div>
<div id="bg_05">
    <div id="container_05">
		<div id="copy_05_01">
            Introducing <b>War Paint</b>, which will be replacing campaign weapon drops and weapon cases as of the Jungle Inferno Update. Think of it as a voucher for a unique pattern that you can redeem for an entirely new patterned item—even one you don’t already own. Redeeming a War Paint means you don’t have to sacrifice any existing items in your backpack by modifying them. Simply select from one of almost forty item choices per War Paint, preview your new item, and redeem it! Some War Paints even feature Stat Tracking and Unusual effects. And also making their debut, the first-ever community-made War Paints. 
        </div>
    </div>
</div>
<div id="bg_06">
    <div id="container_06">
        <div id="v_warpaint_slider_container">
            <div id="v_warpaint_slider">    
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_anodized_aloha.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_bamboo_brushed.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_croc_dusted.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_leopard_printed.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_macaw_masked.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_mannana_peeled.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_park_pigmented.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_pina_polished.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_sax_waxed.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_tiger_buffed.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/v_yeti_coated.png" width="309" height="352" />
            </div>  
		<div class="controls" id="v_warpaint_slider_next" href=""></div>
        <div class="controls" id="v_warpaint_slider_prev" href=""></div>       
        </div>

        <div id="w_warpaint_slider_container">
            <div id="w_warpaint_slider">    
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_bank_rolled.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_bloom_buffed.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_bonk_varnished.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_cardboard_boxed.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_clover_camod.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_dream_piped.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_fire_glazed.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_freedom_wrapped.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_kill_covered.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_merc_stained.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_pizza_polished.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_quack_canvassed.png" width="309" height="352" />
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/jungleinferno/images/warpaint/w_star_crossed.png" width="309" height="352" />
            </div>  
		<div class="controls" id="w_warpaint_slider_next" href=""></div>
        <div class="controls" id="w_warpaint_slider_prev" href=""></div>       
        </div>
    </div>
</div>
<div id="bg_07">
    <div id="container_07">
    </div>
</div>
<div id="bg_08">
    <div id="container_08">
		<div id="copy_08_01">
            Short on Blood Money? No problem! You’ll win some rewards simply by competing in the campaign and fulfilling contracts. (In fact, some of the rarest Valve-made items available in the Campaign will be completion-based.) 
        </div>
        <div id="copy_08_02">
            This prehistoric pompadour was first spotted by Sir Edmund Hillary on the slopes of Everest, followed immediately by Hillary spotting himself crapping his own pants. 
        </div>
        <div id="copy_08_03">
            Everybody loves beards, but why let your face have all the fun? This hirsute hair suit is like a beard for your arms and torso. 
        </div>
        
    </div>
</div>
<div id="bg_09">
    <div id="container_09">
		<div id="copy_09_01">
            The yeti derives its name from the Tibetan yeh-teh, which translates to “terrifying monster who lives in mountains and wears tiny short pants.
        </div>
        <div id="copy_09_02">
            Now you too can look like Mann Co’s owner and CEO! Specifically the head part, not the marvelously well-muscled below-the-head part. 
        </div>
        <div id="copy_09_03">
            You went to Yeti Park, and all you got was this stupid hat! And dysentery. Display one of these handsome travel mementos in your new home: The bathroom at the hospital.
        </div>
    </div>
</div>
<div id="bg_10">
    <div id="container_10">
		
    </div>
</div>
<div id="bg_11">
    <div id="container_11">
		
    </div>
</div>
<div id="bg_12">
    <div id="container_12">
		
    </div>
</div>

<div id="bg_13">
    <div id="container_13">
		<a href="campaign.php#" id="parroter"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/toughbreak/images/trans.gif" width="285x" height="413px" /></a>
    </div>
</div>

		

	
</body>
</html>