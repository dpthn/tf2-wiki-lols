<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - Aktualizacja Mann-Konomii </title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro">Zazwyczaj podczas aktualizacji TF2 wolimy nic nie 
						ujawniać aż do wypuszczenia aktualizacji, gdyż wiemy, że 
						nasi fani sami się w tym połapią po pewnym czasie 
						spędzonym w grze. Wraz z nadejściem aktualizacji tak 
						ambitnej, jak aktualizacja Mann-Konomia, zwłaszcza że 
						dotyczy rzeczywistych pieniędzy, chcemy jednak 
						zaprzestać na chwilę takich działań oraz wytłumaczyć 
						dokładnie co robimy i czym jest tak właściwie ta 
						aktualizacja.</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Back to Update Page" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Back to TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        		<h1>O SKLEPIE</h1>
           		<h2>P: Czym jest Sklep Mann Co.?</h2>
                O: Dostępny z poziomu TF2 Sklep Mann Co. pozwala teraz graczom 
				na kupno, wymianę i personalizację przedmiotów w dotychczas 
				największej fali dodanych przedmiotów w historii gry dzięki 
				użycia Portfela Steam.
            	<br /><br />
                <h2>P: Czym jest Portfel Steam?</h2>

                O: Steam posiada teraz funkcję o nazwie Portfel Steam. Pozwala 
				ona na przelanie do niego pieniędzy, które mogą być następnie 
				wykorzystane do kupna przedmiotów w grze, jak np. przedmioty 
				kupowane w Sklepie Mann Co. lub do kupna pełnowartościowych 
				produktów, czyli np. gier w sklepie Steam.
            	<br /><br />
           		<h2>P: Czy przedmioty w sklepie kupuje się za punkty lub inne 
				dziwne waluty?</h2>
                O: Nie, Portfel Steam używa prawdziwych pieniędzy obsługiwanych 
				przez Steama (dolary/funty brytyjskie/euro).
            	<br /><br />
                <h2>P: Czy mogę dodać dowolną kwotę pieniędzy do swojego 
				portfela?</h2>
                O: Tak. nie będziemy cię zmuszać do zakupu “pliku” gotówki. 
				Możesz dodać dokładnie tyle pieniędzy, ile potrzebujesz na zakup 
				interesujących cię przedmiotów. Istnieje jednak minimalna stawka 
				w wysokości $5 / £4 / 5€ w celu ograniczenia podatków od 
				transakcji i pośrednika płatniczego do niezbędnego minimum.
            	<br /><br />
                <h2>P: Czy mogę handlować lub wykorzystać do wytwarzania 
				przedmioty, które zakupiłem?</h2>
                O: Mówiąc w skrócie - nie. Możesz handlować i używać do 
				wytwarzania wyłącznie przedmioty, które znalazłeś lub zdobyłeś 
				podczas normalnej gry (zobacz poniższą sekcję Wymiana i 
				personalizacja). Przyjęliśmy jak na razie tę regułę do czasu, 
				kiedy zorientujemy się jaki wpływ na Mann-Konomię może wywierać 
				handel zakupionymi przedmiotami.<br />
                <h2>P: Czy moje pieniądze do TF2 mogę być wykorzystane gdzie 
				indziej na Steamie?</h2>
                O: Tak. Pieniądze z Portfela Steam mogą być wykorzystane do 
				kupna każdej rzeczy na Steamie.
                <br /><br />
                <h2>P: Czy będziecie sprzedawać tylko swoje przedmioty? Co z 
				przedmiotami wykonanymi przez społeczność?</h2>
                O: Sklep będzie sprzedawać oba rodzaje przedmiotów. Dodatkowo 
				autorzy tych przedmiotów będą otrzymywać pewien odsetek od kwoty 
				przedmiotów, które sami stworzyli! Wkrótce zamieścimy więcej 
				informacji o tym, jak dokładnie ma to działać.
            	<br /><br />
                <h2>P: Jestem autorem przedmiotu, który został dodany do gry. 
				Czemu nie znajduje się on w Sklepie Mann Co.?</h2>
                O: Początkowo będziemy sprzedawać wyłącznie przedmioty od 
				społeczności z Paczki Polycount, co pozwoli nam na rozkręcenie 
				się z małą liczbą autorów. Po tym, jak opanujemy płatności dla 
				twórców, rozszerzymy sklep, aby zawierał wszystkie przedmioty od 
				społeczności. Planujemy zrobić to tak szybko, jak to tylko 
				możliwe.
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content">
            <h1>CO SIĘ ZMIENI?<i> (A CO POZOSTANIE TAKIE SAME?)</i></h1>
            <h2>P: Czy to oznacza koniec aktualizacji? Zamierzacie zaprzestać 
			wydawania darmowej zawartości gry?</h2>
			O: Nie. Będziemy nadal aktualizować TF2 tak samo, jak wcześniej, 
			dodając darmowe mapy, tryby gry, nowe właściwości i wiele więcej. 
			Sklep Mann Co. jest po prostu innym sposobem na zdobycie 
			przedmiotów, które inni gracze mogą zdobyć podczas normalnej gry.
            <br /><br />

            <h2>P: Czy zamierzacie pobierać opłaty za inną zawartość, jak np. 
			mapy?</h2>
            O: Nie, nie planujemy czegoś takiego. Chcemy uniknąć przydzielania 
			się ludzi do grup, które nie mogą grać razem, a które uzależnione są 
			od tego, kto co kupił.
            <br /><br />
            <h2>P: Czy będę musiał wydawać pieniądze na to, aby wciąż grać 
			ligowo?</h2>
            O: Nie. Wszystkie przedmioty oddziałujące na rozgrywkę, a nawet 
			czysto kosmetyczne przedmioty, będą wciąż dostępne poprzez normalną 
			grę.
            <br /><br />
            <h2>P: Czy zwiększyliście szybkość pojawiania się losowych 
			przedmiotów dla tych już istniejących?</h2>
            O: Nie. Nie uczyniliśmy w tym kierunku żadnych zmian, aby utrudnić 
			zdobywanie istniejących już wcześniej przedmiotów poprzez normalną 
			grę.
            <h2>P: Dlaczego wszystkie moje przedmioty stały się przedmiotami&nbsp; "klasycznymi"?</h2>
            O: Niektóre rzadko występujące przedmioty staną się bardziej 
			rozpowszechnione. Chcemy dać graczom taką opcję, jednocześnie 
			pozwalając rozpoznać ludzi, którzy zdobyli te przedmioty w 
			przeszłości “starym, dobrym sposobem”. Zdecydowaliśmy się więc na 
			konwersję wszystkich wcześniejszych przedmiotów do ich &quot;klasycznych&quot; 
			wersji, których w przyszłości nie będzie można już zdobyć. W ten 
			sposób starsze rzadsze przedmioty pozostaną rzadkie (w zasadzie są 
			teraz one nawet jeszcze bardziej limitowane, ponieważ pochodzą teraz 
			z limitowanej edycji).
            <h2>P: Co z przedmiotami, które zdobędę w przyszłości? Jak inni będą 
			mogli je odróżnić od zakupionych przedmiotów?</h2>
            O: Obecnie nie planujemy odróżniania takich przedmiotów przez innych 
			graczy. Zakupione przedmioty nie mogą być jednak wymieniane lub 
			używane do wytwarzania, dlatego wciąż &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;korzystniejsze jest zdobycie 
			przedmiotów "starym, dobrym sposobem".

            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
            <h1>WYMIANA I PERSONALIZACJA</h1>
            <h2>P: Czy mogę wymieniać się przedmiotami?</h2>
            O: Tak. Dodaliśmy oczekiwany przez wielu z was handel przedmiotami, 
			dzięki czemu możesz wymienić sie przedmiotami ze znajomymi.
            <br /><br />
            <h2>P: Czy można wymieniać się wszystkimi przedmiotami?</h2>
            O: Nie, nie wszystkimi przedmiotami można się wymieniać. Niektóre 
			przedmioty o ograniczonej dostępności nie podlegają wymianie. 
			Zakupionych przedmiotów także nie można aktualnie wymieniać z innymi&nbsp; (zobacz 
			O sklepie).
            <br /><br />
            <h2>P: Czy mogę personalizować swoje przedmioty?</h2>
            O: Tak. W dodatku do już istniejącej personalizacji przedmiotów 
			stworzyliśmy nowe rodzaje przedmiotów, które pozwolą na większe niż 
			wcześniej możliwości personalizacji przedmiotów, jak np. zmiana 
			nazwy lub nawet schematu kolorów. Katalog Co. pozwoli ci dowiedzieć 
			się, które przedmioty można personalizować i jak możesz to zrobić. 
			Podwoiliśmy także dostępną ilość miejsca w plecaku, dzięki czemu 
			masz teraz miejsce na przechowywanie wszystkich nowych przedmiotów.
            <br /><br />
            <h2>P: Czy mogę po prostu dać komuś przedmioty w prezencie, czy 
			muszą jednak dać mi coś w zamian?</h2>
            O: Tak, możesz &quot;obdarować&quot; innych swoimi przedmiotami. nie muszą ci 
			niczego oddawać w zamian.
            <br /><br />
            <h2>P: Czy mogę wytworzyć bronie z Polycount? Czy posiadają one 
			przepisy, jak reszta przedmiotów od społeczności?</h2>
			O: Tak, można je wytworzyć. Posiadają one ten sam styl przepisu 
			(połącz dwie bronie), jak inne przedmioty od społeczności.
            <br /><br />
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
