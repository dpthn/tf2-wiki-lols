<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/favicon.ico" />
	<title>TF2 - Robotic Boogaloo</title>
	<link rel="stylesheet" type="text/css" media="screen" href="style/shared.css">
	<link rel="stylesheet" type="text/css" media="screen" href="style/salesbot.css">
	<link rel="stylesheet" type="text/css" media="screen" href="style/comic.css">
	<link rel="stylesheet" type="text/css" media="screen" href="credits/credits_base.css">
	<script type="text/javascript" src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/jquery.min.js"></script>
	<script type="text/javascript" src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/jquery-utils-0.8.5/jquery.utils.lite.min.js"></script>
	<script type="text/javascript" src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/jquery.ba-bbq.min.js"></script>
	<script type="text/javascript" src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/jquery.imagesloaded.min.js"></script>
	<script type="text/javascript" src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/spin.min.js"></script>
	<script type="text/javascript" src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/jquery.comicviewer.js?v=5"></script>
	<script type="text/javascript">

	$(
		function()
		{
			$( '#comic' ).comicViewer(
			{
				'last_frame'	: 13,
				'base_url'		: 'images/comic/salesbot_',
				'last_frame_link'	: '/roboticboogaloo/'
			}
			);
		}
		);

	</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
	<div id="loader">
		<img src="images/boogaloo_logo_hover.png">
		<img src="images/comic_title_hover.png">
	</div>
	<div id="container">
		<a id="return" class="text" href="index.html"></a>
		<a id="title" href="comic.php#"></a>
		<div id="comic"></div>
		<div id="infobar">
			<div id="instructions">Click image or use space bar to advance</div>
			<div id="links">
				<a href="https://steamcdn-a.akamaihd.net/apps/tf2/roboticboogaloo/salesbot.pdf">Download as PDF</a><br>
				<a href="https://steamcdn-a.akamaihd.net/apps/tf2/roboticboogaloo/salesbot.cbr">Download as CBR</a>
			</div>
		</div>
	</div>
</body>
</html>
