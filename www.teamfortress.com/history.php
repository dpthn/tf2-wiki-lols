<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<link rel="shortcut icon" href="images/favicon.ico" />
<title>Team Fortress 2 - History</title>
<link rel = "stylesheet" type = "text/css" href = "history.css?v=11" />
<link rel="stylesheet" type="text/css" href="header.css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>
	<div id="navBarBGRepeat">

		<div id="navBarShadow"></div>
		<div id="navBarBG">

			<div id="navBar">


														<a class="navBarItem" href="index.html">

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_teamfortress_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_teamfortress.png"/>

					</a>

														<a class="navBarItem" href="https://steamcommunity.com/workshop/browse?appid=440"target='_blank'>

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_workshop_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_workshop.png"/>

					</a>

														<a class="navBarItem" href="https://wiki.teamfortress.com/"target='_blank'>

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_wiki_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_wiki.png"/>

					</a>

														<a class="navBarItem" href="https://steamcommunity.com/app/440"target='_blank'>

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_community_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_community.png"/>

					</a>

							</div>

		</div>

	</div>



<script language="javascript">



	function MM_preloadImages() { //v3.0

	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

	}



	function PreloadHeaderImages()

	{

		//  Preload header rollover images

		MM_preloadImages(

										'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_teamfortress_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_store_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_workshop_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_wiki_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_community_over.png'					);

	}



		document.addEventListener( "DOMContentLoaded", PreloadHeaderImages, false );



</script>


<div id="pageContainer"><a name="top"></a>
	<div id="centerColumn">
		<div id="primaryHeaderArea">
			<div id="headerAreaImage"><img src="images/class_header.png" width="984" height="103" border="0" /></div>
			<div id = "logoLink"><a href="index.php"><img src="images/trans.gif" width="270" height="60" /></a></div>
			<div id="spNavMenu">
				<a class="aHoverVertShift" id="sp_menu_home" href="index.php"><img src="images/trans.gif" width="80" height="28" border="0" /></a>
				<a class="aHoverVertShift" id="sp_menu_movies" href="movies.php"><img src="images/trans.gif" width="99" height="28" border="0" /></a>
				<a class="aHoverVertShift" id="sp_menu_comics" href="comics.php"><img src="images/trans.gif" width="103" height="28" border="0" /></a>
				<img class="aHoverVertShift" src="images/sp_menu_history_on.png" width="115" height="28" border="0" />
				<a class="aHoverVertShift" id="sp_menu_artwork" href="artwork.php"><img src="images/trans.gif" width="123" height="28" border="0" /></a>
			</div>
		</div>
		<div id="centerColContent">
			<div id = "ticker_box">
				<div id = "ticker">
					<a href="index.php?tab=updates"><img src = "images/history/ticker_numerals/n7.png" class = "ticker_n" /><img src = "images/history/ticker_numerals/n9.png" class = "ticker_n" /><img src = "images/history/ticker_numerals/n0.png" class = "ticker_n" /></a>
				</div>
				<div class = "ticker_caption"><a href="index.php?tab=updates"><img src = "images/history/ticker_caption.png" alt = "Updates Since Release" /></a></div>
			</div>
			<div id = "blurb">
					Way back in the day, when we first shipped Team Fortress 2 as part of The Orange Box, we'd always planned on it being an ongoing project. But even we had no idea that, over fifteen years later, we'd be more than seven hundred updates strong, with a growing community of collaborators and no signs of stopping. Take a scroll down memory lane at the many tweaks, fixes and epic improvements that have made TF2 the world's #1 war-themed hat simulator.
					<br /><a href = "https://en.wikipedia.org/wiki/Team_Fortress_2" target="_blank">For the full story, check the internet &gt;</a>
			</div>

			<div id="main_content">

				<div id = "updates_container">
					<img src = "images/history_updates_header.png" />					<div class = "update_box">
						<a href = "post.php?id=143884">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2022-10-05.png?v=4"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=143884">Scream Fortress XIV Update</a></span>
						<span class = "caption_date">October 5, 2022</span>
					 </div>						<div class = "update_box">
						<a href = "post.php?id=89183">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2021-10-05.png?v=4"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=89183">Scream Fortress XIII Update</a></span>
						<span class = "caption_date">October 5, 2021</span>
					 </div>						 
					<div class = "update_box">
						<a href = "post.php?id=76112">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2020-10-01.png?v=6"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=76112">Scream Fortress XII Update</a></span>
						<span class = "caption_date">October 1, 2020</span>
					 </div>	

					<div class = "update_box">
						<a href = "post.php?id=54757">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2019-10-10.png?v=4"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=54757">Scream Fortress XI Update</a></span>
						<span class = "caption_date">October 10, 2019</span>
					 </div>	

					<div class = "update_box">
						<a href = "post.php?id=45049">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2018-10-19.png?v=4"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=45049">Scream Fortress X Update</a></span>
						<span class = "caption_date">October 19, 2018</span>
					 </div>	

					<div class = "update_box">
						<a href = "https://www.sourcefilmmaker.com/saxxyawards2017/">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2018-03-15.png?v=2"/></a>
						<span class = "caption_main">
							<a href = "https://www.sourcefilmmaker.com/saxxyawards2017/">The Seventh Annual Saxxy Awards</a></span>
						<span class = "caption_date">March 15, 2018</span>
					 </div>	

					<div class = "update_box">
						<a href = "post.php?id=34052">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2017-10-26.png?v=8"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=34052">Scream Fortress IX Update</a></span>
						<span class = "caption_date">October 26, 2017</span>
					 </div>	

					<div class = "update_box">
						<a href = "jungleinferno/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2017-10-20.png"/></a>
						<span class = "caption_main">
							<a href = "jungleinferno/index.html">The Jungle Inferno Update</a></span>
						<span class = "caption_date">October 20, 2017</span>
					 </div>	

					<div class = "update_box">
						<a href = "post.php?id=26448">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2016-12-21.png"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=26448">Smissmas 2016</a></span>
						<span class = "caption_date">December 21, 2016</span>
					 </div>	

					<div class = "update_box">
						<a href = "https://www.sourcefilmmaker.com/saxxyawards2016/">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2016-11-18.png?v=2"/></a>
						<span class = "caption_main">
							<a href = "https://www.sourcefilmmaker.com/saxxyawards2016/">The Sixth Annual Saxxy Awards</a></span>
						<span class = "caption_date">November 18, 2016</span>
					 </div>	

					<div class = "update_box">
						<a href = "post.php?id=25023">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2016-10-21.png?v=6"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=25023">Scream Fortress VIII Update</a></span>
						<span class = "caption_date">October 21, 2016</span>
					 </div>	

					<div class = "update_box">
						<a href = "meetyourmatch/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2016-07-07.png"/></a>
						<span class = "caption_main">
							<a href = "meetyourmatch/index.html">The Meet Your Match Update</a></span>
						<span class = "caption_date">July 7, 2016</span>
					 </div>	

					<div class = "update_box">
						<a href = "toughbreak/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2015-12-17.png"/></a>
						<span class = "caption_main">
							<a href = "toughbreak/index.html">Tough Break Update</a></span>
						<span class = "caption_date">December 17, 2015</span>
					 </div>	

					<div class = "update_box">
						<a href = "https://www.sourcefilmmaker.com/saxxyawards2015/">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2015-11-18.png"/></a>
						<span class = "caption_main">
							<a href = "https://www.sourcefilmmaker.com/saxxyawards2015/">The Fifth Annual Saxxy Awards</a></span>
						<span class = "caption_date">November 18, 2015</span>
					 </div>	

					<div class = "update_box">
						<a href = "screamfortress7/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2015-10-28.png?v=2"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=19046">Scream Fortress VII Update</a></span>
						<span class = "caption_date">October 28, 2015</span>
					 </div>	
					
					<div class = "update_box">
						<a href = "invasion/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2015-10-06.png"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=18730">Invasion Community Update</a></span>
						<span class = "caption_date">October 6, 2015</span>
					 </div>	
					
					<div class = "update_box">
						<a href = "gunmettle/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2015-07-02.png"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=17356">Gun Mettle Update</a></span>
						<span class = "caption_date">July 2, 2015</span>
					 </div>	

					<div class = "update_box">
						<a href = "post.php?id=15304">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2014-12-22.png"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=15304">Smissmas 2014</a></span>
						<span class = "caption_date">December 22, 2014</span>
					 </div>	

					<div class = "update_box">
						<a href = "endoftheline/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2014-12-08.png"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=15139">End Of The Line Update</a></span>
						<span class = "caption_date">December 8, 2014</span>
					 </div>	
					
					<div class = "update_box">
						<a href = "screamfortress6/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2014-10-29.png"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=14816">Scream Fortress VI Update</a></span>
						<span class = "caption_date">October 29, 2014</span>
					 </div>	

					<div class = "update_box">
						<a href = "https://www.sourcefilmmaker.com/saxxyawards2014/">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2014-10-01.png?v=100"/></a>
						<span class = "caption_main">
							<a href = "https://www.sourcefilmmaker.com/saxxyawards2014/">The Fourth Annual Saxxy Awards</a></span>
						<span class = "caption_date">October 1, 2014</span>
					 </div>	

					<div class = "update_box">
						<a href = "loveandwar/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2014-06-18.png"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=12146">Love & War</a></span>
						<span class = "caption_date">June 18, 2014</span>
					 </div>	
					 
					<div class = "update_box">
						<a href = "post.php?id=12146">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2013-12-20.png"/></a>
						<span class = "caption_main">
							<a href = "post.php?id=12146">Smissmas 2013</a></span>
						<span class = "caption_date">December 20, 2013</span>
					 </div>						

					<div class = "update_box">
						<a href = "https://www.sourcefilmmaker.com/saxxyawards2013/">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2013-11-26.png?v=100"/></a>
						<span class = "caption_main">
							<a href = "https://www.sourcefilmmaker.com/saxxyawards2013/">The Third Annual Saxxy Awards</a></span>
						<span class = "caption_date">November 26, 2013</span>
					 </div>					
					
					<div class = "update_box">
						<a href = "twocities/mannhattan/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2013-11-21.png"/></a>
						<span class = "caption_main">
							<a href = "twocities/mannhattan/index.html">The Two Cities Update</a></span>
						<span class = "caption_date">November 21, 2013</span>
					 </div>					

					<div class = "update_box">
						<a href = "bazbobarrabus/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2013-10-29.png"/></a>
						<span class = "caption_main">
							<a href = "bazbobarrabus/index.html">Scream Fortress V Update</a></span>
						<span class = "caption_date">October 29, 2013</span>
					 </div>

					<div class = "update_box">
						<a href = "roboticboogaloo/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2013-05-17.png"/></a>
						<span class = "caption_main">
							<a href = "roboticboogaloo/index.html">Robotic Boogaloo</a></span>
						<span class = "caption_date">100% Community-Created Update<br />May 17, 2013</span>
					 </div>
                     
					<div class = "update_box">
						<a href = "mechaupdate/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2012-12-20.png"/></a>
						<span class = "caption_main">
							<a href = "mechaupdate/index.html">The Mecha Update</a></span>
						<span class = "caption_date">December 20, 2012</span>
					 </div>

					<div class = "update_box">
						<a href = "saxxyawards2012/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2012-11-30.png"/></a>
						<span class = "caption_main">
							<a href = "saxxyawards2012/index.html">The Second Annual Saxxy Awards</a></span>
						<span class = "caption_date">November 30, 2012</span>
					 </div>				 
                     
					<div class = "update_box">
						<a href = "blizzbobarrabas/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2012-10-26.png?v=6"/></a>
						<span class = "caption_main">
							<a href = "blizzbobarrabas/index.html">Scream Fortress IV Update</a></span>
						<span class = "caption_date">October 26, 2012</span>
					 </div>
                     
					<div class = "update_box">
						<a href = "mvm/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2012-08-15.png"/></a>
						<span class = "caption_main">
							<a href = "mvm/index.html">Mann vs. Machine</a></span>
						<span class = "caption_date">August 15, 2012</span>
					</div>
                    
					<div class = "update_box">
						<a href = "pyromania/index.html">
						<img src = "images/history/updates/2012-06-27.png"/></a>
						<span class = "caption_main">
							<a href = "pyromania/index.html">Pyromania</a></span>
						<span class = "caption_date">June 27, 2012</span>
					</div>
                    
					<div class = "update_box">
						<a href = "australianchristmas2011/index.html">
						<img src = "images/history/updates/2011-12-15.png"/></a>
						<span class = "caption_main">
							<a href = "australianchristmas2011/index.html">Australian Christmas 2011</a></span>
						<span class = "caption_date">December 15, 2011</span>
					</div>
                    
					<div class = "update_box">
						<a href = "veryscaryhalloween/index.html">
						<img src = "images/history/updates/2011-10-27.png"/></a>
						<span class = "caption_main">
							<a href = "veryscaryhalloween/index.html">Very Scary Halloween Special</a></span>
						<span class = "caption_date">October 27, 2011</span>
					</div>
                    
					<div class = "update_box">
						<a href = "manniversary/index.html">
						<img src = "images/history/updates/2011-10-13.png"/></a>
						<span class = "caption_main">
							<a href = "manniversary/index.html">The Manniversary Update</a></span>
						<span class = "caption_date">October 13, 2011</span>
					</div>
                    
					<div class = "update_box">
						<a href = "uberupdate/index.html">
						<img src = "images/history/updates/2011-06-23.png"/></a>
						<span class = "caption_main">
							<a href = "uberupdate/index.html">The &#220;ber Update</a></span>
						<span class = "caption_date">June 23, 2011</span>
					</div>
                    
					<div class = "update_box">
						<a href = "replayupdate/index.html">
						<img src = "images/history/updates/2011-05-05.png"/></a>
						<span class = "caption_main">
							<a href = "replayupdate/index.html">The Replay Update</a></span>
						<span class = "caption_date">May 5, 2011</span>
					</div>

					<div class = "update_box">
						<a href = "hatless/index.html">
						<img src = "images/history/updates/2011-04-14.png"/></a>
						<span class = "caption_main"><a href = "hatless/index.html">The Hatless Update</a></span>
						<span class = "caption_date">April 14, 2011</span>
					</div>

					<div class = "update_box">
						<a href = "australianchristmas/index.html">
						<img src = "images/history/updates/2010-12-17.png"/></a>
						<span class = "caption_main"><a href = "australianchristmas/index.html">Australian Christmas</a></span>
						<span class = "caption_date">December 17, 2010</span>
					</div>

					<div class = "update_box">
						<a href = "screamfortress/index.html">
						<img src = "images/history/updates/2010-10-27.png" /></a>
						<span class = "caption_main"><a href = "screamfortress/index.html">Scream Fortress Update</a></span>
						<span class = "caption_date">October 27, 2010</span>
					</div>

					<div class = "update_box">
						<a href = "mannconomy/index.html">
						<img src = "images/history/updates/2010-09-30.png" /></a>
						<span class = "caption_main"><a href = "mannconomy/index.html">The Mann-Conomy Update</a></span>
						<span class = "caption_date">September 30, 2010</span>
					</div>

					<div class = "update_box">
						<a href = "engineerupdate/index.html">
						<img src = "images/history/updates/2010-07-08.png" /></a>
						<span class = "caption_main"><a href = "engineerupdate/index.html">The Engineer Update</a></span>
						<span class = "caption_date">July 8, 2010</span>
					</div>

					<div class = "update_box">
						<a href = "macupdate/index.html">
						<img src = "images/history/updates/2010-06-10.png" /></a>
						<span class = "caption_main"><a href = "macupdate/index.html">The Mac Update</a></span>
						<span class = "caption_date">June 10, 2010</span>
					</div>

					<div class = "update_box">
						<a href = "119/index.html">
						<img src = "images/history/updates/2010-04-29.png" /></a>
						<span class = "caption_main"><a href = "119/index.html">119th Update</a></span>
						<span class = "caption_date">April 29, 2010</span>
					</div>

					<div class = "update_box">
						<a href = "war/part1/index.htm">
							<img src = "images/history/updates/2009-12-17.png" />
						</a>
						<span class = "caption_main"><a href = "war/part1/index.htm">WAR! Update</a></span>
						<span class = "caption_date">December 17, 2009</span>
					</div>

					<div class = "update_box">
						<a href = "pumpkinpatch/index.html">
							<img src = "images/history/updates/2009-10-29.png"/>
						</a>
						<span class = "caption_main"><a href = "pumpkinpatch/index.html">Haunted Hallowe'en Special</a></span>
						<span class = "caption_date">October 29, 2009</span>
					</div>

					<div class = "update_box">
						<a href = "classless/day01.php">
							<img src = "images/history/updates/2009-08-13.png"/>
						</a>
						<span class = "caption_main"><a href = "classless/day01.php">Classless Update</a></span>
						<span class = "caption_date">August 13, 2009</span>
					</div>

					<div class = "update_box">
						<a href = "sniper_vs_spy/day01_english.htm">
							<img src = "images/history/updates/2009-05-21.png" />
						</a>
						<span class = "caption_main"><a href = "sniper_vs_spy/day01_english.htm">The Sniper vs. Spy Update</a></span>
						<span class = "caption_date">May 21, 2009</span>
					</div>

					<div class = "update_box">
						<a href = "scoutupdate/index.html">
							<img src = "images/history/updates/2009-02-24.png"/>
						</a>
						<span class = "caption_main"><a href = "scoutupdate/index.html">The Scout Update</a></span>
						<span class = "caption_date">February 24, 2009</span>
					</div>

					<div class = "update_box">
						<a href = "heavy/index.html">
							<img src = "images/history/updates/2008-08-19.png" />
						</a>
						<span class = "caption_main"><a href = "heavy/index.html">A Heavy Update</a></span>
						<span class = "caption_date">August 19, 2008</span>
					</div>

					<div class ="update_box">
						<a href = "pyro/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2008-06-19.png?v=4"/></a>
						<span class = "caption_main"><a href = "pyro/index.html">The Pyro Update</a></span>
						<span class = "caption_date">June 19, 2008</span>
					</div>

					<div class = "update_box">
						<a href = "goldrush/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/history/updates/2008-04-29.png?v=4"/></a>
						<span class = "caption_main"><a href = "goldrush/index.html">Gold Rush Update</a></span>
						<span class = "caption_date">April 29, 2008</span>
					</div>

					<div style="clear:both"></div>

				</div>
			</div>
		</div>
	</div>
</div>
<div id="centerFooter">
	<div id="footerContainer"><a href="https://www.valvesoftware.com"><img src="images/valve_footer.png" width="984" height="52" border="0" /></a></div>
</div>


</body>
