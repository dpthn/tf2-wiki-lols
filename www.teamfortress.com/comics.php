<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<link rel="shortcut icon" href="images/favicon.ico" />
<title>Team Fortress 2 - Comics</title>
<link rel="stylesheet" type="text/css" href="comics.css?v=14" />
<link rel="stylesheet" type="text/css" href="header.css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>
	<div id="navBarBGRepeat">

		<div id="navBarShadow"></div>
		<div id="navBarBG">

			<div id="navBar">


														<a class="navBarItem" href="index.html">

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_teamfortress_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_teamfortress.png"/>

					</a>

														<a class="navBarItem" href="https://steamcommunity.com/workshop/browse?appid=440"target='_blank'>

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_workshop_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_workshop.png"/>

					</a>

														<a class="navBarItem" href="https://wiki.teamfortress.com/"target='_blank'>

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_wiki_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_wiki.png"/>

					</a>

														<a class="navBarItem" href="https://steamcommunity.com/app/440"target='_blank'>

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_community_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_community.png"/>

					</a>

							</div>

		</div>

	</div>



<script language="javascript">



	function MM_preloadImages() { //v3.0

	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

	}



	function PreloadHeaderImages()

	{

		//  Preload header rollover images

		MM_preloadImages(

										'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_teamfortress_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_store_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_workshop_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_wiki_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_community_over.png'					);

	}



		document.addEventListener( "DOMContentLoaded", PreloadHeaderImages, false );



</script>


<div id="pageContainer"><a name="top"></a>
	<div id="centerColumn">
		<div id="primaryHeaderArea">
			<div id="headerAreaImage"><img src="images/class_header.png" width="984" height="103" border="0" /></div>
			<div id = "logoLink"><a href="index.php"><img src="images/trans.gif" width="270" height="60" /></a></div>
			<div id="spNavMenu">
				<a class="aHoverVertShift" id="sp_menu_home" href="index.php"><img src="images/trans.gif" width="80" height="28" border="0" /></a>
				<a class="aHoverVertShift" id="sp_menu_movies" href="movies.php"><img src="images/trans.gif" width="99" height="28" border="0" /></a>
				<img class="aHoverVertShift" src="images/sp_menu_comics_on.png" width="103" height="28" border="0" />
				<a class="aHoverVertShift" id="sp_menu_history" href="history.php"><img src="images/trans.gif" width="115" height="28" border="0" /></a>
				<a class="aHoverVertShift" id="sp_menu_artwork" href="artwork.php"><img src="images/trans.gif" width="123" height="28" border="0" /></a>
			</div>
		</div>
		<div id="centerColContent">
			<div id = "blurb">
				<p>
					TF2 didn't start with a lot of story. There wasn't room for one. But as the updates got more ambitious, we found the perfect way to explore the mercs' world: comics. Over the years, the comics have spawned ancillary characters, then assistants to the ancillary characters. Companies mentioned in passing became global empires three generations old. The game that started as a handful of guys in a desert shooting at each other slowly blossomed into the most labyrinthine story in Valve history.
				</p>
			</div>
			<div id = "comics_container">

				<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/comics_tf_comics_header.png" width="855" height="20" border="0" />

				<div class = "comic_box">
					<a href = "catchup/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/tf_catchup.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "catchup/index.html">Catch-Up Comic</a></span>
						<span class = "caption_date">May 2, 2014</span>
					</div>
                </div>
				
				<div class = "comic_box">
					<a href = "tf01_ring_of_fired/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/tf_1.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "tf01_ring_of_fired/index.html">TF Comics #1:<br/>"Ring of Fired"</a></span>
						<span class = "caption_date">August 28, 2013</span>
					</div>
                </div>

				<div class = "comic_box">
					<a href = "tf02_unhappy_returns/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/tf_2.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "tf02_unhappy_returns/index.html">TF Comics #2:<br/>"Unhappy Returns"</a></span>
						<span class = "caption_date">December 4, 2013</span>
					</div>
                </div>
				
				<div class = "comic_box">
					<a href = "tf03_cold_day_in_hell/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/tf_3.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "tf03_cold_day_in_hell/index.html">TF Comics #3:<br/>"A Cold Day in Hell"</a></span>
						<span class = "caption_date">April 2, 2014</span>
					</div>
                </div>
				
				<div class = "comic_box">
					<a href = "tf04_blood_in_the_water/index.html"><img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/tf_4.png"></a>
				       <div class = "comic_caption">
						      <span class = "caption_main"><a href = "tf04_blood_in_the_water/index.html">TF Comics #4:<br/>"Blood in the Water"</a></span>
						      <span class = "caption_date">October 2, 2014</span>
				       </div>
				</div>
				
				<div class = "comic_box">
					<a href = "tf05_old_wounds/index.html"><img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/old_wounds.png"></a>
				       <div class = "comic_caption">
						      <span class = "caption_main"><a href = "tf05_old_wounds/index.html">TF Comics #5:<br/>"Old Wounds"</a></span>
						      <span class = "caption_date">August 31, 2015</span>
				       </div>
				</div>
				
				<div class = "comic_box">
					<a href = "tf06_thenakedandthedead/index.html"><img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/tf_6.png"></a>
				       <div class = "comic_caption">
						      <span class = "caption_main"><a href = "tf06_thenakedandthedead/index.html">TF Comics #6:<br/>"The Naked and the Dead"</a></span>
						      <span class = "caption_date">January 10, 2017</span>
				       </div>
				</div>
				
				<div class = "comic_box">
					<a href = "comics.php#"><img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/temp.png"></a>
				       <div class = "comic_caption">
						      <span class = "caption_main"><a href = "comics.php#" style="color:#999999;">TF Comics #7:<br/></a></span>
						      <span class = "caption_date">Coming soon</span>
				       </div>
				</div>
				<br>
				<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/comics_update_comics_header.png" width="855" height="20" border="0" />
				
				<div class="comic_box">
					<a href="theshowdown/index.html"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/theshowdown.png"></a>
						<div class="comic_caption">
							<span class="caption_main"><a href="theshowdown/index.html">The Showdown</a></span>
							<span class="caption_date">July 7, 2016</span>
						</div>
				</div>
				
				<div class="comic_box">
					<a href="gargoyles_and_gravel/index.html"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/screamfortress7.png"></a>
						<div class="comic_caption">
							<span class="caption_main"><a href="gargoyles_and_gravel/index.html">Gargoyles & Gravel - Scream Fortress 7</a></span>
							<span class="caption_date">October 28, 2015</span>
						</div>
				</div>
				
				<div class="comic_box">
					<a href="thecontract/index.html"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/contract.png"></a>
						<div class="comic_caption">
							<span class="caption_main"><a href="thecontract/index.html">The Contract</a></span>
							<span class="caption_date">July 2, 2015</span>
						</div>
				</div>
				
				<div class = "comic_box">
					<a href = "bloodmoney/index.html"><img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/blood_money.png"></a>
				       <div class = "comic_caption">
						      <span class = "caption_main"><a href = "bloodmoney/index.html">Blood Money</a></span>
						      <span class = "caption_date">October 29, 2014</span>
				       </div>
				</div>

				<div class = "comic_box">
					<a href = "gravematters/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/grave_matters.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "gravematters/index.html">Grave Matters</a></span>
						<span class = "caption_date">October 28, 2013</span>
					</div>
                </div>
  
                <div class = "comic_box">
					<a href = "shadowboxers/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/shadow_boxers.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "shadowboxers/index.html">The Shadow Boxers</a></span>
						<span class = "caption_date">December 19, 2012</span>
					</div>
                </div>
                
                <div class = "comic_box">
					<a href = "doommates/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/doom_mates.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "doommates/index.html">Doom-Mates</a></span>
						<span class = "caption_date">October 24, 2012</span>
					</div>
                </div>
                
                <div class = "comic_box">
					<a href = "fateworsethanchess/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/fate_worse_than_chess.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "fateworsethanchess/index.html">A Fate Worse Than Chess</a></span>
						<span class = "caption_date">August 13, 2012</span>
					</div>
                </div>
                
                
                <div class = "comic_box">
					<a href = "bloodbrothers/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/blood_brothers.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "bloodbrothers/index.html">Blood Brothers</a></span>
						<span class = "caption_date">August 12, 2012</span>
					</div>
                </div>
                                    
                <div class = "comic_box">
					<a href = "asmissmasstory/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/smissmas.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "asmissmasstory/index.html">A Smissmas Story</a></span>
						<span class = "caption_date">December 15, 2011</span>
					</div>
                </div>
                
                <div class = "comic_box">
					<a href = "truemeaning/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/true_meaning.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "truemeaning/index.html">True Meaning</a></span>
						<span class = "caption_date">December 13, 2011</span>
					</div>
                </div>
                
               <div class = "comic_box">
					<a href = "bombinomicon/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/bombinomicon.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "bombinomicon/index.html">Bombinomicon</a></span>
						<span class = "caption_date">October 27, 2011</span>
					</div>
                </div>
                
                <div class = "comic_box">
					<a href = "grordborts/comic/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/grordbort.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "grordborts/comic/index.html">Dr. Grordbort's Crash Landing</a></span>
						<span class = "caption_date">July 20, 2011</span>
					</div>

				</div>
                
                <div class = "comic_box">
					<a href = "meetthedirector/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/meet_the_director.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "meetthedirector/index.html">Meet the Director</a></span>
						<span class = "caption_date">May 5, 2011</span>
					</div>

				</div>

				<div class = "comic_box">
					<a href = "bidwells_big_plan/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/bidwells_big_plan.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "bidwells_big_plan/index.html">Bidwell's Big Plan</a></span>
						<span class = "caption_date">September 30, 2010</span>
					</div>
				</div>

				<div class = "comic_box">
					<a href = "loosecanon/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/loose_canon.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "loosecanon/index.html">Loose Canon</a></span>
						<span class = "caption_date">July 2, 2010</span>
					</div>
				</div>

				<div class = "comic_box">
					<a href = "macupdate/comic/index.html">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/mac.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "macupdate/comic/index.html">A Visual History</a></span>
						<span class = "caption_date">June 10, 2010</span>
					</div>
				</div>

				<div class = "comic_box">
					<a href = "war/administrator">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/war.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "war/administrator">WAR!</a></span>
						<span class = "caption_date">December 17, 2009</span>
					</div>
				</div>

				<div class = "comic_box">
					<a href = "sniper_vs_spy/day07_english.htm">
						<img src = "https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/comics/jarate.png">
					</a>
					<div class = "comic_caption">
						<span class = "caption_main"><a href = "sniper_vs_spy/day07_english.htm">The Insult That Made a "Jarate Master" Out of Sniper</a></span>
						<span class = "caption_date">May 20, 2009</span>
					</div>
				</div>

				<div style="clear:both"></div>

			</div>

			<div id="saxton_container">

				<img src = "images/comics_saxton_header.png" width="855" height="20" border="0" />

				<div id = "covers">
					<a href = "https://wiki.teamfortress.com/w/images/c/c2/Mailstorm_large.jpg"><img src = "images/saxton_covers/saxton_mailstorm.png" /></a>
					<a href = "images/posts/001/mild.jpg"><img src = "images/saxton_covers/saxton_mildlythrilling.png" /></a>
					<a href = "images/posts/001/girlscouts.jpg"><img src = "images/saxton_covers/saxton_girlsadventure.png" /></a>
					<a href = "images/posts/001/barbershop.jpg"><img src = "images/saxton_covers/saxton_barbershop.png" /></a>
					<a href = "images/posts/001/ape.jpg"><img src = "images/saxton_covers/saxton_junglebrawl.png" /></a>
					<a href = "images/posts/001/sharks.jpg"><img src = "images/saxton_covers/saxton_thrillingtales.png" /></a>
					<span class = "caption_main">Series of Saxton Hale Comic Book Covers</span>
				<span class = "caption_date">2009</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="centerFooter">
	<div id="footerContainer"><a href="https://www.valvesoftware.com"><img src="images/valve_footer.png" width="984" height="52" border="0" /></a></div>
</div>



</body>
