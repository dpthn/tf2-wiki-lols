<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - The Mann-Conomy Update </title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro">Normalmente, con las actualizaciones de TF2 nos gustaba ser tan vagos como fuese posible antes del lanzamiento, a sabiendas de que nuestros fans lo averiguarían todo ellos solos en cuanto se pusiesen a jugar. Con una actualización tan ambiciosa como la de la Mann-conomía, sin embargo y puesto que implica el uso de dinero real, hemos querido dejar a un lado la complicidad y las bromas para explicar detalladamente de qué va esta actualización.</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Volver a la Página de Actualizaciones" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Volver a TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top_spanish">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        	<h1>ACERCA DE LA TIENDA</h1>
           	<h2>P: ¿Qué es la tienda Mann Co.?</h2>
                R: Accesible desde dentro de TF2, la tienda Mann Co. permitirá a los jugadores comprar, intercambiar y personalizar el mayor catálogo de artículos de la historia de los videojuegos, usando para ello sus Carteras de Steam.
            	<br><br>
                <h2>P: ¿Qué es la Cartera de Steam?</h2>
                R: Steam dispone ahora de una nueva funcionalidad denominada Cartera de Steam. Te permite ingresar dinero en ella, dinero que puede usarse para comprar objetos en el juego (como los disponibles en la Tienda Mann Co. de TF2) o para comprar artículos (como juegos) en la tienda de Steam.
            	<br><br>
           		<h2>P: ¿Los precios de la tienda están en puntos o en alguna otra extraña moneda?</h2>
                R: No, la Cartera de Steam usa las mismas divisas reales que las disponibles en la tienda de Steam (Dólares/Libras/Euros).
            	<br><br>
                <h2>P: ¿Puedo ingresar la cantidad de dinero que yo quiera en ella?</h2>
                R: Sí. No vamos a obligarte a que adquieras cantidades fijas de dinero. Puedes ingresar la cantidad exacta que necesites para comprar los artículos específicos en los que estés interesado. Sin embargo, la cantidad mínima que puedes ingresar en tu cartera cada vez que vayas a recargarla es de 5$ / 4£ / 5€ para minimizar las tasas de pago y transacción de la entidad bancaria.
            	<br><br>
                <h2>P: ¿Puedo intercambiar o fabricar los objetos que he comprado?</h2>
                R: Por el momento, no. Sólo puedes intercambiar o fabricar aquellos objetos que hayas ganado o encontrado en el juego (consulta abajo la sección Intercambio y Personalización). Hemos adoptado esta política de forma temporal, hasta que podamos ver el efecto que los objetos intercambiados tienen sobre la Mann-conomía.
            	<br><br>
                <h2>P: ¿Es mi dinero de TF2 válido en cualquier otro lugar de Steam?</h2>
                R: Sí. El dinero de tu Cartera de Steam puede usarse para comprar cualquier cosa en Steam.
               
                <h2>P: ¿Vais a vender sólo objetos creados por Valve? ¿Y qué pasa con los objetos de la comunidad?</h2>
                R: La Tienda ofrecerá ambos. Además, ¡los contribuyentes de la comunidad recibirán un porcentaje de las ventas de aquellos objetos que hayan creado! Publicaremos pronto más información acerca de cómo funciona esto exactamente.
            	
                <h2>P: Soy un miembro de la comunidad que contribuyó al juego con un objeto. ¿Por qué no está en la Tienda Mann Co.?</h2>
                R: Inicialmente, sólo vamos a vender los objetos de la comunidad del Pack Polycount, lo que nos permitirá perfeccionar la tienda con un pequeño número de contribuyentes. Una vez hayamos dominado el tema de los pagos a los contribuyentes, abriremos la tienda a todas las contribuciones de la comunidad. Tenemos pensado hacer eso tan pronto como nos sea posible.
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content">
        	<h1>¿QUÉ VA A CAMBIAR?<i> (¿Y QUÉ VA A PERMANECER IGUAL?)</i></h1>
            	<h2>P: ¿Significa esto que las actualizaciones se han acabado? ¿Vais a dejar de publicar contenido gratuito?</h2>
            	R: No. Nuestro plan es continuar actualizando TF2 como siempre hemos hecho, añadiendo mapas gratis, modos de juego, nuevas características y mucho más. La Tienda Mann Co. es simplemente una forma alternativa de conseguir los objetos que otros jugadores han obtenido mientras jugaban.

            	<h2>P: ¿Vais a empezar a cobrar por otras clases de contenido como, por ejemplo, mapas?</h2>
            	R: No, no tenemos intención de hacer eso. Dividir a los jugadores en grupos que no puedan jugar juntos, basados en quién compró qué, es algo que nos gustaría evitar.
            	<h2>P: ¿Voy a tener que gastar mi dinero para seguir siendo competitivo?</h2>
            	R: No. Todos los objetos que alteran la jugabilidad e incluso aquellos puramente cosméticos se podrán seguir obteniendo simplemente jugando al juego.
            	<h2>P: ¿Habéis ajustado las probabilidades de obtención de los objetos existentes?</h2>
            	R: No, no hemos hecho que obtener objetos mientras juegas sea más difícil.
            	<br><br>
            	<h2>P: ¿Por qué todos mis objetos se han transformado en objetos "Clásicos"?</h2>
            	R: Algunos objetos que solían ser raros comenzarán a ser más comunes una vez estén disponibles para su compra. Queríamos ofrecer a los jugadores esta alternativa y, al mismo tiempo, otorgar reconocimiento a aquellas personas que obtuvieron esos objetos de la forma "antigua y pasada de moda". Así que decidimos convertir todos los antiguos objetos en versiones "Clásicas" de ellos mismos, que no podrán conseguirse en el futuro. De esta forma, los objetos raros y antiguos seguirán siendo raros (de hecho, son incluso más raros, por que ahora son ediciones limitadas).
            	<br><br>
            	<h2>P: ¿Y qué pasará con los objetos que obtenga en el futuro? ¿Cómo se diferenciarán de aquellos que haya comprado en la tienda?</h2>
            	R: Actualmente no tenemos intención alguna de hacer esa distinción visible a ojos de otros jugadores. Sin embargo, los objetos comprados en la tienda no pueden ser intercambiados o fabricados por el momento, por lo que sigue habiendo cierto beneficio en obtenerlos "a la vieja usanza".
            	<br><br>
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
         	<h1>INTERCAMBIO Y PERSONALIZACIÓN</h1>
            	<h2>P: ¿Podré intercambiar objetos?</h2>
            	R: Sí. Hemos añadido la tan solicitada función de Intercambio, así que puedes cambiar objetos con tus amigos.
            	<br><br>
            	<h2>P: ¿Todos los objetos son intercambiables?</h2>
            	R: No, no todos los objetos son intercambiables. Algunos artículos de disponibilidad limitada no son intercambiables. Los objetos comprados en la tienda tampoco son intercambiables por el momento (consulta la sección Acerca de La Tienda).
            	<br><br>
            	<h2>P: ¿Puedo personalizar mis objetos?</h2>
            	R: Sí. Además de las existentes posibilidades de personalización, hemos creado nuevas clases de objetos que expandirán aún más esas posibilidades y que te permitirán, por ejemplo, renombrar objetos e incluso cambiar su paleta de colores. El Catálogo Mann Co. te permitirá averiguar qué objetos son personalizables y cómo puedes personalizarlos. Además, hemos doblado la cantidad de espacio disponible en tu mochila para que tengas sitio suficiente para guardar todos estos nuevos objetos.
            	<br><br>
            	<h2>P: ¿Puedo dar sin más objetos a alguien? ¿O él tiene que darme algo a cambio?</h2>
            	R: Sí, puedes "regalar" objetos a otros jugadores. No tienen por qué darte nada a cambio.
            	<br><br>
            	<h2>P: ¿Puedo fabricar las armas Polycount? ¿Tienen planos como los otros objetos de la comunidad?</h2>
            	R: Sí, se pueden fabricar. Tienen el mismo plano (combinar dos armas) que los otros objetos creados por la comunidad. 
            	<br><br>
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
