<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - The Mann-Conomy Update </title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro">Typically with TF2 updates we like to be as vague as possible prior to launch, knowing our fans will figure everything out for themselves once they get in the game and start playing. With an update as ambitious as the Mann-conomy update, though, and because it involves actual money, we wanted to push aside the winking, nudging and teases for a second, and lay out precisely what we're doing and what this update is all about.</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Back to Update Page" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Back to TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        		<h1>ABOUT THE STORE</h1>
           		<h2>Q: What's a Mann Co. Store?</h2>
                A: Accessible from inside TF2, the Mann Co. Store will now let players buy, trade and customize the largest addition of inventory items in the game's history, using their Steam Wallets.
            	<br /><br />
                <h2>Q: What's a Steam Wallet?</h2>
                A: Steam now has a feature called the Steam Wallet. It allows you to put money into it, which can then be spent either on in-game items, such as the ones offered in TF2’s Mann Co. Store, or on full products like games in the Steam store.
            	<br /><br />
           		<h2>Q: Are store items priced in points, or some other weird currency?</h2>
                A: No, the Steam Wallet uses the same real-world currency Steam currently supports (USD/GBP/Euros).
            	<br /><br />
                <h2>Q: Can I put whatever amount I want into my wallet?</h2>
                A: Yes. We won't force you to buy “bundles” of currency. You can put the exact amount you need to buy the specific items you're interested in. However, each time you fund your wallet, the minimum amount of funds that you can put in your Steam Wallet is $5 / £4 / 5€ to keep transactions and payment service provider fees to a minimum.
            	<br /><br />
                <h2>Q: Can I trade or craft items I've purchased?</h2>
                A: In the short term, no. You can only trade or craft items you have earned or found through gameplay (see Trading & Customization below). We have adopted this policy for the time being, until we can see what effect trading purchased items may have on the Mann-conomy.
            	<br /><br />
                <h2>Q: Is my “TF2” money good elsewhere in Steam?</h2>
                A: Yes. The money in your Steam Wallet can be used to purchase anything on Steam.
                <br /><br />
                <h2>Q: Will you only sell Valve-crafted items? What about community-made items?</h2>
                A: The Store will sell both. Additionally, community contributors will receive a percentage of sales on items they've created! We’ll be releasing more information soon about exactly how this will work.
            	<br /><br />
                <h2>Q: I'm a community member who contributed an item that's been put in the game. Why isn't it in the Mann Co. Store?</h2>
                A: Initially, we will only be selling the Polycount Pack community items, as this will let us work out the kinks with a small number of contributors. Once we've mastered contributor payments, we'll be expanding the store to include all the community contributions. We plan to do this as soon as possible.
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content">
            <h1>WHAT WILL CHANGE?<i> (AND WHAT WILL STAY THE SAME?)</i></h1>
            <h2>Q: Does this mean the updates are over? Are you going to stop releasing free content now?</h2>
            A: No. Our plan is to continue updating TF2 just like we always have, adding free maps, game modes, new features, and more. The Mann Co. Store is simply an alternative way of obtaining items that other players can earn during gameplay.
            <br /><br />
            <h2>Q: Are you going to start charging for other kinds of content, like maps?</h2>
            A: No, we have no plans to do this. Segregating players into groups that can't play together, based on who bought what, is something we'd like to avoid.
            <br /><br />
            <h2>Q: Will I have to spend money to remain competitive?</h2>
            A: No. Any items affecting gameplay, and even most purely cosmetic items, will still be obtainable simply by playing the game.
            <br /><br />
            <h2>Q: Did you adjust the drop rates for existing items?</h2>
            A: No. We haven't made it any harder to obtain existing items through gameplay.
            <br /><br />
            <h2>Q: Why have all my items switched to "Vintage" items?</h2>
            A: Some items that used to be rare will become more common when they are available for purchase. We wanted to give players this option, but still recognize people who obtained those items “the old-fashioned way” in the past. So we decided to perform a one-time conversion of all the old items into "Vintage" versions, which will never be attainable in the future. This way, those older rare items remain rare (in fact, they're even rarer, because they're limited editions now).
            <br /><br />
            <h2>Q: What about items I earn in the future? How will others tell them apart from purchased items?</h2>
            A: There are not currently any plans to make this distinction visible to other players. However, purchased items are not currently tradeable or craftable, so there's still a benefit to earning your items "the old fashioned way".
            <br /><br />
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
            <h1>TRADING & CUSTOMIZATION</h1>
            <h2>Q: Will I be able to trade items?</h2>
            A: Yes. We've added the long-requested Trading feature, so you can swap items with friends.
            <br /><br />
            <h2>Q: Are all items tradeable?</h2>
            A: No, not all items are tradeable. Some limited-availability items are not tradeable. Purchased items are also not tradeable at the moment (see About the Store).
            <br /><br />
            <h2>Q: Can I customize my items?</h2>
            A: Yes. In addition to our existing item customization, we have created new kinds of items that will enable a greater amount of customization than we've had in the past, like the ability to name items and even change their color scheme. The Mann Co. Catalog will let you find out which items are customizable and how you can customize them. We've also doubled the amount of backpack space available to you, so that you’ll have room to store all this new stuff.
            <br /><br />
            <h2>Q: Can I just give items to someone? Or do they have to give me something back?</h2>
            A: Yes, you can "gift" items to others. They do not have to give you anything back.
            <br /><br />
            <h2>Q: Can I craft the Polycount weapons? Do they have recipes like the other community items?</h2>
            A: Yes, they're craftable. They have the same recipe style (combine two items) as the other community-contributed items.
            <br /><br />
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
