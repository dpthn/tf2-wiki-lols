<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="shortcut icon" href="../images/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
    <link href="gunmettle.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="magnific-popup.css"> 
    <title>TF2 - The Gun Mettle Update</title>
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="https://steamcdn-a.akamaihd.net/apps/tf2/common/javascript/jquery.spritely.js"></script>
	<script src="https://steamcdn-a.akamaihd.net/apps/dota2/javascript/static/jquery.cycle.all.js?v=253" type="text/javascript"></script>
	<script src="jquery.magnific-popup.js" type="text/javascript"></script>
	<meta property="og:title" content="Team Fortress 2 - The Gun Mettle Update" />
	<meta property="og:type" content="game" />
	<meta property="og:url" content="https://www.teamfortress.com/gunmettle/" />
	<meta property="og:image" content="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images//gunmettle_facebook.jpg" />
	<meta property="og:site_name" content="teamfortress.com" />
	<meta property="fb:admins" content="726460592" />
<script>	
// sliders
        $(document).ready(function() {
           $('#borneo_slider').cycle({
                fx:     'fade', 
                speed:   400, 
                next:   '#borneo_slider_next', 
                prev:   '#borneo_slider_prev', 
                pause:   1,
                pager:   0,
                timeout: 6000
            });
        });
		
        $(document).ready(function() {
           $('#suijin_slider').cycle({
                fx:     'fade', 
                speed:   400, 
                next:   '#suijin_slider_next', 
                prev:   '#suijin_slider_prev', 
                pause:   1,
                pager:   0,
                timeout: 6000
            });
        });
		
		$(document).ready(function() {
           $('#snowplow_slider').cycle({
                fx:     'fade', 
                speed:   400, 
                next:   '#snowplow_slider_next', 
                prev:   '#snowplow_slider_prev', 
                pause:   1,
                pager:   0,
                timeout: 6000
            });
        });

// video popups		
		$(document).ready(function() {
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
});
</script>

</head>
<body>
<div id="fb-root"></div>
<div id="powerhouse_bg_01">
    <div id="powerhouse_container_01">
		<a href="../index.html" id="logoLink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/trans.gif" width="255" height="67" /></a>
		<a href="index.html" id="dayoneOff"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/trans.gif" width="111" height="53" /></a>
		<a href="powerhouse.php" id="daytwoOn"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/trans.gif" width="111" height="53" /></a>
		<a href="../thecontract/index.html" id="comicLink"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/trans.gif" width="226" height="200" /></a>
		<div id="powerhouse_copy_01_01">
			The densely-packed, fast-paced Powerhouse is TF2's first three-point CP map. Commit wasteful crimes against nature in the environmentally conscious shadow of a clean, renewable energy source!
		</div>
    </div>
</div>
<div id="powerhouse_bg_02">
    <div id="powerhouse_container_02">
		<div id="powerhouse_copy_02_01">
			A portion of the money made from sales of Campaign Passes will go to the authors of these three new community maps. Remember to submit your maps to the <a href="../post.php?id=17092" target="_blank">Maps Workshop Beta</a>!
		</div>
    </div>
</div>
<div id="powerhouse_bg_03">
    <div id="powerhouse_container_03">
		<div id="borneo_slider_container">
            <div id="borneo_slider">    
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/borneo_01.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/borneo_02.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/borneo_03.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/borneo_04.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/borneo_05.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/borneo_06.png?v=102" width="974" height="573" />              
            </div>		
            <div class="controls" id="borneo_slider_next" href=""></div>
            <div class="controls" id="borneo_slider_prev" href=""></div>
        </div>
		<div id="suijin_slider_container">
            <div id="suijin_slider">    
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/suijin_01.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/suijin_02.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/suijin_03.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/suijin_04.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/suijin_05.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/suijin_06.png?v=102" width="974" height="573" /> 
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/suijin_07.png?v=102" width="974" height="573" /> 				
            </div>			
            <div class="controls" id="suijin_slider_next" href=""></div>
            <div class="controls" id="suijin_slider_prev" href=""></div>
        </div>
		<div id="snowplow_slider_container">
            <div id="snowplow_slider">    
                <img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/snowplow_01.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/snowplow_02.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/snowplow_03.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/snowplow_04.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/snowplow_05.png?v=102" width="974" height="573" />
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/snowplow_06.png?v=102" width="974" height="573" /> 
				<img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/snowplow_07.png?v=102" width="974" height="573" /> 				
            </div>		
            <div class="controls" id="snowplow_slider_next" href=""></div>
            <div class="controls" id="snowplow_slider_prev" href=""></div>
        </div>
    </div>
</div>
<div id="powerhouse_bg_03_5">
    <div id="powerhouse_container_03_5">
    	<div id="powerhouse_copy_03_5_01">
    		We've brought together the best parts of stealing and murder into one guilt-free activity! Players have always dropped their equipped weapons when they die. But now, if it's a weapon your class can equip, you can pick it up and take it for a test drive. You can even change loadouts on the fly! Plus, you get that weapon forever! Until you die. In the game, not real life.
    	</div>
    </div>
</div>
<div id="powerhouse_bg_04">
    <div id="powerhouse_container_04">
    	<div id="powerhouse_copy_04_01">
    		Dance Like the KGB isn't Watching
    	</div>
		<div id="powerhouse_copy_04_02">
    		When you think of the Soviet Union, you think of one thing: the freedom to laugh and love and sing and dance! 
    	</div>
		<div id="popup_russian_dance">
			<a class="popup-youtube" href="https://www.youtube.com/watch?v=QDCPKBF7a1Q"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/trans.gif" width="199" height="112" /"></a>
		</div>
    </div>
</div>
<div id="powerhouse_bg_05">
    <div id="powerhouse_container_05">
		<div id="powerhouse_copy_05_01">
			Think Outside the Box 
			<br />While Hiding Inside One
		</div>
		<div id="powerhouse_copy_05_02">
			No one will think to look for you inside of a moving box with your legs sticking out of the bottom. It's the perfect disguise for the person no one is looking for. Created by <b>Sn1pe</b>, <b>Sparkwire</b> and <b>DeRosaJ</b>.
		</div>
		<div id="popup_boxtrot">
			<a class="popup-youtube" href="https://www.youtube.com/watch?v=JsSmz-6K-dA"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/trans.gif" width="199" height="112" /"></a>
		</div>
    </div>
</div>
<div id="powerhouse_bg_06">
    <div id="powerhouse_container_06">
		<div id="powerhouse_copy_06_01">
			Win the Arms Race
		</div>
		<div id="powerhouse_copy_06_02">
			As a TF2 player, you worked hard for that body. 
			<br />Seize the means of astonished faces production with a posedown for the people! Created by <b>Hypo</b>. 
		</div>
		<div id="popup_posedown">
			<a class="popup-youtube" href="https://www.youtube.com/watch?v=5LptAhOgGRw"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/gunmettle/images/trans.gif" width="199" height="112" /"></a>
		</div>
</div>
<div id="powerhouse_bg_07">
    <div id="powerhouse_container_07">
    </div>
</div>
<div id="powerhouse_bg_08">
    <div id="powerhouse_container_08">
    </div>
</div>
<div id="powerhouse_bg_09">
    <div id="powerhouse_container_09">
		<div id="powerhouse_copy_09_01">
			<b>GAME PLAY CHANGES</b>                        
<ul>                                
<li>New feature : Weapon exchange and pick up.  Weapons dropped by killed players can no longer be picked up for ammo. Killed players will now also drop a medium ammo box. Players that can normally equip the dropped weapon (proper class) can look at the weapon and press the 'action key' to exchange it with what they have equipped.  The player's equipped weapon will be dropped on the exchange.</li>                                  
<li>Inspect Target has been changed to Inspect Target or Item.  While having a decorated weapon deployed with no target under the cross hair and then pressing the bound inspect key (+inspect) , an interruptible view model animation will play.</li>

<li>Random Damage Spread is off by default (ConVar tf_damage_disablespread is set to 1).</li>

<li>Auto reload is on by default (ConVar cl_autoreload 1).  Existing users will still use their currently set value.  This setting can be changed under Advance Options or through the developer console.</li>                        
<li>Updated various weapon descriptions to better detail the weapon's features.</li>
</ul>                        

<b> Class and Weapon Changes </b>                                            
<br>
<br>
<b>SPY</b>                        
<ul>                                
<li>Spy no longer smacks himself in the face while reloading a revolver (New Spy revolver reload animation that does not block cross hair)</li>

<li>Changing to a new disguise while already under a disguise takes 0.5 seconds instead of the normal disguise time of 2 seconds</li>

<li>In the disguise menu, pressing 'reload' will also toggle the disguise team.</li>                                

<li>While invisible, Spy receives 20% less damage from all damage sources</li>

<li>While invisible, Spy has reduced timer on debuffs (fire, jarate, milk, bleed)</li>

<li>Decreased the damage penalty on Sentries sapped by the Spy from -66% to -33% (i.e. shooting a Sentry the Spy sapped with a Revolver is now more effective)</li>
</ul>                        

Spy-cicle                        
<ul>                                
<li>Changed fire immunity for 3 seconds to fire immunity for 1 second and 7 seconds of afterburn immunity

<li>Removed silent killer attribute</li>                                

<li>Spy-cicle recharge timer can now be reduced by picking up ammo boxes</li>
</ul>                        

Enforcer                        
<ul>                                
<li>Changed +20% damage bonus while undisguised to +20% damage bonus while disguised</li>
</ul>                        

Big Earner                        
<ul>                                
<li>Added 3 second speed gain on kill</li>
 </ul>                       
 
  Kunai                        
  <ul>                                
  <li>Health penalty reduced from -65 to -55 (70 Health total)</li>
  
  <li>Minimum Health gain of 75 on kill</li>
  
  <li>Maximum overheal from Kunai increased from 195 to 210</li>
  </ul>                        
  
  Cloak and Dagger                        
  <ul>                                
  <li>Can now pick up ammo kits for cloak meter while visible.  Previously could not pick up ammo packs for cloak.  Cloak gain is at a reduced rate when compared to stock invis watch on ammo pick up</li>                        </ul>
  
  Dead Ringer                        
  <ul>                                
  <li>Triggering Feign Death instantly removes 50% cloak meter</li>                                
  
  <li>Changed increased drain rate to a decreased drain rate. Overall duration of invisibility is still 7 seconds when accounting for initial spend of 50 cloak meter for triggering Feign Death</li>                                

<li>Decreased cloak regen rate from +80% to +50%</li>                                

<li>When Feign Death is triggered, the Spy receives a 3 second speed boost</li>                                

<li>Initial attack that triggers feign death has its damage reduced by 50%</li>                                

<li>Damage resistance on triggering feign death scales over time. 65% to 20% over 3 seconds</li>                                

<li>Feign Death stealth has no bump shimmer for 3 seconds</li>                               

<li>3 seconds after triggering Feign Death, the Spy is under normal invisible conditions (20% armor and shimmers if bumped or shot)</li>                                

<li>Can no longer pick up ammo for cloak meter while cloaked</li>                        
</ul>                        

<b> ENGINEER</b>                        
<ul>                                
<li>Construction boosts (wrench hits, redeploys) from multiple sources is now additive instead of multiplicative.  Calculations are based around a 1x base building speed.</li>                                

<li>Increased base wrench construction speed on hit boost by 50%.  Buildings now build 2.5x faster instead of just 2x (additive of 1.5x + base speed)</li>                                

<li>Teleporters and Dispensers redeploy +50% faster (2.5x without wrench boost, 5.5x with wrench boost)</li>                                

<li>Building pick up speed penalty reduced from 25% to 10%</li>                                

<li>On Wrench equip change, buildings no longer self-destruct unless the building type is changed (i.e. Only Sentry explodes when switching from Wrench to Gunslinger)</li>

 <li>Building repair costs increased from 20 metal to 33 metal to repair 100hp per wrench hit (from 5HP per metal to 3HP per metal)</li>

<li>Level 2 and Level 3 Sentries have less passive damage resistance against Heavy miniguns.  Level 2 Sentry Minigun resistance changed from 20% to 15% and Level 3 Changed from 33% to 20%</li>                        
</ul>
  
Gunslinger / Minisentry                        
<ul>                                
<li>Mini Sentries can now be repaired</li>                                

<li>Mini Sentries can now be wrench construction boosted</li>                                

<li>Mini Sentry base build speed decreased.  Mini Sentries that are wrench boosted build slightly faster than previously.</li>

<li>Mini Sentries start at 50% health on construction and gain health during construction instead of starting at 100% </li>

<li>Metal gibs from destroyed Mini Sentries no longer grant any metal</li>
</ul>                        

Pomson 6000                        
<ul>                               
<li>Uber and Cloak drain decreases over distance from target.  Decreases start at 512 Hammer Units (Hu) from target and reach 0 drain at 1536Hu</li>
</ul>                        

Wrangler                        
<ul>                                
<li>Ammo and Repair given to a shielded Sentry is reduced by the strength of the shield (66% reduced) when shield is active</li>
<li>Engineer death keeps the Wrangled sentry shielded and disabled for 3 seconds, same as when Wrangler is switched away.  Previously Engineer death caused disable state for only 1 second</li>                        
</ul>                        

Jag                        
<ul>                               
 <li>With the change to Base Construction boost, Jag bonus has improved.  30% increase of 1.5x makes a total of 1.95x (total of 2.95x when base speed is added)</li>                                
 
 <li>Added +15% swing speed.</li>                                
 
 <li>Added 20% repair penalty.  Repairs will give up to 80hp per swing instead 100hp per swing.</li>                        
 
 </ul>                        
 
 The Short Circuit                        
 <ul>                  
<li>Projectile destruction has been moved to alt-fire at the cost of -15 per shot.  There is a 0.5s cool down between attempts and refire</li>                                

<li>Cannot pick up buildings when the Short Circuit is deployed</li>
</ul>                        

Eureka Effect                        
<ul>                                
<li>Previous penalties have been removed and replaced with the following</li>                                
<li>Construction hit speed boost decreased by 50%</li>                                
<li>50% less metal from pickups and dispensers</li>                        
</ul>                       


 <b> SCOUT </b>                        
 <br><br>Baby Face's Blaster                        
 <ul>                                
 <li>Added Boost reduction on taking damage.</li>                                
 
 <li>Increased amount of Boost lost on air jump</li>
 </ul>                        
 
 Short Stop                        
 <ul>                                
 <li>No longer uses secondary ammo and now uses primary ammo instead</li>                             
 
 <li>Healing and knockback passives are only active when weapon is deployed</li>
 </ul>                        
 
 Pretty Boy's Pocket Pistol
 <ul>                                
 <li>Passive effects on the Pretty Boy's Pocket Pistol are only in effect when the weapon is deployed</li>
 
  <li>Removed +15 max health passive</li>                                
  
  <li>Added up to +3 health per hit</li>                                
  
  <li>Changed damage vulnerability from +50% fire to +20% all sources while active</li>                        
  </ul>                        
  
Fan O' War
 <ul>                                
 <li>Now crits whenever it would normally mini-crit</li>
 
  <li>Reduced damage penalty from -90% to -75%</li>                 
  </ul> 
  
  <b> SOLDIER </b>                        
  <br><br>Airstrike                        
  <ul>                                
  <li>Removed clipsize penalty</li>

<li>Reduced radius penalty from -15% to -10%</li>                                

<li>Reduced damage penalty from -25% to -15%</li>

<li>Rocket jump blast damage reduction reduced from -25% to -15%</li>                        
</ul>                        

Equalizer and Escape Plan                        
<ul>                               
 <li>Changed no healing penalty to 90% less healing from Medics while active</li>                        
 </ul>                        
 
 Blackbox                        
 <ul>                                
 <li>Changed +15 health on hit to +20 health on hit per attack</li>                                
 
 <li>Changed how health on hit works for radius damage.  Is now capped per attack and scales with total damage done relative to base damage.</li>                                
 
 <li>i.e. doing 45 damage nets +10 health on hit as base damage is 90.  Hitting 3 enemies for 45 each for a total of 135 damage only returns +20 health.</li>                        </ul>
 
 Liberty Launcher                        
 <ul>                                
 <li>Now has +25% clip size</li>
 </ul>                        
 
 Battalions Backup                        
 <ul>                                
 <li>Fixed an issue that caused rocket jumps to be decreased when it was active.</li>
 </ul>                        
 
 
 <b> DEMOMAN </b>                       
  <br><br>Tide Turner                        
  <ul>                                
  <li>Self damage will no longer decrease charge when charging</li>
  
  <li>Fall damage will no longer decrease charge when charging</li>                                
  
  <li>Amount of charge taken away on damage when charging reduced from 3 to 1 per point of damage</li>                        
  </ul>                        
  
  Bootlegger / Ali Baba's Wee Booties                        
  <ul>                                
  <li>Added +10% movement speed bonus                                
  
  <li>Changed 25 charge on charge kill to 25 charge on melee kill</li>                       
   </ul>

   The Claidheamh Mòr
   <ul>                                
   
   <li>Changed 25 charge on charge kill to 25 charge on melee kill</li>                        
   </ul>                        
   
   Loch-n-Load                        
   <ul>                                
   <li>Changed +20% damage bonus to +20% damage against buildings</li>                        
   </ul>                       
   
   Iron Bomber                        
   <ul>                                
   <li>Removed damage penalty on self-detonate</li>                                
   
   <li>Reduced radius penalty from -20% to -15%</li>                        
   </ul>                        
   
   Quickiebomb Launcher                        
   <ul>                                
   <li>Damage is now increased based on charge amount when the bomb is fired</li>
    </ul>                        
    
    Ullapool Caber                        
    <ul>                                
    <li>Reduced explosion base damage from 100 to 75</li>                                
    
    <li>Reduced damage ramp up bonus for close range attacks.  Now is the same as other explosive weapons.</li>
    </ul>                        
    
    
    <b> PYRO </b>                        
    <br><br>Flaregun                        
    <ul>                                
    <li>Added text to describe 100% critical hits on burning targets</li>                        
    </ul>                        
    
    Scorch Shot                        
    <ul>                                
    <li>Reduced damage penalty from -50% to -35%</li>                                
    
    <li>Now has increased knock back on burning targets</li>
    
    <li>Increased the blast radius from flares from 92Hu to 110Hu</li>                                
    
    <li>Hits and explosions always minicrit burning targets</li>                                
         
</ul>                        

Detonator                        
<ul>                                
<li>Added text to describe 100% minicrits on burning targets</li>

<li>Slightly increased blast jump height when doing a Detonator jump</li>

<li>Increased blast radius from 92Hu to 110Hu</li>                                

<li>Detonated explosions now also minicrit burning targets</li>                                

<li>Increased damage penalty to -25%</li>

<li>Increased self-damage penalty from +25% to +50%</li>                        
</ul>                        


<b> MEDIC </b>                        
<br><br>Vaccinator                        
<ul>                                
<li>Fixed a bug that gave Vaccinator patients full crit immunity.</li>                                

<li>Vaccinator base resist does not grant any crit resistance.</li>                                

<li>Vaccinator Uber deploys now always take exactly 1 bar of Uber charge.</li>                                

<li>Vaccinator Uber deploys give the patient a 2.5 second bubble of 75% damage resistance of the current resist type and full crit resistance to that type.  These bubbles do not disappear if the medic stops  targeting the current patient.  Multiple bubbles of different types can be applied to the same patient or multiple patients given the same resist uber each consuming 1 charge.</li>

<li>Vaccinator uber build now suffers the same penalties as other mediguns when it comes to multiple medics on the same target and max overhealed patients.</li>                                

<li>Decreased the bonus healing a Medic received for properly selecting the right damage resistance type from 25% of incoming damage to 10% of incoming damage</li>
<li>Added Penalty of 66% decreased uber build rate while healing a overhealed patient</li>                        
</ul>                        

Solomn Vow                         
<ul>                                
<li>Added 10% attack speed penalty</li>                        
</ul>                        

<b> SNIPER </b>                        
<br><br>Sydney Sleeper
<ul>                                
<li>Jarate now applies on all scoped shots, duration scales with charge duration (2 to 8 seconds)</li>                        
</ul>                        

Bazaar Bargain
<ul>                                
<li>No longer lose heads on miss</li>                                
<li>Collecting a head requires a headshot kill and not just a headshot</li>                                

<li>Each head boosts charge rate by 25% up to 200%</li>                                

<li>Charge rate penalty changed from -20% to -50%.</li>                                

<li>Now requires 2 headshot kills to be back at base speed and 6 kills for 200% charge rate</li>
</ul>                        

Bushwacka
<ul>                                
<li>Changed penalty from +20% fire vulnerability to +20% damage vulnerability while active</li>                        
</ul>                        

<b> HEAVY </b>                        
<ul>                               
 <li>Minigun damage penalty on Level 2 and Level 3 Sentries slightly decreased.  Level 2 Sentry resistance changed from 20% to 15% and Level 3 changed from 33% to 20%</li>
 </ul>                        
 
 Natascha                           
 <ul>                                
 <li>Added 20% damage resistance while spun up  </li>
 
 <li>Stun amount now has distance falloff. Decreases starting at 512hu down to zero stun at 1536Hu</li>
 </ul>                        
 
 Brass Beast                          
 <ul>  
 <li>Added 20% damage resistance while spun up  </li>
 </ul>                        
 
 Tomislav                         
 <ul>                                
 <li>Now 20% more accurate (less spread) </li>
 
  <li>Increased spin up bonus from 10% to 20%</li>    
 </ul>                         
 
 Family Business                          
 <ul>                                
 <li>Now has +15% increased attack speed </li>
 </ul>                         
 
 Warriors Spirit                          
 <ul>                                
 <li>Now has +10 health on hit </li>
 </ul>                         
 
 Eviction Notice                         
 <ul>                                
 <li>Now has 3 second speed boost on hit </li>
 </ul>                        
 
 Dalokoh's Bar                         
 <ul>                                
 <li>Now has 10 second cool down on use </li>
 <li>Can now overheal up to 400hp</li>
 <li>Can be thrown (alt-fire) as a small medkit for other players to use</li>                        
 
 </ul>                        
 Lunchbox items                        
 <ul>                                
 <li>Updated description to note that these items can be thrown (alt-fire) to supply players with a medkit.</li>                        
 </ul>

<b> MULTI-CLASS </b>   
<br>
<br>Panic Attack                          
<ul>                                
<li>Base Fire rate increased (from 15% to 30%)</li>                                  

<li>Base reload speed increased (from 33% to 50%)</li>                                   

<li>Added increased switch to speed by +50%</li>
</ul>   

		</div>
    </div>
</div>
<div id="powerhouse_bg_10">
    <div id="powerhouse_container_10">
    </div>
</div>

</body>
</html>
