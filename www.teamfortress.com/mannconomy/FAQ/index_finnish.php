<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - The Mann-Conomy Update </title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/finnish//FAQ_ManncoLink_over.jpg','../images/finnish//FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
       <div id="FAQ_intro">Yleensä yritämme olla mahdollisimman epämääräisiä ennen TF2-päivityksiä, sillä tiedämme että fanimme tulevat hahmottamaan kaiken ilman apua pelatessaan päivityksen jälkeen. Mutta yhtä kunnianhimoisen uudistuksen kuin Mann-conomy -päivityksen kanssa (ja myös siksi että siihen liittyy oikea raha) halusimme siirtää hetkeksi sivuun kaiken kiusoittelun sekä jahkailun ja kertoa täsmällisesti sen, mitä olemme tekemässä ja mistä tässä päivityksessä oikein on kyse.</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/finnish//FAQ_ManncoLink_over.jpg',1)"><img src="../images/finnish/FAQ_ManncoLink.jpg" alt="Back to Update Page" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/finnish//FAQ_TFLink_over.jpg',1)"><img src="../images/finnish/FAQ_TFLink.jpg" alt="Back to TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top_finnish">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
 <h1>TIETOA KAUPASTA</h1>              <h2>K: Mikä on Mann Co. -kauppa?</h2>           V: Mann Co. -kauppa antaa pelaajille mahdollisuuden ostaa, vaihtaa ja muokata suurta valikoimaa esineitä pelin sisäisesti käyttämällä Steam-lompakkoa.           <br /><br />           <h2>K: Mikä on Steam-lompakko?</h2>
              V: Steamiin sisältyy nykyään ominaisuus nimeltään Steam-lompakko. Voit siirtää sinne rahaa käyttääksesi sitä joko pelin sisäisiin ostoksiin, kuten Mann Co. -kaupan esineisiin, tai itse peleihin Steamin kaupasta.
               <br /><br />              <h2>K: Ovatko kaupan tuotteet hinnoiteltu pisteillä tai jollain muulla oudolla valuutalla?</h2>           V: Ei. Steam-lompakko käytää samoja oikeita rahayksiköitä joita Steam tällä hetkellä tukee (Yhdysvaltain dollari, Iso-Britannian punta ja Euro).           <br /><br />           <h2>K: Voinko laittaa lompakkooni minkä tahansa määrän rahaa?</h2>           V: Kyllä. Emme määrää sinua ostamaan rahaa “niputtain”. Voit laittaa lompakkoon pilkulleen sen verran rahaa kuin tarvitset ostaaksesi tiettyjä esineitä joista olet kiinnostunut. Pitääksemme maksupalvelun tarjoajan palkkiot mahdollisimman alhaisina, huomioitavaa kuitenkin on, että pienin määrä mitä voit lisätä lompakkoosi on 5 $ / 4 £ / 5 €.           <br /><br />           <h2>K: Voinko vaihtaa ostamiani esineitä tai käyttää niitä muiden esineiden luomiseen?</h2>
              V: Lyhyesti sanottuna, et. Voit käyttää vaihtamiseen tai luomiseen ainoastaan esineitä, jotka olet ansainnut tai löytänyt pelin kautta. Olemme omaksuneet tämän käytännön siihen asti, kunnes näemme mikä vaikutus ostettujen esineiden vaihtamisella saattaisi olla Mann-konomiaan.
               <br /><br />           <h2>K: Onko “TF2-rahani” käypää muualla Steamissa?</h2>           V: Kyllä. Voit käyttää Steam-lompakkosi rahoja missä tahansa muussakin Steamin palvelussa.           <br /><br />           <h2>K: Aioitteko myydä ainoastaan Valven tekemiä esineitä? Mikä on tilanne yhteisön tuotosten kohdalla?</h2>           V: Kauppa tulee myymään molempia. Yhteisön avustajat saavat myös osuuden tekemiensä esineiden myyntituloista! Julkaisemme pian enemmän tietoa siitä, miten tämä käytännössä tulee toimimaan.           <br /><br />           <h2>K: Olen yhteisön jäsen ja tekemäni esine on lisätty peliin. Miksei sitä löydy Mann Co. -kaupasta?</h2>
              V: Näin aluksi myymme vain Polycount-paketin yhteisöesineitä, sillä vikojen korjaaminen pienen avustajaryhmän kanssa on helpompaa. Kun hallitsemme maksuliikenteen avustajien ja meidän välillämme, laajennamme kaupan koskemaan kaikkia yhteisön avustajia. Suunnitelmissamme on toteuttaa tämä mahdollisimman pian.
           </div>       </div>   <div id="FAQ_mid">       <div id="FAQ_mid_content_finnish">       <h1>MIKÄ MUUTTUU?<i> (JA MIKÄ PYSYY MUUTTUMATTOMANA?)</i></h1>       <h2>K: Tarkoittaako tämä, että päivityksiä ei enää tule? Lopetatteko nyt ilmaisen sisällön julkaisemisen?</h2>       V: Ei. Suunnitelmamme on jatkaa TF2:n päivittämistä aivan kuten ennenkin, lisäämällä ilmaisia karttoja, pelimuotoja ja ominaisuuksia sekä muuta vastaavaa. Mann Co. -kauppa on yksinkertaisesti vain vaihtoehtoinen tapa hankkia esineitä, jotka muut pelaajat voivat ansaita pelaamisen aikana.       <br /><br />
          <h2>K: Aiotteko pyytää maksua muunkinlaisesta sisällöstä, kuten kartoista?</h2>
           V: Emme, meillä ei ole suunnitelmia tehdä sellaista. Haluamme välttää pelaajien jaottelemista ryhmiin, jotka eivät voi pelata keskenään sen perusteella kuka osti ja mitä.       <br /><br />       <h2>K: Täytyykö minun käyttää rahaa pysyäkseni kilpailukykyisenä?</h2>       A: Ei. Jokainen pelattavuuteen vaikuttava esine, ja jopa suurin osa täysin kosmeettisista esineitä, tulee edelleen olemaan saatavilla yksinkertaisesti pelaamalla peliä.       <br /><br />       <h2>K: Muutitteko olemassa olevien esineiden saantinopeutta?</h2>       V: Ei, emme ole tehneet esineiden saamisesta pelaamalla yhtään sen hankalampaa kuin ennenkään.       <br /><br />
          <h2>K: Miksi kaikki esineeni ovat muuttuneet “Klassikko”-esineiksi?</h2>
           V: Jotkin esineet, jotka olivat harvinaisia, muuttuvat nyt yleisemmäksi kun ne voi hankkia myös ostamalla. Halusimme antaa pelaajille tämän mahdollisuuden, mutta silti tunnustaa ne jotka hankkivat ko. esineet “vanhalla hyvällä tavalla” aikaisemmin. Päätimmekin suorittaa ainutkertaisen käännöksen vanhojen esineiden kohdalla niin, että ne muuttuivat “Klassikkoversioiksi”, joita ei voi hankkia enää tulevaisuudessa. Tämän ansiosta vanhat esineet säilyvät harvinaisina ja ovat itseasiassa yhä harvinaisempia, sillä niiden määrä on nyt rajoitettu.       <br /><br />       <h2>K: Entäpä esineet jotka ansaitsen tulevaisuudessa? Miten muut voivat erottaa ne ostetuista esineistä?</h2>       V: Tällä hetkellä ei ole suunnitelmia tämän eron näyttämiseksi pelaajille. Ostettuja esineitä &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ei kuitenkaan tällä hetkellä voi käyttää vaihtamiseen tai luomiseen, joten on yhä &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hyödyllistä ansaita esineitä “vanhalla hyvällä tavalla”.       <br /><br />       </div>      </div>   <div id="FAQ_bottom">
          <div id="FAQ_bottom_content">
           <h1>VAIHTAMINEN & RÄÄTÄLÖINTI</h1>       <h2>K: Voinko vaihtaa esineitä toisten pelaajien kanssa?</h2>       V: Kyllä. Lisäsimme pitkään toivotun vaihtamisominaisuuden, jotta voit vaihtaa esineitä kavereidesi kanssa.       <br /><br />       <h2>K: Ovatko kaikki esineet vaihdettavia?</h2>       V: Eivät. Eräät rajoitetun saatavuuden esineet eivät ole vaihdettavissa. Ostettuja esineitä ei myöskään voi vaihtaa tällä hetkellä (katso Tietoa kaupasta -osio).       <br /><br />
          <h2>K: Voinko räätälöidä esineitäni?</h2>
           V: Kyllä. Lisänä nykyiseen esineiden räätälöintiin lisäsimme uudenlaisia esineitä, jotka avaavat uusia muokkausmahdollisuuksia, kuten esineiden nimeämisen ja niiden väriteeman vaihtamisen. Mann Co. -katalogin avulla voit tutkia mitkä esineet ovat räätälöitävissä ja miten niitä voi räätälöidä. Olemme myös tuplanneet repun tilavuuden, joten saat varastoitua enemmän uutta tavaraa.       <br /><br />       <h2>K: Voinko lahjoittaa esineitä toisille? Vai täytyykö heidän antaa minulle jotain takaisin?</h2>       V: Kyllä, voit lahjoittaa esineitä muille, eikä heidän tarvitse antaa mitään vastineeksi.       <br /><br />       <h2>K: Voinko luoda Polycount-paketin aseita? Onko niitä varten reseptejä kuten muilla yhteisön tuottamilla esineillä?</h2>       V: Kyllä, ne ovat luotavissa. Niillä on myös sama reseptityyli kuin muilla yhteisön tuottamilla esineillä, eli yhdistä kaksi esinettä.       <br /><br /> 
          </div>

        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
