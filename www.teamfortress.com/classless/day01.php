<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="images/favicon.ico" />
<title>Team Fortress 2 - Classless Update</title>
<link rel="stylesheet" type="text/css" href="main.css" />
<style type="text/css">
#pageContainer, #mainbody, body, html {
	min-height:100%;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('images/01_tf_logo_over.gif')">

<div id="mainContainer">
<center>
	<div id="pageContainer">
		<div id="day01_background">
			<div id="homeLink"><a href="../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','images/01_tf_logo_over.gif',1)"><img src="images/01_tf_logo.gif" alt="home" name="home" width="134" height="36" border="0"></a></div>
			<div id="dayNav"><a href="day02.php">></a></div>
			<div id="day01_content_a">
			Throughout history, men have worn hats as a way of showing how much better they are than other men. “I buy hats,” a behatted man seems to say. “I am better than you.”<br /><br /> 
 			In wartime, hats were a useful way of conferring rank, and ensuring that casualties were confined to the lower classes (hence the famous command of “Don't fire till you see the tops of their heads” at the Battle of Bunker Hill by William Prescott, a general renowned for only shooting enemy combatants who were poor). During peacetime, hats have been instrumental for men to let the non-hatted know just who is wearing the hat around here.<br /><br /> 
 			And that’s the entire history of hats. The history you were spoon fed at school, that is, in your government-run hat class. But here’s a little truth bomb your teachers, pastors and ombudsmen of your regional newspapers DON’T WANT YOU TO KNOW: Team Fortress now has MORE hats. We hope this didn’t blow your mind out of the top of your head, since you’ll need a place to put all these cool new hats.<br /><br />
			</div>
		<div id="hat1"><a href="images/01_soldier_hat.jpg"><img src="images/01_soldier_hat_thumb.jpg" width="244" height="244" border="0"></a></div>
		<div id="hat2"><a href="images/01_spy_hat.jpg"><img src="images/01_spy_hat_thumb.jpg" width="244" height="244" border="0"></a></div>
		<div id="hat3"><a href="images/01_pyro_hat.jpg"><img src="images/01_pyro_hat_thumb.jpg" width="244" height="244" border="0"></a></div>
			<div id="day01_content_b">
			Arena Offblast is a fast-paced, high-altitude community map set in a top secret missile silo wedged into the top of a hollowed-out mountain. Open-air paths and narrow hallways converge on a central control point located just under the silo’s blast doors.<br /><br /> 
 			This mountaintop arena also boasts a sheer cliff face encircling the battleground, giving you the chance to help your opponents improve their tactics. Specifically, anyone you blast over the side will get some time to think about the various flaws in their “standing next to the edge of a cliff” strategy as they plummet to an excruciating, non-respawnable death.<br /><br />
			This awesome community map was created by Magnar "insta" Jenssen, whose dedication and commitment to TF2 map-making has convinced us to make him the tenth class.*<br /><br />
 			*We are lying.
  			</div>
		<div id="day01_screen_a"><a href="images/01_offblast_a.jpg"><img src="images/01_offblast_a_thumb.jpg" width="117" height="73" border="0"></a></div>
		<div id="day01_screen_b"><a href="images/01_offblast_b.jpg"><img src="images/01_offblast_b_thumb.jpg" width="117" height="73" border="0"></a></div>
		<div id="day01_screen_c"><a href="images/01_offblast_c.jpg"><img src="images/01_offblast_c_thumb.jpg" width="117" height="73" border="0"></a></div>	
		</div>
	</div>
	<center>
	<div id="footer">
		<div id="copyright">
			&#169; 2009 Valve Corporation, all rights reserved. Valve, the Valve logo, Half-Life, the Half-Life logo, the Lambda logo, Steam, the Steam logo,<br />
		 	Team Fortress, the Team Fortress logo, Opposing Force, Day of Defeat, the Day of Defeat logo, Counter-Strike, the Counter-Strike logo,<br />
		 	Source, the Source logo, Valve Source and Counter-Strike: Condition Zero are trademarks and/or registered trademarks of Valve Corporation. 
		</div>
	<div id="footerLogo"><img src="images/valve_logo.jpg" height="60" width="111" border="0"></div>
	</div>
	<div class="footerShadowBottom">
	<div id="languageSelect"><a href="day01.php">English</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day01.php?l=french">Français</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day01.php?l=german">Deutsch</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day01.php?l=italian">Italiano</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day01.php?l=spanish">Español </a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day01.php?l=swedish">Svenska</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day01.php?l=russian">Русский</a>
	<div id="forumLink"><a href="https://steamcommunity.com/app/440/discussions/" target="_blank">Discuss @ TF2 Forums</a></div>
		<div id="digg">
			<script type="text/javascript">digg_url = 'https://www.teamfortress.com/classless'; digg_bgcolor = '#bbaca5'; digg_skin = 'compact'; digg_window = 'new'; </script>
			<script src="http://digg.com/tools/diggthis.js" type="text/javascript"></script>
		</div>
	</div>
	</div>
	</center>	
	</div>
  </center>
</div>
</center>
</div>
</body>
</html>
