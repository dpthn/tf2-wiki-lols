<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - The Mann-Conomy Update </title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro">基本上大部分的 TF2 更新於釋出時都是盡可能讓玩家摸不著頭緒，因為我們知道我們的狂熱者將會於更新釋出後透過實際遊戲來自行悟出所有玩意。但是，像這次擁有如此遠大目標的 Mann-conomy 更新，同時也是因為這玩意跟真實的金錢有所牽扯，我們決定暫時把之前那套必須擠眉弄眼來窺探究竟的方式捨棄，並且將關於這次更新將要達成的目的全盤托出。</div> 
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Back to Update Page" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Back to TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top_tchinese">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        		<h1>關於商店</h1> 
           		<h2>Q:  Mann Co. 商店是甚麼？</h2> 
                A: Mann Co. 商店能夠從 TF2 中開啟。這個商店將透過 Steam Wallets 功能，讓玩家進行購買、物品交換，以及自訂遊戲史上擁有最豐富的額外物品庫中絕大部分的玩意。
            	<br /><br />
                <h2>Q: Steam Wallet 是甚麼？</h2> 
                A: Steam 現在擁有一種新的功能，稱做「Steam Wallet（Steam 皮夾，暫譯）」。這項功能讓您能夠將金錢存在其中，並且能在遊戲內商店（例如 TF2 的 Mann Co, 商店）使用，或是透過 Steam 商店來購買一般的遊戲產品。
            	<br /><br />
           		<h2>Q: 商店內的物品將以點數來標價嗎，還是其他詭異的幣值？</h2> 
              A: 並不是，Steam 皮夾使用的是真實世界中相同的幣值（美金/英鎊/歐元）。 <br /><br /> 
                <h2>Q: 我可以想放多少錢就放多少錢在我的皮夾內嗎？</h2> 
              A: 當然。我們並不會強制您購買特定的「組合包」貨幣。您可以購買您感興趣的物品，並且指定確切的價格。然而，每當您將金錢存入您的 Steam 皮夾內時，您必須至少存入 5 美金 / 4 英鎊 / 5 歐元，才能通過交易付款服務提供者的最低金額底線。<br /><br /> 
                <h2>Q: 我能夠交易或是精製我所購買的物品嗎？</h2> 
            A: 以目前初期來說，不行。您只能交換或是精製您透過一般遊戲過程所獲得的物品（請參考最下方的「物品交換與自訂化」）。我們在初期實施這項政策，直到我們能夠了解到能夠交換購買的物品會對 Mann-conomy 造成甚麼實質的影響。<br /><br /> 
                <h2>Q: 所以我的 「TF2 幣」在 Steam 上也可以用？</h2> 
          A: 是的。您在 Steam 皮夾內的錢可以購買 Steam 上的任何商品。<br /><br /> 
                <h2>Q: 你們只會賣 Valve 打造的物品嗎？那社群貢獻物品呢？</h2> 
              A: 這兩樣商店都會販售。另外，社群貢獻者將會獲得他們創作商品的部分營收！我們將會釋出更多相關的消息讓大家瞭解這套系統將怎樣運作。<br /><br /> 
                <h2>Q: 我是一名社群貢獻者，而我的物品已經在遊戲中了。為什麼我的物品沒有出現在  Mann Co. 商店內呢？</h2> 
            A: 這項更新最初，我們只打算販售 Polycount 社群物品組合包，這讓我們可以只跟少數的貢獻者一起工作研究。當我們能夠完整掌握貢獻者應得的報酬後，我們將會擴展商店的規模到所有社群貢獻物。我們計畫是愈快愈好。</div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content">
            <h1>這將會改變甚麼？<i>（而甚麼又將維持原樣？）</i></h1> 
            <h2>Q: 這是否表示更新結束了？你們是否將不再釋出免費更新？</h2> 
            A: 並不是。我們的計畫是將會繼續更新 TF2，新增免費地圖、遊戲模式、新功能以及更多，就像往常一樣。當其他玩家依舊可以透過遊戲來獲得物品時，Mann Co. 商店只是另一種物品取的的方法。<br /><br /> 
            <h2>Q: 你們是否將要對這些遊戲內容收費，像是地圖之類的？</h2> 
            A: 沒有，我們沒有計畫去做這個。將玩家用是否購買某樣物品來區分，不能一起進行遊戲的這種狀況，是我們會避免其發生的。<br /><br /> 
            <h2>Q: 我是否必須花錢才能保持競爭力？</h2> 
            A: 當然不是。任何會影響遊戲內容，甚至單純的外觀物品，依舊可以透過進行遊戲來獲得。<br /><br /> 
            <h2>Q: 你們有調整過現有物品的掉落機率嗎？</h2> 
            A: 沒有。我們不曾做過讓那些讓現有物品（透過一般遊戲過程掉落取得）更難拿到的動作。<br /><br /> 
            <h2>Q: 為什麼我所有的物品都變成了「復古（Vintage）」裝備？</h2> 
            A: 某些本來就很稀有的物品將會因為可以直接購買而變的普遍。我們雖然給了玩家這個方式，但仍舊想要區別那些過去透過「經典方式」獲取的物品。所以我們決定做一次性的轉換，將所有已經擁有的物品改成「復古」版本，這些物品在將來是不會再出現的。如此一來，這些老骨董就能保持其稀有價值（事實上，不只是很稀有而已，還是限量版）。<br /><br /> 
            <h2>Q: 那我之後拿到的物品呢？要怎樣與購買的物品做區別？</h2> 
            A: 目前並沒有計畫讓其他玩家明顯地看出物品的差異。然而，購買的物品目前無法進行交易與精製，所以透過「經典方式」取得物品還是有其優點的。<br /><br /> 
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
            <h1>物品交換與自訂化</h1> 
            <h2>Q: 我是否可以交換物品？</h2> 
            A: 是的。我們新增了讓人望穿秋水的物品交換功能，使您能夠和朋友交換物品。<br />
            <br /> 
            <h2>Q: 所有的物品都可以交換嗎？</h2> 
            A: 不是，並非所有的物品都可以進行交換。某些擁有特殊限制的物品是無法交換的。而目前購買的物品同樣也是無法進行交換（請參閱「關於商店」）。<br />
            <br /> 
            <h2>Q: 我可以自訂我的物品嗎？</h2> 
            A: 當然可以。除了目前內建的物品自訂功能外，我們還做了許多不同種類的物品，讓可自訂化的項目比過去多更多，例如現在可以變更物品的名稱，甚至其色彩主題。Mann Co. 目錄可以讓您了解到那些物品是可以進行自訂功能的，以及您能夠怎樣去自訂它們。我們同時還將背包的空間增加一倍，讓您能夠有足夠的空間放置這些全新的玩意。<br />
            <br /> 
            <h2>Q: 我可以單純的只把物品給別人嗎？還是他們一定要給我某些回饋？</h2> 
            A: 是的，您可以「贈送」物品給其他玩家。他們並非必須給您回饋。<br />
            <br /> 
            <h2>Q: 我是否可以精製出 Polycount 更新的武器？它們是否像其他社群武器擁有特定的合成配方？</h2> 
            A: 是的，這些玩意是可以被精製出來的。他們擁有與其他社群貢獻物品相同形式的合成配方（結合兩樣物品）。<br />
            <br /> 
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
