<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - Mann-conomy Updateringen </title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro">Typisk, når vi arbejder med TF2-opdateringer kan vi godt lide at afsløre så lidt som muligt, vel vidende at vores fans vil regne alting ud når de begynder at spille opdateringen. Men da Mann-conomy opdateringen er så ambitiøs, og fordi den involverer virkelige penge, har vi bestemt os for at sætte vores vinken, prikken og drilleri til side i et øjeblik, for at fortælle præcis hvad vi har gang i og havd denne opdatering handler om.</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Back to Update Page" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Back to TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
        		<h1>OM BUTIKKEN</h1>
           		<h2>Q: Hvad er en Mann Co. Butik?</h2>
                A: Mann Co. Butikken er nu tilgængelig fra inde i TF2, og den vil lade spillere købe, bytte and personalisere den største nye mængde genstande i spillets historie, ved hjælp af deres Steam Wallets.
            	<br /><br />
                <h2>Q: Hvad er en Steam Wallet?</h2>
                A: Steam har nu en funktion kaldet Steam Wallet. Du kan putte penge i din Wallet, som du så senere kan bruge til at købe genstande i spillet, såsom dem i TF2’s Mann Co. Butik, eller på hele prdukter som spil i Steam butikken.
            	<br /><br />
           	<h2>Q: Er tingene i butikken vurderet i point, eller en eller anden mærkelig valuta?</h2>
                A: Nej, Steam Wallet bruger de samme valuta som Steam butikken understøtter (USD/GBP/Euro).
            	<br /><br />
                <h2>Q: Kan jeg putte så mange penge som jeg har lyst til i min Wallet?</h2>
                A: Ja. Vi tvinger dig ikke til at købe "bunker" af penge. Du kan putte præcis den mængde penge du skal bruge for at købe en ting du er interesseret i, i din Wallet. Hver gang du køber penge til din Wallet, er der dog en minimumsgrænse på $5 / £4 / 5€ for at holde transaktions- og serviceudbydergebyr på et minimum.
            	<br /><br />
                <h2>Q: Kan jeg bytte eller Crafte ting som jeg har købt?</h2>
                A: På kort sigt, nej. Du kan kun bytte og Crafte ting som du enten har optjent eller fundet i spillet (se Bytning og Personalisering nedenfor). Vi har påtaget os denne politik indtil videre, indtil vi kan se hvilken effekt det har på Mann-conomien at kunne bytte købte ting.
            	<br /><br />
                <h2>Q: Er mine "TF2 penge" brugbare andre steder på Steam?</h2>
                A: Ja. Pengene i din Steam Wallet kan bruges til at købe hvad som helst på Steam.
                <br /><br />
                <h2>Q: Vil i kun sælge Valve-skabte ting, eller kan vi også købe ting der er lavede af Fællesskabet?</h2>
                A: Butikken vil sælge begge dele. Desuden vil spillere der skaber genstande til butikken få en procentdel af salgsprisen på deres genstande! Vi vil udgive mere information om hvordan det virker, snart.
            	<br /><br />
                <h2>Q: Jeg er en spiller som har lavet en genstand der er kommet med i spillet. Hvorfor er den ikke i Mann Co. Butikken?</h2>
                A: Til at starte med vil vi kun sælge genstande fra Polycount-pakken, da det vil lade os ordne de problemer der måtte være sammen med en lille gruppe af deltagere. Når vi har mestret deltagerbetaling vil vi udvide Butikken til at sælge alle spillerskabte genstande. Vi planlægger at gøre dette så hurtigt som muligt.
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content">
            <h1>HVAD VIL ÆNDRE SIG?<i> (OG HVAD VIL FORBLIVE?)</i></h1>
            <h2>Q: Betyder det her at opdateringerne er ovre? Vil i stoppe med at udgive gratis opdateringer nu?</h2>
            A: Nej. Vores plan er at blive ved med at opdatere TF2 ligesom vi altid har gjort, med nye kort, spilmåder, indhold, og mere. The Mann Co. Butikken er simpelthen en alternativ metode til at få fat på genstande som andre spillere kan få i spillet.
            <br /><br />
            <h2>Q: Har i planer om at tage penge for andre former for indhold, såsom kort?</h2>
            A: Nej, vi har ingen planer om at gøre sådan noget. At opdele spillere i grupper der ikke kan spille sammen, baseret på hvem der har købt hvad, er noget vi helst vil undgå.
            <br /><br />
            <h2>Q: Er jeg nødt til at bruge penge for at forblive kampklar?</h2>
            A: Nej. Alle ting der har noget med gameplayet at gøre, og også de fleste kosmetiske genstande, vil stadig være tilgængelige bare ved at spille spillet.
            <br /><br />
            <h2>Q: Har i justeret chancen for at finde de eksisterende genstande?</h2>
            A: Nej. Vi har ikke gjort det sværere at finde genstande gennem gameplayet.
            <br /><br />
            <h2>Q: Hvorfor har alle mine ting skiftet til "Vintage" genstande?</h2>
            A: Nogle ting som var sjældne vil blive mere sædvanlige når de kan købes. Vi ville give spillerne denne mulighed, men samtidig også belønne spillere der fandt deres ting på den “gammeldags facon” før Mann-conomy opdateringen. Så vi bestemte os for at lave en en-gangs konversion af alle gamle genstande til "Vintage" versioner, som du aldrig vil kunne samle i fremtiden. På denne måde bliver de gamle, sjældne genstande ved med at være sjældne (faktisk er de endnu mere sjældne nu, da der er et begrænset antal).
            <br /><br />
            <h2>Q: Hvad med ting som jeg samler i fremtiden? Hvordan kan andre kende forskel på dem og købte genstande?</h2>
            A: Der er ingen planer om at gøre forskellen på de to synlige for spillerne. Købte genstande kan dog ikke byttes eller Craftes, så der er stadig fordele ved at indtjene dine ting på den "gammeldags facon".
            <br /><br />
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
            <h1>TRADING & CUSTOMIZATION</h1>
            <h2>Q: Vil jeg være i stand til at bytte genstande?</h2>
            A: Ja. Vi har nu tilføjet den længe ventede Byttefunktion, så du kan bytte genstande med dine venner.
            <br /><br />
            <h2>Q: Kan man bytte med alle genstande?</h2>
            A: Nej. Du kan ikke bytte med alle genstande. Nogle genstande med begrænset tilgængelighed kan ikke byttes. Købte genstande kan heller ikke byttes (se Om Butikken).
            <br /><br />
            <h2>Q: Kan jeg personalisere mine genstande?</h2>
            A: Ja. Udover den genstandspersonalisering vi allerede har, har vi skabt nye typer genstande som vil tillade en større personalisering end vi har haft før, såsom muligheden for at navngive genstande og ligefrem skifte deres farver. Mann Co. Kataloget vil lade dig finde ud af hvilke genstande du kan personalisere og hvordan du kan gøre det. Vi har også fordoblet pladsen i din rygsæk, så du har mere plads til at opbevare alle disse nye sager.
            <br /><br />
            <h2>Q: Kan jeg bare "give" en genstand til nogen, eller skal de give mig noget til gengæld?</h2>
            A: Ja. Du kan sende genstande som en gave. Modtageren behøver ikke nødvendigvis at give noget igen.
            <br /><br />
            <h2>Q: Kan jeg Crafte Polycount-genstandene? Har de opskrifter som de andre fællesskabsgenstande?</h2>
            A: Ja, de kan Craftes. De har den samme type opskrift (kombiner to genstande) som de andre spiller-skabte genstande.
            <br /><br />
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
