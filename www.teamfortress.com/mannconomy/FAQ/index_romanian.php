<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - Actualizarea "Mann-Conomy"</title> 
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
<div id="FAQ_intro">In mod normal cu actualizarile pentru TF2, noi vrem sa fim cât mai vagi posibil înainte sa le lansam, stiind ca fanii nostri isi vor da seama de toate lucrurile singuri indata intra in joc si incep sa se joace.Dar, cu o actualizare atat de ambitioasa precum actualizarea “Mann-conomy”, deoarece implica folosirea banilor adevarati, am vrut sa dam la o parte nevoia de a va tachina si in schimb sa va aratam exact ce dorim sa facem si despre ce este vorba in aceasta actualizare. </div> 
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Back to Update Page" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Back to TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
<h1>DESPRE MAGAZIN </h1> 
<h2>Q: Ce este Magazinul Mann Co. ?</h2> 
A: Accesibil din TF2, Magazinul Mann Co. permite jucatorilor sa cumpere, sa schimbe si sa personalizeze cea mai mare colectie de obiecte din istoria jocului folosind Portofelul Steam.
            	<br /><br />
<h2>Q: Ce este Portofelul Steam?</h2> 
A: Utilizatorii Steam au acum posibilitatea de a folosi Portofelul Steam. Îti permite sa pui bani in el, bani ce pot fi folositi ori cumparand obiecte in joc, precum cele din Magazinul Mann Co. sau jocuri din Magazinul Steam.
            	<br /><br />
<h2>Q: Obiectele din magazin au pretul sub forma de puncte sau alta valuta?</h2> 
A: Nu, Portofelul Steam foloseste aceeasi valuta folosita pe Steam (Dolar/Lira/Euro).
            	<br /><br />
<h2>Q: Pot pune orice suma de bani in portofelul meu?</h2> 
A: Da. Nu va vom forta sa cumparati valuta "cu tona". Poti pune exact suma necesara pentru a cumpara obiectele dorite de tine. Dar, de fiecare data cand pui o suma in portofel, valoarea minima a fondului depus trebuie sa fie de $5 / £4 / 5€ pentru a mentine costul tranzactiilor si a furnizatorului de transfer a banilor la un minim.  
            	<br /><br />
<h2>Q: Pot schimba sau crea obiectele cumparate?</h2> 
A: Pe termen scurt, nu. Poti schimba sau crea obiectele obtinute sau gasite in joc (vezi sectiunea Schimburi si Personalizari). Am adoptat aceasta politica pentru moment, pana cand putem sa vedem ce efecte ar avea asupra "Mann-conomiei" schimburile folosind obiectele cumparate.
            	<br /><br />
<h2>Q: Sunt banii "TF2" buni si altundeva pe Steam?</h2> 
A: Da. Banii din Portofelul Steam pot fi folositi pentru a cumpara orice de pe Steam.
                <br /><br />
<h2>Q: Veti vinde numai obiecte create de Valve? Sau si obiectele create de comunitate?</h2> 
A: Magazinul va vinde ambele. În plus, ce care contribuie vor primi un procent din vanzarile pe obiectele create de ei! Vom lansa mai multe informatii despre modul in care acest lucru va functiona.
            	<br /><br />
<h2>Q: Sunt un membru a comunitatii care a contribuit cu un obiect care a fost pus in joc. De ce nu este obiectul meu in Magazinul Mann Co. ?</h2> 
A: Initial, noi vom vinde numai obiectele din pachetul de comunitate Polycount, ce ne va permite sa perfectionam sistemul folosind un numar mic de contribuitori. Îndata ce vom fi stapani pe sine cu platile pentru contribuitori, vom mari magazinul pentru a cuprinde pe toate contributiile comunitatii. Planuim sa facem acest lucru cat de curand.
            </div>	
        </div>
        <div id="FAQ_mid">
        	<div id="FAQ_mid_content">
<h1>CE SE VA SCHIMBA? <i> (SI CE VA RAMÂNE LA FEL?)</i></h1> 
<h2>Q: Acest lucru inseamna ca actualizarile se vor opri? Veti lansa numai actualizari pentru care trebuie sa platim de acum incolo?</h2> 
A: Nu. Planul nostru este sa actualizam in continuare TF2 ca intotdeauna, adaugand gratuit harti, moduri de joc, optiuni noi, si multe altele. Magazinul Mann Co. este doar o metoda alternativa de a obtine obiecte ce alti jucatori le primesc in timpul jocului.
            <br /><br />
<h2>Q: Veti incepe sa taxati pentru alt fel de continut, precum harti?</h2>
	
A: Nu, nu avem de gand sa facem asta. Încercam sa evitam segregarea jucatorilor in grupuri care nu pot juca impreuna din pricina a ceea ce au cumparat sau nu au cumparat.
            <br /><br />
<h2>Q: Va trebui sa platesc ca sa raman competitiv?</h2>
A: Nu. Orice obiect care afecteaza jocul, chiar si obiectele cosmetice, inca vor putea fi obtinute pur si simplu jucand.
            <br /><br />
<h2>Q: Ati modificat intervalul la care se gasesc obiectele?</h2>
	
A: Nu. Nu am ingrunat modul in care se obtin obiectele jucand.
            <br /><br />
<h2>Q: De ce toate obiectele mele au devenit "de epoca" ?</h2>
A: Unele obiecte ce obisnuiau sa fie rare vor deveni mai comune pe masura ce sunt disponibile in magazin. Am vrut sa oferim jucatorilor aceasta optiune, dar sa recunoastem in continuare jucatorii care au obtinut obiectele “in modul traditional” in trecut. Asa ca am decis ca toate obiectele sa fie transformate, o data, in obiecte "de epoca", ce nu vor mai putea fi obtinute in viitor. În acest mod, acele obiecte raman rare (si chiar si mai rare de indata ce sunt versiuni limitate).
            <br /><br />
<h2>Q: Dar obiectele ce le voi obtine in viitor? Cum imi voi da seama de diferenta dintre cele cumparate si cele obtinute normal?</h2> 
A: Nu avem niciun plan pentru a face aceasta distinctie vizibila pentru jucatori. Dar, obiectele cumparate nu pot fi schimbate sau create cu alte obiecte, asa ca inca au un plus obiectele obtinute “in modul traditional”.
            <br /><br />
            </div>   
        </div>
        <div id="FAQ_bottom">
        	<div id="FAQ_bottom_content">
<h1>SCHIMBURI SI PERSONALIZARI </h1> 
<h2>Q: Pot sa dau la schimb obiectele mele?</h2> 
	
A: Da. Am adaugat optiunea mult asteptata de “Schimb”, pentru a putea face trocuri cu prietenii tai.
            <br /><br />
<h2>Q: Pot sa dau la schimb toate obiectele?</h2> 
A: Nu, nu toate obiectele se pot schimba cu altele. Unele obiecte cu disponibilitate limitata nu se pot schimba. Obiectele cumparate nu se pot schimba pentru moment (vezi sectiunea Despre Magazin).
            <br /><br />
<h2>Q: Pot sa personalizez obiectele mele?</h2> 
A: Da. Pe langa modul actual de personalizare a obiectelor, noi am creat tot felul de obiecte ce dau frau liber personalizarii, precum abilitatea de a schimba numele si culoarea la unele obiecte. Magazinul Mann Co. iti permite sa gasesti alte obiecte ce pot fi personalizate si cum pot fi personalizate. De asemenea, am dublat spasiul alocat in rucsac pentru a avea loc pentru obiectele noi.
            <br /><br />
<h2>Q: Pot sa dau obiecte altor persoane? Sau trebuie neaparat sa imi dea inapoi alte obiecte?</h2> 
	
A: Da, puteti da "cadou" obiectele tale altor oameni. Ei nu trebuie sa va dea inapoi nimic. 
            <br /><br />
<h2>Q: Pot crea armele din pachetul Polycount? Au retete precum celelalte obiecte facute de comunitate?</h2> 
A: Da, se pot crea. Au acelasi fel de retete (trebuie combinate doua obiecte) precum celelalte obiecte contribuite de comunitate.
            <br /><br />
            </div>
        </div>
		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
