<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<title>Team Fortress 2 - Saxxy Awards</title>
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="css/global.css?v=22" />
<style type="text/css">
#nom_main_content {
	background-image:url( 'images/awards_bg4.jpg' );
}
</style>
<script language="javascript">
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<center>
<div id="nom_main_content">
	<div id="winner_videos">
		<div id="videos_inner">
			<div id="tflink"><a href="../index.html"><img src="images/trans.gif" width="215" height="65" border="0" /></a></div>
			<div id="winners_category_dropdown">
				<form action="winners.php" id="category_form">
				Category: <select name="category" id="category_select">
					<option value="0">Most Inventive Kill</option>
					<option value="1">Best Mid-air Murder</option>
					<option value="2">Biggest Massacre</option>
					<option value="3">Funniest Replay</option>
					<option value="4">Best Getaway </option>
					<option value="5">Best Revenge</option>
					<option value="6">Mostest Pwnage</option>
					<option value="7">Most Heroic</option>
					<option value="8">Best Set Design</option>
					<option value="9">Best Team Costume</option>
					<option value="10">Best Original Soundtrack</option>
					<option value="11">Best 30 second trailer</option>
					<option value="12">Best Coordinated Combat</option>
					<option value="13">Most Dramatic</option>
					<option value="14">Best Cinematography</option>
					<option value="15">Best Editing</option>
					<option value="16">Most Epic Fail</option>
					<option value="17">Most Extreme Stunt</option>
					<option value="18">Players' Choice</option>
					<option value="19" selected="selected">Best Overall</option>
				</select> <input type="submit" value="View" id="category_submit" /></form>
			</div>
			<div class="cat_list_col" style="width:225px; margin-left:10px;">
			<a href="winners.php?category=0">1. Most Inventive Kill</a><br />
			<a href="winners.php?category=1">2. Best Mid-air Murder</a><br />
			<a href="winners.php?category=2">3. Biggest Massacre</a><br />
			<a href="winners.php?category=3">4. Funniest Replay</a><br />
			<a href="winners.php?category=4">5. Best Getaway </a><br />
			<a href="winners.php?category=5">6. Best Revenge</a><br />
			<a href="winners.php?category=6">7. Mostest Pwnage</a><br />
			</div>
			<div class="cat_list_col" style="width:265px;">
			<a href="winners.php?category=7">8. Most Heroic</a><br />
			<a href="winners.php?category=8">9. Best Set Design</a><br />
			<a href="winners.php?category=9">10. Best Team Costume</a><br />
			<a href="winners.php?category=10">11. Best Original Soundtrack</a><br />
			<a href="winners.php?category=11">12. Best 30 second trailer</a><br />
			<a href="winners.php?category=12">13. Best Coordinated Combat</a><br />
			<a href="winners.php?category=13">14. Most Dramatic</a><br />
			</div>
			<div class="cat_list_col" style="width:235px; margin-right:0px">
			<a href="winners.php?category=14">15. Best Cinematography</a><br />
			<a href="winners.php?category=15">16. Best Editing</a><br />
			<a href="winners.php?category=16">17. Most Epic Fail</a><br />
			<a href="winners.php?category=17">18. Most Extreme Stunt</a><br />
			<a href="winners.php?category=18">19. Players' Choice</a><br />
			<a href="winners.php?category=19">20. Best Overall</a><br />
			</div>
		</div>
	</div>
</div>
<div id="nom_footer"><img src="images/v_foot.jpg" width="802" height="59" border="0" /></div>
</center>
</body>
</html>
