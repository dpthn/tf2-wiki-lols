<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" />
<title>Team Fortress 2 - The Mann-Conomy Update </title>
<link rel="stylesheet" type="text/css" href="../main.css" />
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onLoad="MM_preloadImages('../images/FAQ_ManncoLink_over.jpg','../images/FAQ_TFLink_over.jpg')">
<div id="mainContainer">
	<div id="FAQContainer">
    	<div id="FAQ_intro">Di solito, prima del lancio degli aggiornamenti di TF2  siamo molto evasivi. Infatti, sappiamo che tanto i nostri fan riescono a scoprire tutto da soli durante le partite, una volta che il gioco è stato lanciato. Tuttavia, con un aggiornamento di tale portata come è la Mann-conomia, faremo un'eccezione. Eviteremo ammiccamenti e congetture, e sveleremo chiaramente cosa stiamo facendo e qual è la logica sottesa a questo aggiornamento... anche perché, stavolta ci sono i soldi di mezzo!</div>
        <div id="FAQ_ManncoLink"><a href="https://www.teamfortress.com/mannconomy" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Mann Co Link','','../images/FAQ_ManncoLink_over.jpg',1)"><img src="../images/FAQ_ManncoLink.jpg" alt="Back to Update Page" name="Mann Co Link" width="192" height="62" border="0"></a></div>
        <div id="FAQ_TFLink"><a href="../../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('TF2 Home Link','','../images/FAQ_TFLink_over.jpg',1)"><img src="../images/FAQ_TFLink.jpg" alt="Back to TF2.com" name="TF2 Home Link" width="192" height="62" border="0"></a></div>
    	<div id="FAQ_top_italian">
			<div id="FAQ_top_content">        
				<div id="language_dropdown_area">
					﻿<script type="text/javascript" src="prototype-1.6.0.2.js"></script>
<script type="text/javascript" src="main.js"></script>
<span class="pulldown global_action_link" id="language_pulldown" onclick="ShowMenu( this, 'language_dropdown', 'right' );">Language</span>
<div class="popup_block" id="language_dropdown" style="display: none;">
	<div class="popup_body popup_menu shadow_content" style="width: 133px;">
		<a class="popup_menu_item tight" href="index.php">English</a>
		<a class="popup_menu_item tight" href="index_danish.php">Dansk</a>
		<a class="popup_menu_item tight" href="index_dutch.php">Nederlands</a>
		<a class="popup_menu_item tight" href="index_finnish.php">Suomi</a>
		<a class="popup_menu_item tight" href="index_french.php">Français</a>
		<a class="popup_menu_item tight" href="index_german.php">Deutsch</a>
		<a class="popup_menu_item tight" href="index_italian.php">Italian</a>
		<a class="popup_menu_item tight" href="index_hungarian.php">Magyar</a>
		<a class="popup_menu_item tight" href="index_polish.php">Polski</a>
		<a class="popup_menu_item tight" href="index_romanian.php">Română</a>
		<a class="popup_menu_item tight" href="index_russian.php">Русский</a>
		<a class="popup_menu_item tight" href="index_spanish.php">Español</a>
		<a class="popup_menu_item tight" href="index_schinese.php">简体中文</a>
		<a class="popup_menu_item tight" href="index_tchinese.php">繁體中文</a>
		<a class="popup_menu_item tight" href="mailto:translationserver@valvesoftware.com">(submit translation)</a>
	</div>
</div>

				</div>	
<H1>IL NEGOZIO</H1>
<H2>D: Cos'&egrave; il Negozio Mann Co.?</H2>R: Accessibile da TF2, il Negozio Mann Co.
consentir&agrave; ai giocatori di acquistare, barattare e personalizzare la pi&ugrave; vasta gamma
di oggetti dell'inventario nella storia del gioco, tramite il loro Portafoglio Steam.<BR><BR>
<H2>D: Cos'&egrave; un Portafoglio Steam?</H2>R: Steam adesso ha a disposizione il nuovo Portafoglio Steam.
Ti consente di aggiungervi denaro, che pu&ograve; essere speso sia per oggetti in gioco, come nel Negozio
Mann Co. di TF2, sia su prodotti come giochi nel Negozio di Steam.<BR><BR>
<H2>D: Il prezzo degli oggetti nel negozio &egrave; espresso in punti, o in qualche altra strana valuta?</H2>
No, il Portafoglio Steam usa le stesse valute adottate da Steam attualmente (Dollari/Sterline Inglesi/Euro)<BR><BR>
<H2>D: Posso aggiungere qualunque quantit&agrave; di denaro al mio portafoglio?</H2>
R: S&igrave;. Non ti obblighiamo ad aggiungere quantit&agrave; fisse. Potrai inserire la quantit&agrave; desiderata per acquistare
gli oggetti specifici che suscitano il tuo interesse. Tuttavia, ogni volta che aggiungi fondi al portafoglio,
la minima quantit&agrave; da aggiungere &egrave; 5$ / 4&pound; / 5&euro; per tenere bassi i costi di gestione del servizio per ogni transazione.<BR><BR>
<H2>D: Posso barattare o combinare gli oggetti che ho acquistato?</H2>R:
In breve, no.
Puoi solo barattare o combinare oggetti che ti sei guadagnato o che hai trovato durante la sessione di gioco (consulta la sezione
Baratto &amp; Personalizzazione qui sotto). Abbiamo adottato questa soluzione per adesso, finch&eacute; non riusciremo a studiare bene
l'effetto che gli oggetti acquistati avranno sulla Mann-conomia.<BR><BR>
<H2>D: I soldi che ho aggiunto per TF2 sono validi altrove su Steam?</H2>R: 
S&igrave;. Il denaro presente nel tuo Portafoglio Steam pu&ograve; essere usato per acquistare qualunque cosa su Steam.<BR><BR>
<H2>D: Venderete solo oggetti creati da Valve? E per quanto riguarda gli oggetti fatti dalla Comunit&agrave;?</H2>R: 
Il Negozio vender&agrave; entrambi. Inoltre, i contributori della Comunit&agrave; riceveranno una percentuale dei profitti sugli oggetti creati!
Rilasceremo presto maggiori informazioni riguardo al funzionamento di questo sistema.<BR><BR>
<H2>D: Sono un membro della Comunit&agrave; che ha contribuito con un oggetto che &egrave; stato inserito nel gioco. Perch&eacute; non si trova nel Negozio Mann Co.?</H2>R:
Inizialmente, venderemo solo gli oggetti della Comunit&agrave; del Polycount Pack, in quanto sar&agrave; pi&ugrave; facile organizzarci con un minor numero di contribuenti.
Quando avremo imparato a gestire i pagamenti ai contribuenti in modo efficiente, espanderemo il Negozio per includere tutti i contributi della Comunit&agrave;.
&eacute; nei nostri piani che avvenga il prima possibile.</DIV></DIV>
<DIV id=FAQ_mid>
<DIV id=FAQ_mid_content>
<H1>COSA CAMBIER&Agrave;?<I> (E COSA RESTER&Agrave; UGUALE?)</I></H1>
<H2>D: Questo significa che gli aggiornamenti sono conclusi? Smetterete di rilasciare contenuti gratuiti adesso?</H2>
R: No. I nostri piani sono di continuare ad aggiornare TF2 come abbiamo sempre fatto, aggiungendo gratuitamente mappe, stili di gioco,
nuove caratteristiche, e molto altro. Il Negozio Mann Co. &egrave; semplicemente un modo alternativo di ottenere gli oggetti che altri giocatori
possono trovare durante la sessione di gioco.<BR><BR>
<H2>D: Comincerete a farci pagare per altri tipi di contenuti, come le mappe?</H2>R: 
No, non ne abbiamo intenzione. Segregare i giocatori in gruppi che non possono giocare insieme, basandosi su ci&ograve; che hanno comprato, &egrave; qualcosa che vogliamo evitare.<BR><BR>
<H2>D: Dovr&ograve; spendere denaro per rimanere competitivo?</H2>R: 
No. Qualunque oggetto che influenzi il gioco, e anche gli oggetti puramente cosmetici, saranno ottenibili semplicemente giocando.<BR><BR>
<H2>D: Avete ritoccato il tasso di ritrovamento degli oggetti esistenti?</H2>R: 
No. Non abbiamo reso pi&ugrave; difficile ottenere nuovi oggetti durante il gioco.<BR><BR>
<H2>D: Perch&eacute; tutti i miei oggetti hanno la dicitura "Vintage"?</H2>R: 
Alcuni oggetti che erano rari diventeranno pi&ugrave; comuni quando saranno disponibili per l'acquisto. Volevamo dare ai giocatori questa opzione, pur dando un merito a chi li aveva ottenuti
col "vecchio metodo" in passato. Quindi, abbiamo deciso di convertire per una volta tutti gli oggetti vecchi nelle relative versioni "Vintage", che non saranno mai pi&ugrave; disponibili in futuro.
In questo modo, i vecchi oggetti rimarranno rari (in realt&agrave;, ancora pi&ugrave; rari, perch&eacute; saranno in edizione limitata da adesso).<BR><BR>
<H2>D: E per quanto riguarda gli oggetti che otterr&ograve; in futuro? Come potranno gli altri distinguerli da quelli acquistati?</H2>R: 
Non ci sono attualmente piani per renderli distinguibili dagli altri giocatori. Tuttavia, gli oggetti acquistati non sono attualmente barattabili o combinabili, quindi c'&egrave; sempre
un benefici nel guadagnarseli col "vecchio metodo".<BR><BR></DIV></DIV>
<DIV id=FAQ_bottom>
<DIV id=FAQ_bottom_content>
<H1>BARATTO &amp; PERSONALIZZAZIONE</H1>
<H2>D: Potr&ograve; barattare gli oggetti?</H2>R: 
S&igrave;. Abbiamo aggiunto questa nuova caratteristica da sempre desiderata, il Baratto, cos&igrave; potrai scambiare oggetti con i tuoi amici.<BR><BR>
<H2>D: Posso barattare qualunque oggetto?</H2>R: 
No, non tutti sono barattabili. Alcuni oggetti prodotti in quantit&agrave; limitata non sono barattabili. Al momento, anche gli oggetti acquistati non sono barattabili (consulta la sezione Il Negozio).<BR><BR>
<H2>D: Posso personalizzare i miei oggetti?</H2>R: 
S&igrave;. In aggiunta alla personalizzazione degli oggetti esistenti, abbiamo creato nuovi tipi di oggetti che consentiranno una pi&ugrave; elevata personalizzazione rispetto al passato,
come la possibilit&agrave; di cambiarne il nome e perfino il colore. Il Catalogo Mann Co. ti consentir&agrave; di scoprire quali oggetti sono personalizzabili, e in che modo.
Abbiamo anche raddoppiato lo spazio nello zaino, cos&igrave; avrai la possiblit&agrave; di riempirlo con tutta la nuova attrezzatura.<BR><BR>
<H2>D: Posso donare oggetti a qualcuno? O devono obbligatoriamente darmi qualcosa in cambio?</H2>R: 
S&igrave;, ti &egrave; consentito donare oggetti ad altri. Non &egrave; necessario che ricambino il favore.<BR><BR>
<H2>D: Posso creare le armi del Polycount Pack? Hanno delle ricette come gli altri oggetti della Comunit&agrave;?</H2>R: 
S&igrave;, sono disponibili per la creazione. Hanno lo stesso stile delle ricette degli altri oggetti realizzati dalla Comunit&agrave; (combinare due armi).
<BR><BR></DIV></DIV><A href="https://www.valvesoftware.com/">		
    	<a href="https://www.valvesoftware.com/"><img src="../images/footer.png" width="880" height="104" border="0"></a>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
</body>
</html>
