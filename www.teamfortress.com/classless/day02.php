<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="images/favicon.ico" />
<title>Team Fortress 2 - Classless Update</title>
<link rel="stylesheet" type="text/css" href="main.css?x=3" />
<style type="text/css">
#pageContainer, #mainbody, body, html {
	min-height:100%;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('images/02_tf_logo_over.gif')">

<div id="mainContainer">
<center>
	<div id="pageContainer">
		<div id="day02_background">
			<div id="homeLink"><a href="../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','images/02_tf_logo_over.gif',1)"><img src="images/02_tf_logo.gif" alt="home" name="home" width="134" height="36" border="0"></a></div>
			<div id="dayNav"><a href="day01.php"><</a>&nbsp;<a href="english.htm">></a></div>
				<div id="day02_content_a">
					King of the Hill (KotH) is an intense new game mode focused around a single central capture point that must be defended for three minutes. Gameplay begins with the point initially locked—after a short time, it will open for capture by either team. Once the point is captured by a team, their team clock will start a three minute countdown. If the enemy team manages to capture the point back, their clock will start counting down while the other team’s clock freezes at the time the point was recaptured. 
				</div>
				<div id="day02_screen_a"><a href="images/02_screen_a.jpg"><img src="images/02_screen_a_thumb.jpg" width="78" height="58" border="0"></a></div>
				<div id="day02_screen_b"><a href="images/02_screen_b.jpg"><img src="images/02_screen_b_thumb.jpg" width="78" height="58" border="0"></a></div>
				<div id="day02_screen_c"><a href="images/02_screen_c.jpg"><img src="images/02_screen_c_thumb.jpg" width="78" height="58" border="0"></a></div>
				<div id="day02_content_b">
					Viaduct is an all-new map especially built for KotH, set outside of the entrance to a secret underground base. Viaduct is also the first Team Fortress map to take place during a snowfall (not counting Acegikmo’s awesome cp_glacier map, or for that matter all the other community mappers who manage to think up and execute on all the cool stuff before we even think of it). Both teams start at the base of a low hill, and will have to climb to get to the capture point at the hill’s peak. The various elevations of the map and multiple routes to the central point guarantee some frenzied battles as the point changes hands during gameplay.
				</div>	
		</div>
	</div>
	<center>
	<div id="footer">
		<div id="copyright">
			&#169; 2009 Valve Corporation, all rights reserved. Valve, the Valve logo, Half-Life, the Half-Life logo, the Lambda logo, Steam, the Steam logo,<br />
		 	Team Fortress, the Team Fortress logo, Opposing Force, Day of Defeat, the Day of Defeat logo, Counter-Strike, the Counter-Strike logo,<br />
		 	Source, the Source logo, Valve Source and Counter-Strike: Condition Zero are trademarks and/or registered trademarks of Valve Corporation. 
		</div>
	<div id="footerLogo"><img src="images/valve_logo.jpg" height="60" width="111" border="0"></div>
	</div>
	<div class="footerShadowBottom">
	<div id="languageSelect"><a href="day02.php">English</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day02.php?l=french">Français</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day02.php?l=german">Deutsch</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day02.php?l=italian">Italiano</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day02.php?l=spanish">Español </a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day02.php?l=swedish">Svenska</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="day02.php?l=russian">Русский</a>
	<div id="forumLink"><a href="https://steamcommunity.com/app/440/discussions/" target="_blank">Discuss @ TF2 Forums</a></div>
		<div id="digg">
			<script type="text/javascript">digg_url = 'https://www.teamfortress.com/classless'; digg_bgcolor = '#bbaca5'; digg_skin = 'compact'; digg_window = 'new'; </script>
			<script src="http://digg.com/tools/diggthis.js" type="text/javascript"></script>
		</div>
	</div>
	</div>
	</center>	
	</div>
  </center>
</div>
</center>
</div>
</body>
</html>
