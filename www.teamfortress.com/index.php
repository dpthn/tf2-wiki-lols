<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<link rel="shortcut icon" href="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/favicon.ico" />
	<title>Team Fortress 2</title>

	<link rel="stylesheet" type="text/css" href="blog.css?v=52" />
	<link rel="stylesheet" type="text/css" href="header.css" />

	<link rel="alternate" type="application/rss+xml" title="RSS" href="rss.xml" />

<!--[if IE]>
	<style type="text/css">
	#contentLeftCol {
		/*background-image:url('images/blog_leftcol_top_bg_ie.png');*/
	}
	</style>
<![endif]-->
	<script language="javascript">
	function charOn( cname )
	{
		el = document.getElementById( 'char' + cname );
		el.style.display = "block";
	}
	function charOff( cname )
	{
		el = document.getElementById( 'char' + cname );
		el.style.display = "none";
	}
	function submitSearch()
	{
		el = document.getElementById( 'searchInput' );
		if ( el.value == 'Search with Google..' )
		{
			el.value = '';
		}
		el.value = el.value + ' site:teamfortress.com';
	}
	function searchBoxFocus()
	{
		el = document.getElementById( 'searchInput' );
		if ( el.value == 'Search with Google..' )
		{
			el.value = '';
		}
	}
	function searchBoxBlur()
	{
		el = document.getElementById( 'searchInput' );
		if ( el.value == '' )
		{
			el.value = 'Search with Google..';
		}
	}
	</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33822635-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>
	<div id="navBarBGRepeat">

		<div id="navBarShadow"></div>
		<div id="navBarBG">

			<div id="navBar">


														<a class="navBarItem" href="index.html">

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_teamfortress_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_teamfortress.png"/>

					</a>

														<a class="navBarItem" href="https://steamcommunity.com/workshop/browse?appid=440"target='_blank'>

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_workshop_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_workshop.png"/>

					</a>

														<a class="navBarItem" href="https://wiki.teamfortress.com/"target='_blank'>

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_wiki_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_wiki.png"/>

					</a>

														<a class="navBarItem" href="https://steamcommunity.com/app/440"target='_blank'>

						<img border='0' class='top' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_community_over.png"/>

						<img border='0' class='bottom' src="https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_community.png"/>

					</a>

							</div>

		</div>

	</div>



<script language="javascript">



	function MM_preloadImages() { //v3.0

	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

	}



	function PreloadHeaderImages()

	{

		//  Preload header rollover images

		MM_preloadImages(

										'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_teamfortress_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_store_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_workshop_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_wiki_over.png',							'https://steamcdn-a.akamaihd.net/apps/tfstore/images/header/en_community_over.png'					);

	}



		document.addEventListener( "DOMContentLoaded", PreloadHeaderImages, false );



</script>


<map id="characterMap" name="characterMap">
<area shape="poly" coords="64,1,102,40,141,13,180,35,164,47,166,89,193,131,213,179,169,242,127,259,111,240,73,237,78,178,52,181,24,92,15,49,0,0" onmouseover="charOn('Sniper');" onmouseout="charOff('Sniper');" href="classes.php?class=sniper" alt="Sniper" title="Sniper"   />
<area shape="poly" coords="208,43,169,52,175,88,195,88,209,117,199,132,212,172,221,181,206,228,253,245,278,264,285,237,312,238,339,222,347,195,343,159,326,130,303,116,284,102,284,76,269,64,254,66,243,86,234,81,232,62,246,52,227,45" onmouseover="charOn('Pyro');" onmouseout="charOff('Pyro');" href="classes.php?class=pyro" alt="Pyro" title="Pyro"   />
<area shape="poly" coords="395,18,413,26,402,45,412,58,423,96,430,113,442,125,408,144,401,166,400,200,395,230,344,237,345,220,353,199,349,162,332,130,307,115,290,102,298,75,319,54,346,43,379,26" onmouseover="charOn('Scout');" onmouseout="charOff('Scout');" href="classes.php?class=scout" alt="Scout" title="Scout"   />
<area shape="poly" coords="409,143,425,132,440,126,447,112,446,99,451,81,466,71,482,73,497,87,507,103,525,110,537,125,542,97,545,71,536,58,533,46,529,31,526,22,539,19,563,19,580,25,578,72,600,77,597,92,584,93,584,113,574,122,564,130,558,142,545,158,532,173,524,189,522,209,526,227,522,262,423,263,421,246,403,248,394,228,399,203,402,171,403,159" onmouseover="charOn('Soldier');" onmouseout="charOff('Soldier');" href="classes.php?class=soldier" alt="Soldier" title="Soldier"   />
<area shape="poly" coords="637,50,655,67,651,94,649,109,662,117,684,123,697,140,709,162,723,180,719,204,698,218,679,234,666,241,660,263,550,263,532,255,529,237,525,212,529,190,537,172,553,160,562,141,577,123,596,112,611,104,609,81,608,67,621,51" onmouseover="charOn('Engineer');" onmouseout="charOff('Engineer');" href="classes.php?class=engineer" alt="Engineer" title="Engineer"   />
<area shape="poly" coords="756,0,732,48,721,77,728,98,744,92,746,65,762,45,783,51,788,81,795,98,842,128,881,167,854,201,813,235,795,264,764,274,737,265,720,249,719,214,732,194,723,167,709,147,705,140,678,114,664,96,670,67,692,37,712,0" onmouseover="charOn('Medic');" onmouseout="charOff('Medic');" href="classes.php?class=medic" alt="Medic" title="Medic"   />
<area shape="poly" coords="840,21,856,39,865,65,897,76,925,92,941,117,962,93,990,53,1016,11,1022,1,1063,32,1001,115,1023,147,1065,195,1057,227,1000,300,987,249,964,221,956,181,945,162,936,168,933,208,908,208,884,213,877,236,877,273,860,292,844,273,839,242,842,221,868,197,888,172,888,159,848,127,803,98,795,79,798,68,813,43,826,23" onmouseover="charOn('Heavy');" onmouseout="charOff('Heavy');" href="classes.php?class=heavy" alt="Heavy" title="Heavy"   />
<area shape="poly" coords="819,242,835,249,840,265,847,281,856,293,871,305,882,319,881,342,896,330,907,326,919,333,914,346,919,363,919,378,901,391,896,411,880,416,860,411,854,415,849,425,730,425,722,401,710,399,693,387,685,371,700,341,720,323,730,306,734,284,757,275,780,276,795,261,809,243" onmouseover="charOn('Demoman');" onmouseout="charOff('Demoman');" href="classes.php?class=demoman" alt="Demoman" title="Demoman"   />
<area shape="poly" coords="904,213,917,219,933,219,938,210,941,171,947,167,953,183,957,219,966,235,986,253,991,290,995,308,980,343,968,358,966,379,966,425,896,424,906,404,917,391,928,375,925,339,909,323,901,325,892,327,877,315,870,294,883,278,887,266,885,247,885,227,894,215" onmouseover="charOn('Spy');" onmouseout="charOff('Spy');" href="classes.php?class=spy" alt="Spy" title="Spy"   />
</map>
<div id="pageContainer">
	<div id="centerColumn">
		<div id="primaryHeaderArea">
        	<!--<div id="bloodsplatter2"><a href="https://www.teamfortress.com/mannwill"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/bloodsplatter2.png" width="44" height="44" border="0"></a></div>
        	<div id="bloodsplatter"><a href="https://www.teamfortress.com/gray"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/bloodsplatter.png" width="82" height="58" border="0"></a></div>
			<div id="spyskull"><a href="https://www.teamfortress.com/wizardcon/"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/spyskull.png" width="50" height="62" border="0"></a></div>-->
            <a id="getItBtn" href="https://store.steampowered.com/app/440/" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="302" height="60" border="0" /></a>
			<div id="transparentMapper"><img usemap="#characterMap" src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="1068" height="426" border="0" /></div>
			<div class="charOnImage" id="charSniper"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_chars_sniper2.jpg" width="214" height="262" border="0" /></div>
			<div class="charOnImage" id="charPyro"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_chars_pyro2.jpg" width="182" height="200" border="0" /></div>
			<div class="charOnImage" id="charScout"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_chars_scout2.jpg" width="157" height="244" border="0" /></div>
			<div class="charOnImage" id="charSoldier"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_chars_soldier2.jpg" width="206" height="242" border="0" /></div>
			<div class="charOnImage" id="charEngineer"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_chars_engineer2.jpg" width="203" height="214" border="0" /></div>
			<div class="charOnImage" id="charMedic"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_chars_medic2.jpg" width="221" height="283" border="0" /></div>
			<div class="charOnImage" id="charHeavy"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_chars_heavy2.jpg" width="284" height="329" border="0" /></div>
			<div class="charOnImage" id="charDemoman"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_chars_demoman2.jpg" width="235" height="184" border="0" /></div>
			<div class="charOnImage" id="charSpy"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_chars_spy2.jpg" width="124" height="264" border="0" /></div>
			<div id="headerCharsBGImg"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_chars_base3.jpg" width="1068" height="426" border="0" /></div>
		</div>
		<div id="centerColContent">
			<div id="blogNavMenu"><a class="aHoverVertShift" id="blog_menu_home" href="index.php"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="93" height="28" border="0" /></a><a class="aHoverVertShift" id="blog_menu_movies" href="movies.php"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="105" height="28" border="0" /></a><a class="aHoverVertShift" id="blog_menu_comics" href="comics.php"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="102" height="28" border="0" /></a><a class="aHoverVertShift" id="blog_menu_history" href="history.php"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="115" height="28" border="0" /></a><a class="aHoverVertShift" id="blog_menu_artwork" href="artwork.php"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="126" height="28" border="0" /></a></div>
			<div id="contentRightCol">
				<div id="featuringSpotlight" style="z-index:999"><a href="https://steamcommunity.com/app/440/workshop/" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/now_featuring_workshop.png" width="380" border="0" /></a></div>
				<div id="rightColSearchBox"><form id="blogSearchForm" onsubmit="submitSearch()" action="https://www.google.com/search" method="GET"><input id="searchInput" name="q" value="Search with Google.." onfocus="searchBoxFocus()" onblur="searchBoxBlur()" /><input id="searchButton" type="image" src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_search_button.png" width="74" height="24" border="0" /></form></div>
                
                <div id="buttonBlock1"><a class="btn" id="wiki_btn" href="https://wiki.teamfortress.com" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="148" height="93" border="0" /></a><a class="btn" id="workshop_btn" href="https://steamcommunity.com/app/440/workshop/" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="148" height="93" border="0" /></a></div>
				<div id="buttonBlock2"><a class="btn" id="market_btn" href="https://steamcommunity.com/market/" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="148" height="93" border="0" /></a><a class="btn" id="gamehub_btn" href="https://steamcommunity.com/app/440" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="148" height="93" border="0" /></a></div>
                <div id="buttonBlock3"><a class="btn" id="sfm_btn" href="https://www.sourcefilmmaker.com/" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="148" height="93" border="0" /></a><a class="btn" id="sheet_music_btn" href="https://steamcdn-a.akamaihd.net/apps/tf2/sheet_music/TF2_Soundtrack_Scores.zip?v=3"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="148" height="93" border="0" /></a></div>
                
                <div id="f2pBlock"><a id="f2pLink" href="freetoplay/index.html"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="280" height="40" border="0" /></a></div>
				<div id="communityButtons">
					<a class="btn" id="comm_fb" href="https://www.facebook.com/TeamFortress2" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="148" height="35" border="0" /></a><a class="btn" id="comm_forums" href="https://steamcommunity.com/app/440/discussions/" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="148" height="35" border="0" /></a><br />
					<a class="btn" id="comm_twitter" href="https://twitter.com/TeamFortress" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="148" height="36" border="0" /></a><a class="btn" id="comm_reddit" href="https://www.reddit.com/r/TF2" target="_blank"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/trans.gif" width="148" height="36" border="0" /></a>
				</div>
                
				<div id="contactBlock">
					<a href="https://www.valvesoftware.com/contact?recipient=TF+Team">TF Team</a><p>For general feedback about the game.</p>
					<a href="https://help.steampowered.com/en/wizard/HelpWithGame/?appid=440">Steam Support</a><p>Visit the support site for any issues you may be having with the game or Steam.</p>
				</div>
			</div>
			<div id="contentLeftCol" style="z-index:1;">
				<div id="leftColTabs">
					<a style="display:none" id="tab_blog_new" href="index.html?tab=blog"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_new.png" width="32" height="21" border="0" /></a>
					<a style="display:none" id="tab_news_new" href="index.html?tab=news"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_new.png" width="32" height="21" border="0" /></a>
					<a style="display:none" id="tab_updates_new" href="index.html?tab=updates"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_new.png" width="32" height="21" border="0" /></a>

					<img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_tab_blog_on.png" width="74" height="24" border="0" /><a href="index.html?tab=news"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_tab_news_off.png" width="75" height="24" border="0" /></a><a href="index.html?tab=updates"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_tab_updates_off.png" width="98" height="24" border="0" /></a>

				</div>
				<div id="subscribeLink"><a id="link_rss" href="rss.xml">Subscribe</a><a href="rss.xml"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/blog_rss_icon.png" width="13" height="13" border="0" /></a></div>
				<div id="leftColPosts">
				<a href="post.php?id=189493" class="postLink">Attention, Steam Workshop Creators!</a>
				<h2>February 9, 2023 - TF2 Team</h2>
				<p>
				<img src="https://steamcdn-a.akamaihd.net/steam/news/189493/workshop.jpg?t=1482256900"><br><br>

<p>Steam Workshop Creators, can we have your attention please. The following message is <i>so</i> urgent, <i>so</i> time-sensitive, we made the executive decision to skip TikTok and Twitter entirely and break the glass on the most bleeding-edge communication technology available.</p><br>  

<p>Welcome to the future. Welcome… to a "blog-post".</p><br> 

<p>"Wow!" you're probably thinking. "I forgot how hard reading is!" Yeah, it's scary how fast you lose that. Don't worry, we'll be brief:</p><br> 

<p>The last few Team Fortress summer events have only been item updates. But <i>this</i> year, we're planning on shipping a full-on holiday-sized update — with items, maps, taunts, unusual effects, war paints, and other community-contributed fixes for the game! Which means we need Steam Workshop content! YOUR Steam Workshop content!</p><br> 

<p>So get to work! (Or back to work, if you were already working but got distracted when the entire internet simultaneously found out about this state-of-the-art blog-post.) Make sure to get your submissions into the Steam Workshop by May 1st, so they can be considered for this as-yet-unnamed, un-themed, but still very exciting summer-situated (but not summer-themed) (unless you wanted to develop summer-themed stuff) update.</p><br>

				</p>
				<iframe src="https://www.facebook.com/plugins/like.php?app_id=169653326431744&amp;href=https%3A%2F%2Fwww.teamfortress.com%2Fpost.php%3Fid%3D189493&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px; display:inline;" allowTransparency="true"></iframe> 
				<div class="postBottomHR"><img src="images/blog_post_hr.png" width="552" height="2" border="0" /></div>
				<a href="post.php?id=63208" class="postLink">TF2 Fight Songs Album is Getting a Free Expansion Pack</a>
				<h2>July 10, 2020 - TF2 Team</h2>
				<p>
				<img src="https://steamcdn-a.akamaihd.net/steam/news/63208/album.png?t=1482256900"><br><br>

<p>People always talk about what a great musician Mozart was. But you know who never updated any of his albums with free music? Go ahead, guess.</p><br> 
 
<p>Did you guess Team Fortress 2? Bzzt. Wrong. Because <i>we're</i> adding three new numbers from our Jungle Inferno update to the TF2 Fight Songs album. If you bought <a href="https://store.steampowered.com/app/629330/Fight_Songs_The_Music_Of_Team_Fortress_2/">Fight Songs on Steam</a>, you don't have to do anything, because the songs have automatically been added to your account.</p><br> 
 
<p>If you bought Fight Songs somewhere else, or even if you <i>didn’t</i> buy Fight Songs, or even if you <i>stole</i> Fight Songs, or did <i>any</i> of those things and then later sold Fight Songs, or bought a recording studio out of spite and recorded a competing version of Fight Songs… look, you’re not on trial here. <a href="https://valvestudioorchestra.bandcamp.com/album/jungle-inferno-more-music-from-team-fortress-2">Download all three songs for free</a> and we’ll forget the whole thing ever happened.</p><br> 
 
<p>Anyway, did you guess yet? Did you guess Mozart? Because that’s what we guessed, too. Except we looked him up while you were reading and it turns out he’s famous for updating all his hit albums. But you know what he hasn’t done yet? Make his sheet music and Sibelius files available for free, like we’re doing literally right now: <a href="https://steamcdn-a.akamaihd.net/apps/tf2/sheet_music/TF2_Soundtrack_Scores.zip">Sheet music. Sibelius files.</a> Checkmate, Mozart. See you in hell.</p><br>


				</p>
				<iframe src="https://www.facebook.com/plugins/like.php?app_id=169653326431744&amp;href=https%3A%2F%2Fwww.teamfortress.com%2Fpost.php%3Fid%3D63208&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px; display:inline;" allowTransparency="true"></iframe> 
				<div class="postBottomHR"><img src="images/blog_post_hr.png" width="552" height="2" border="0" /></div>
				<a href="post.php?id=61081" class="postLink">A Hell of a Campaign</a>
				<h2>May 4, 2020 - TF2 Team</h2>
				<p>
				<img src="https://steamcdn-a.akamaihd.net/steam/news/61081/soldier_statue.png?t=1482255080"><br><br>

<p>Rick May, the inimitable voice of the Soldier for thirteen years, many shorts and countless updates, passed away this April at age 79. We were lucky enough to work with Rick many times in the studio over the past decade. A quick-witted and kind-hearted collaborator, Rick endowed the character with a trademark bellow and bootfulls of idiotic charm. The Soldier wouldn’t be the Soldier without him.</p><br> 

<p>We are honoring Rick during the month of May with an in-game Soldier statue that will play some of Rick’s signature lines. He will be missed by the Team Fortress dev team and community, and remembered for the indelible character he helped bring to life for so many years.</p><br>


				</p>
				<iframe src="https://www.facebook.com/plugins/like.php?app_id=169653326431744&amp;href=https%3A%2F%2Fwww.teamfortress.com%2Fpost.php%3Fid%3D61081&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px; display:inline;" allowTransparency="true"></iframe> 
				<div class="postBottomHR"><img src="images/blog_post_hr.png" width="552" height="2" border="0" /></div>
				<a href="post.php?id=56988" class="postLink">Merry Smissmas, Everyone!</a>
				<h2>December 16, 2019 - TF2 Team</h2>
				<p>
				<a href="post.php?id=56987"><img src="https://steamcdn-a.akamaihd.net/steam/news/56988/smissmas.png?t=1482255080"></a><br><br>

<p>It's that time of the year again! Merry Smissmas!</p><br>

<b>New Community Cosmetics, War Paints, and Unusual Effects</b>
<p>We've added 18 new community cosmetics, 10 new community War Paints, and 17 new community Unusual effects! Plus, the Festivizer can be found as a bonus drop when opening the Winter 2019 Cosmetic Case, and the taunt Unusualifier can be found as a bonus drop when opening the Winter 2019 War Paint Case!</p><br>

<b>Big Sale in the Mann Co. Store</b><br>
<p>It wouldn't be Smissmas without the Spirit of Practically Giving. Look for 50% off tons of items, taunts, and tools in the Mann Co. Store through January 7th, 2020.</p><br>

<p>And last but not least... <b>Stocking stuffers for everyone!</b> Merry Smissmas! See you in 2020!</p><br>

				</p>
				<iframe src="https://www.facebook.com/plugins/like.php?app_id=169653326431744&amp;href=https%3A%2F%2Fwww.teamfortress.com%2Fpost.php%3Fid%3D56988&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px; display:inline;" allowTransparency="true"></iframe> 
				<div class="postBottomHR"><img src="images/blog_post_hr.png" width="552" height="2" border="0" /></div>
				<a href="post.php?id=53280" class="postLink">Insomnia65</a>
				<h2>August 12, 2019 - TF2 Team</h2>
				<p>
				<a href="https://lan.tf/"><img src="https://steamcdn-a.akamaihd.net/steam/news/53280/blogpost.png?t=1496190709"></a><br><br>

<p>The biggest event on the Competitive TF2 calendar is back again! ​Teams and players from around the entire world will be heading to the National Exhibition Centre in Birmingham, England, to the <a href="https://insomniagamingfestival.com/" target="_blank">​Insomnia Gaming Festival</a> for their chance at glory and a share of the prize pool. Taking place from Friday, August 23rd, to Sunday, August 25th, the event will feature three days of non-stop action and the highest level of competition the game has to offer.</p><br>

<p>If you wish to support the event there is an ​<a href="https://matcherino.com/t/i65tf2/" target="_blank">ongoing fundraiser</a>​ where you can donate money or purchase a range of items; money raised goes towards the production, prize pool, and supporting the North American team, Ascent.NA, attending to represent their region.</p><br>

<p>Signups for the tournament are <a href="https://www.toornament.com/en_US/tournaments/2587533425695768576/participants/" target="_blank">​here</a>​. However, if you can’t make it, the event will be streamed both at the event and online by EssentialsTF over at their <a href="https://www.twitch.tv/essentialstf" target="_blank">​Twitch page</a>​. For the latest information, be sure to check out ​<a href="https://lan.tf/" target="_blank">LAN.TF</a>​ to follow the tournament live.</p><br>


				</p>
				<iframe src="https://www.facebook.com/plugins/like.php?app_id=169653326431744&amp;href=https%3A%2F%2Fwww.teamfortress.com%2Fpost.php%3Fid%3D53280&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px; display:inline;" allowTransparency="true"></iframe> 
				<div class="postBottomHR"><img src="images/blog_post_hr.png" width="552" height="2" border="0" /></div>

					<div id="navLinks">
						<div id="navNewer"></div>
						<a href="index.php?p=2&amp;tab=blog">&lt;&lt; Older Posts</a>
					</div>

				</div>
			</div>
			<br clear="right" />
		</div>
	</div>
	<div id="centerFooter">
		<div id="footerContainer"><a href="https://www.valvesoftware.com"><img src="https://steamcdn-a.akamaihd.net/apps/tf2/blog/images/valve_footer.png" width="984" height="52" border="0" /></a></div>
	</div>
</div>
</body>
</html>
